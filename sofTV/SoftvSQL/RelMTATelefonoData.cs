using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Softv.Entities;
using Softv.Providers;
using SoftvConfiguration;

namespace Softv.DAO
{
  /// <summary>
  /// Class                   : Softv.DAO.RelMTATelefonoData
  /// Generated by            : Desarroll@, Class Generator (c) 2009
  /// Description             : RelMTATelefono Data Access Object
  /// File                    : RelMTATelefonoDAO.cs
  /// Creation date           : 17/11/2011
  /// Creation time           : 01:29:14 p.m.
  /// </summary>
  public class RelMTATelefonoData : RelMTATelefonoProvider
  {
      /// <summary>
      /// </summary>
      /// <param name="RelMTATelefono">Object RelMTATelefono added to list</param>
      public override int AddRelMTATelefono(RelMTATelefonoEntity entity_RelMTATelefono)
      {
          int result=0;
          using(SqlConnection connection = new SqlConnection(SoftvSettings.Settings.RelMTATelefono.ConnectionString))
          {
              SqlCommand comandoSql = new SqlCommand("Softv_AddRelMTATelefono", connection);
              comandoSql.CommandType = CommandType.StoredProcedure;

              comandoSql.Parameters.Add("@id", SqlDbType.BigInt);
              comandoSql.Parameters["@id"].Direction = ParameterDirection.Output;

               comandoSql.Parameters.AddWithValue("@idAparato", (object)entity_RelMTATelefono.idAparato ?? (object)DBNull.Value);
               comandoSql.Parameters.AddWithValue("@idTelefono", (object)entity_RelMTATelefono.idTelefono ?? (object)DBNull.Value);
               comandoSql.Parameters.AddWithValue("@fechaAsignacion", (object)entity_RelMTATelefono.fechaAsignacion ?? (object)DBNull.Value);
               comandoSql.Parameters.AddWithValue("@usrAsignacion", String.IsNullOrEmpty(entity_RelMTATelefono.usrAsignacion) ? DBNull.Value : (object)entity_RelMTATelefono.usrAsignacion );

              try
              {
                  if (connection.State == ConnectionState.Closed)
                      connection.Open();
                  result = ExecuteNonQuery(comandoSql);
              }
              catch (Exception ex)
              {
                  throw new Exception("Error adding RelMTATelefono " + ex.Message, ex);
              }
              finally
              {
                  connection.Close();
              }
              if (result > 0)
                  result = int.Parse(comandoSql.Parameters["@id"].Value.ToString());
          }
          return result;

      }

      /// <summary>
      /// Deletes a RelMTATelefono
      /// </summary>
      /// <param name="System.String[]">RelMTATelefonoid to delete</param>
      public override int DeleteRelMTATelefono(long? id)
      {
          int result=0;
          using(SqlConnection connection = new SqlConnection(SoftvSettings.Settings.RelMTATelefono.ConnectionString))
          {
              SqlCommand comandoSql = new SqlCommand("Softv_DeleteRelMTATelefono", connection);
              comandoSql.CommandType = CommandType.StoredProcedure;
              comandoSql.Parameters.AddWithValue("@id", id);

              try
              {
                  if (connection.State == ConnectionState.Closed)
                     connection.Open();
                  result = ExecuteNonQuery(comandoSql);
              }
              catch (Exception ex)
              {
                  throw new Exception("Error deleting RelMTATelefono " + ex.Message, ex);
              }
              finally
              {
                  if(connection != null)
                  connection.Close();
              }
          }
          return result;

      }

      /// <summary>
      /// Edits a RelMTATelefono
      /// </summary>
      /// <param name="RelMTATelefono">Objeto RelMTATelefono a editar</param>
      public override int EditRelMTATelefono(RelMTATelefonoEntity entity_RelMTATelefono)
      {
          int result=0;
          using(SqlConnection connection = new SqlConnection(SoftvSettings.Settings.RelMTATelefono.ConnectionString))
          {
              SqlCommand comandoSql = new SqlCommand("Softv_EditRelMTATelefono", connection);
              comandoSql.CommandType = CommandType.StoredProcedure;

               comandoSql.Parameters.AddWithValue("@id", (object)entity_RelMTATelefono.id ?? (object)DBNull.Value);
               comandoSql.Parameters.AddWithValue("@idAparato", (object)entity_RelMTATelefono.idAparato ?? (object)DBNull.Value);
               comandoSql.Parameters.AddWithValue("@idTelefono", (object)entity_RelMTATelefono.idTelefono ?? (object)DBNull.Value);
               comandoSql.Parameters.AddWithValue("@fechaAsignacion", (object)entity_RelMTATelefono.fechaAsignacion ?? (object)DBNull.Value);
               comandoSql.Parameters.AddWithValue("@usrAsignacion", String.IsNullOrEmpty(entity_RelMTATelefono.usrAsignacion) ? DBNull.Value : (object)entity_RelMTATelefono.usrAsignacion );

              try
              {
                  if (connection.State == ConnectionState.Closed)
                      connection.Open();
                  result = ExecuteNonQuery(comandoSql);
              }
              catch (Exception ex)
              {
                  throw new Exception("Error updating RelMTATelefono " + ex.Message, ex);
              }
              finally
              {
                  if(connection != null)
                      connection.Close();
              }
          }
          return result;
      }

      /// <summary>
      /// Gets all RelMTATelefono
      /// </summary>
      public override List<RelMTATelefonoEntity> GetRelMTATelefono()
      {
          List<RelMTATelefonoEntity> RelMTATelefonoList = new List<RelMTATelefonoEntity>();
          using(SqlConnection connection = new SqlConnection(SoftvSettings.Settings.RelMTATelefono.ConnectionString))
          {
              SqlCommand comandoSql = new SqlCommand("Softv_GetRelMTATelefono", connection);
              comandoSql.CommandType = CommandType.StoredProcedure;

              IDataReader rd = null;
              try
              {
                  if (connection.State == ConnectionState.Closed)
                  connection.Open();
                  rd = ExecuteReader(comandoSql);

                  while (rd.Read())
                  {
                      RelMTATelefonoList.Add(GetRelMTATelefonoFromReader(rd));
                  }
              }
              catch (Exception ex)
              {
                  throw new Exception("Error getting data RelMTATelefono "  + ex.Message, ex);
              }
              finally
              {
                  if(connection!=null)
                      connection.Close();
                  if(rd != null)
                      rd.Close();
              }
          }
          return RelMTATelefonoList;
      }

      /// <summary>
      /// Gets all GetMTAsDisponibles
      /// </summary>
      public override DataTable GetMTAsDisponibles()
      {

          DataTable dt = new DataTable();
          using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.RelMTATelefono.ConnectionString))
          {
              SqlCommand comandoSql = new SqlCommand("uspConsultaMTAsAsignacionNumTel", connection);
              comandoSql.CommandType = CommandType.StoredProcedure;
              SqlDataAdapter da = new SqlDataAdapter();

              try
              {
                  if (connection.State == ConnectionState.Closed)
                      connection.Open();
                  da.SelectCommand = comandoSql;
                  da.Fill(dt);
              }
              catch (Exception ex)
              {
                  throw new Exception("Error getting uspConsultaMTAsAsignacionNumTel " + ex.Message, ex);
              }
              finally
              {
                  if (connection != null)
                      connection.Close();
              }
          }
          return dt;
      }

      /// <summary>
      /// Gets all GetMTAsDisponibles
      /// </summary>
      public override DataTable GetMTAsDisponiblesByFiltros(long idAparato, string MAC, string MACMTA)
      {

          DataTable dt = new DataTable();
          using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.RelMTATelefono.ConnectionString))
          {
              SqlCommand comandoSql = new SqlCommand("uspConsultaMTAsAsignacionNumTelByFiltros", connection);
              comandoSql.CommandType = CommandType.StoredProcedure;
              comandoSql.Parameters.Add("@idAparato", SqlDbType.BigInt).Value = idAparato;
              comandoSql.Parameters.Add("@MAC", SqlDbType.VarChar, 250).Value = MAC;
              comandoSql.Parameters.Add("@MACMTA", SqlDbType.VarChar,250).Value = MACMTA;  
              SqlDataAdapter da = new SqlDataAdapter();

              try
              {
                  if (connection.State == ConnectionState.Closed)
                      connection.Open();
                  da.SelectCommand = comandoSql;
                  da.Fill(dt);
              }
              catch (Exception ex)
              {
                  throw new Exception("Error getting GetMTAsDisponiblesByFiltros " + ex.Message, ex);
              }
              finally
              {
                  if (connection != null)
                      connection.Close();
              }
          }
          return dt;
      }

      /// <summary>
      /// Obtiene todos los n�meros telef�nicos que est�n disponibles para asignaci�n a MTAs
      /// </summary>
      public override DataTable GetTelefonosDisponiblesAsignacionMTA()
      {

          DataTable dt = new DataTable();
          using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.RelMTATelefono.ConnectionString))
          {
              SqlCommand comandoSql = new SqlCommand("uspConsultaTelefonosDisponiblesAsignacionMTA", connection);
              comandoSql.CommandType = CommandType.StoredProcedure;
              SqlDataAdapter da = new SqlDataAdapter();

              try
              {
                  if (connection.State == ConnectionState.Closed)
                      connection.Open();
                  da.SelectCommand = comandoSql;
                  da.Fill(dt);
              }
              catch (Exception ex)
              {
                  throw new Exception("Error getting GetTelefonosDisponiblesAsignacionMTA " + ex.Message, ex);
              }
              finally
              {
                  if (connection != null)
                      connection.Close();
              }
          }
          return dt;
      }

      /// <summary>
      /// Obtiene todos los n�meros telef�nicos que est�n disponibles para asignaci�n a MTAs
      /// </summary>
      public override DataTable GetRelMTATelefonoByIDAparato(long idAparato)
      {

          DataTable dt = new DataTable();
          using (SqlConnection connection = new SqlConnection(SoftvSettings.Settings.RelMTATelefono.ConnectionString))
          {
              SqlCommand comandoSql = new SqlCommand("uspConsultaRelMTATelefono", connection);
              comandoSql.CommandType = CommandType.StoredProcedure;
              comandoSql.Parameters.Add("@idAparato", SqlDbType.BigInt).Value = idAparato;
              SqlDataAdapter da = new SqlDataAdapter();

              try
              {
                  if (connection.State == ConnectionState.Closed)
                      connection.Open();
                  da.SelectCommand = comandoSql;
                  da.Fill(dt);
              }
              catch (Exception ex)
              {
                  throw new Exception("Error getting GetRelMTATelefonoByIDAparato " + ex.Message, ex);
              }
              finally
              {
                  if (connection != null)
                      connection.Close();
              }
          }
          return dt;
      }

      /// <summary>
      /// Gets RelMTATelefono by System.String[]
      /// </summary>
      /// <param name="System.String[]">Id del RelMTATelefono a consultar</param>
      public override RelMTATelefonoEntity GetRelMTATelefonoById(long? id)
      {
          using(SqlConnection connection = new SqlConnection(SoftvSettings.Settings.RelMTATelefono.ConnectionString))
          {
              SqlCommand comandoSql = new SqlCommand("Softv_GetRelMTATelefonoById", connection);
              RelMTATelefonoEntity entity_RelMTATelefono = null;
              comandoSql.CommandType = CommandType.StoredProcedure;

              comandoSql.Parameters.AddWithValue("@id", id);

              IDataReader rd = null;
              try
              {
                  if (connection.State == ConnectionState.Closed)
                      connection.Open();
                  rd = ExecuteReader(comandoSql, CommandBehavior.SingleRow);
                  if (rd.Read())
                      entity_RelMTATelefono = GetRelMTATelefonoFromReader(rd);
              }
              catch (Exception ex)
              {
                  throw new Exception("Error getting data RelMTATelefono "  + ex.Message, ex);
              }
              finally
              {
                  if(connection!=null)
                      connection.Close();
                  if(rd != null)
                      rd.Close();
              }
              return entity_RelMTATelefono;
          }
      }
  }
}

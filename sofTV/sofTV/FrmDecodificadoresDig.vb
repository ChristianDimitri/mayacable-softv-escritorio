﻿Imports System.Data.SqlClient
Imports System.Text

Public Class FrmDecodificadoresDig


    Private Sub MuestraServiciosEricDec(ByVal Clv_TipSer As Integer, ByVal Clv_Servicio As Integer, ByVal Op As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder("EXEC MuestraServiciosEric " & CStr(Clv_TipSer) & ", " & CStr(Clv_Servicio) & ", " & CStr(Op))
        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString, conexion)
        Dim dataTable As New DataTable
        Dim bindingSource As New BindingSource
        Try
            dataAdapter.Fill(dataTable)
            bindingSource.DataSource = dataTable
            comboServicioDec.DataSource = bindingSource
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub
    Private Sub ConTblDecodificadores()

        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder("EXEC ConTblDecodificadores")
        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString, conexion)
        Dim dataTable As New DataTable
        Dim bindingSource As New BindingSource
        Try
            dataAdapter.Fill(dataTable)
            bindingSource.DataSource = dataTable
            dgvDec.DataSource = dataTable
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub


    Private Sub NueTblDecodificadores(ByVal Descripcion As String, ByVal Clv_Servicio As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("NueTblDecodificadores", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Dim par As New SqlParameter("@Descripcion", SqlDbType.VarChar, 150)
        par.Direction = ParameterDirection.Input
        par.Value = Descripcion
        comando.Parameters.Add(par)

        Dim par2 As New SqlParameter("@Clv_Servicio", SqlDbType.VarChar, 150)
        par2.Direction = ParameterDirection.Input
        par2.Value = Clv_Servicio
        comando.Parameters.Add(par2)

        Dim par3 As New SqlParameter("@Res", SqlDbType.Int)
        par3.Direction = ParameterDirection.Output
        comando.Parameters.Add(par3)

        Dim par4 As New SqlParameter("@Msj", SqlDbType.VarChar, 150)
        par4.Direction = ParameterDirection.Output
        comando.Parameters.Add(par4)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            eRes = 0
            eMsj = ""
            eRes = par3.Value
            eMsj = par4.Value
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

    Private Sub BorTblDecodificadores(ByVal Id As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("BorTblDecodificadores", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Dim par As New SqlParameter("@Id", SqlDbType.Int)
        par.Direction = ParameterDirection.Input
        par.Value = Id
        comando.Parameters.Add(par)

        Dim par2 As New SqlParameter("@Res", SqlDbType.Int)
        par2.Direction = ParameterDirection.Output
        comando.Parameters.Add(par2)

        Dim par3 As New SqlParameter("@Msj", SqlDbType.VarChar, 150)
        par3.Direction = ParameterDirection.Output
        comando.Parameters.Add(par3)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            eRes = 0
            eMsj = ""
            eRes = par2.Value
            eMsj = par3.Value
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub



    Private Sub txtAgregarDec_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtAgregarDec.Click
        If txtDescripcionDec.Text.Length = 0 Then
            MsgBox("Escribe un nombre para el Decodificador.", MsgBoxStyle.Information)
            Exit Sub
        End If
        If IsNumeric(comboServicioDec.SelectedValue) = False Then
            MsgBox("Selecciona un Servicio.", MsgBoxStyle.Information)
            Exit Sub
        End If

        NueTblDecodificadores(txtDescripcionDec.Text, comboServicioDec.SelectedValue)
        If eRes = 1 Then
            MsgBox(eMsj, MsgBoxStyle.Exclamation)
            Exit Sub
        End If
        ConTblDecodificadores()

    End Sub

    Private Sub txtEliminarDec_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtEliminarDec.Click
        If dgvDec.Rows.Count = 0 Then
            MsgBox("Selecciona un Decodificador.", MsgBoxStyle.Information)
            Exit Sub
        End If
        BorTblDecodificadores(dgvDec.SelectedCells.Item(0).Value)
        If eRes = 1 Then
            MsgBox(eMsj, MsgBoxStyle.Exclamation)
            Exit Sub
        End If
        ConTblDecodificadores()
    End Sub

    Private Sub FrmDecodificadoresDig_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
        MuestraServiciosEricDec(3, 0, 5)
        ConTblDecodificadores()
    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub
End Class
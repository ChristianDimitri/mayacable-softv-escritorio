﻿Public Class FrmCargadeTrabajo

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        UspGuardaCarga()

        Me.Close()
    End Sub

    Private Sub UspMostrarCarga()
        Try
            Dim DT As New DataTable
            BaseII.limpiaParametros()
            DT = BaseII.ConsultaDT("UspMostrarCarga")
            If DT.Rows.Count > 0 Then
                CargaDeTrabajoTextBox.Text = DT.Rows(0)(0).ToString
                Carga_trab_insTextBox.Text = DT.Rows(0)(1).ToString
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub UspGuardaCarga()
        Try
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@CARGATRABAJO", SqlDbType.Int, CInt(Me.CargaDeTrabajoTextBox.Text))
            BaseII.CreateMyParameter("@CARGAINST", SqlDbType.Int, CInt(Me.Carga_trab_insTextBox.Text))
            BaseII.Inserta("UspGuardaCarga")
            MsgBox("Fue guardado con exito")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    '](@CARGATRABAJO INT,@CARGAINST INT)

    Private Sub FrmCargadeTrabajo_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        UspMostrarCarga()
    End Sub
End Class
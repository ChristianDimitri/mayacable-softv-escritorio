Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Public Class FrmNumMeses
    Private customersByCityReport As ReportDocument
    Private Sub FrmNumMeses_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        colorea(Me, Me.Name)
        Me.Label1.Text = "Estado de Cuenta del Contrato :" + CStr(LiContrato)
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Me.Close()
    End Sub

    Private Sub Button11_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button11.Click
        'Reporte de Estado de Cuenta
        If Me.NumericUpDown1.Value > 0 Then
            ConfigureCrystalReportsEstadodeCuenta(Me.NumericUpDown1.Value)
            Me.Close()
        Else
            MsgBox("Por lo Menos debe ser un Mes", MsgBoxStyle.Information)
        End If

    End Sub

    Private Sub ConfigureCrystalReportsEstadodeCuenta(ByVal Meses As Integer)
        Try
            Dim impresora As String = Nothing
            customersByCityReport = New Report_Estado_Cuentas
            Dim connectionInfo As New ConnectionInfo

            connectionInfo.ServerName = GloServerName
            connectionInfo.DatabaseName = GloDatabaseName
            connectionInfo.UserID = GloUserID
            connectionInfo.Password = GloPassword

            'Dim reportPath As String = Nothing
            'reportPath = RutaReportes + "\ReportEstado_Cuenta.rpt"
            'reportPath = "Report_Estado_Cuentas.rpt"
            'customersByCityReport.Load(reportPath)

            SetDBLogonForReport(connectionInfo, customersByCityReport)

            '@Contrato 
            customersByCityReport.SetParameterValue(0, LiContrato)
            customersByCityReport.SetParameterValue(1, IdSistema)
            customersByCityReport.SetParameterValue(2, 0)
            customersByCityReport.SetParameterValue(3, Meses)
            'CrystalReportViewer1.ReportSource = customersByCityReport
            'CrystalReportViewer1.ShowPrintButton = True
            'Me.Selecciona_ImpresoraTableAdapter.Fill(Me.DataSetarnoldo.Selecciona_Impresora, 1, impresora)

            customersByCityReport.PrintOptions.PaperOrientation = PaperOrientation.Landscape
            customersByCityReport.PrintOptions.PrinterName = ImpresoraEstado
            customersByCityReport.PrintToPrinter(1, True, 1, Meses - 1)
            customersByCityReport = Nothing
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub
    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        customersByCityReport.DataSourceConnections(0).SetConnection(GloServerName, GloDatabaseName, GloUserID, GloPassword)
        'customersByCityReport.SetDatabaseLogon(GloUserID, GloPassword, GloServerName, GloDatabaseName)

        Dim myTables As Tables = myReportDocument.Database.Tables
        Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
        For Each myTable In myTables
            Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            myTable.ApplyLogOnInfo(myTableLogonInfo)
            myTable.Location = GloDatabaseName & ".dbo." & myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)
        Next
    End Sub
End Class
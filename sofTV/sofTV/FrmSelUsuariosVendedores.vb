﻿Public Class FrmSelUsuariosVendedores

    Private Sub Dame_clv_session_Reportes()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_Session", ParameterDirection.Output, SqlDbType.Int)
        BaseII.ProcedimientoOutPut("Dame_clv_session_Reportes")
        eClv_Session = BaseII.dicoPar("@Clv_Session").ToString
    End Sub

    Private Sub MUESTRASeleccionaUsuariosPro(ByVal Clv_Grupo As Integer, ByVal Clv_Session As Integer, ByVal Op As Integer)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_Grupo", SqlDbType.Int, Clv_Grupo)
        BaseII.CreateMyParameter("@Clv_Session", SqlDbType.Int, Clv_Session)
        BaseII.CreateMyParameter("@Op", SqlDbType.Int, Op)
        dgvIzquierda.DataSource = BaseII.ConsultaDT("MUESTRASeleccionaUsuariosPro")
    End Sub


    Private Sub MUESTRASeleccionaUsuariosTmp(ByVal Clv_Session As Integer)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_Session", SqlDbType.Int, Clv_Session)
        dgvDerecha.DataSource = BaseII.ConsultaDT("MUESTRASeleccionaUsuariosTmp")
    End Sub

    Private Sub NUESeleccionaUsuariosTmp(ByVal Clv_Grupo As Integer, ByVal Clave As Integer, ByVal Clv_Session As Integer, ByVal Op As Integer)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_Grupo", SqlDbType.Int, Clv_Grupo)
        BaseII.CreateMyParameter("@Clave", SqlDbType.Int, Clave)
        BaseII.CreateMyParameter("@Clv_Session", SqlDbType.Int, Clv_Session)
        BaseII.CreateMyParameter("@Op", SqlDbType.Int, Op)
        BaseII.Inserta("NUESeleccionaUsuariosTmp")
    End Sub

    Private Sub BORSeleccionaUsuariosTmp(ByVal Clv_Grupo As Integer, ByVal Clave As Integer, ByVal Clv_Session As Integer, ByVal Op As Integer)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_Grupo", SqlDbType.Int, Clv_Grupo)
        BaseII.CreateMyParameter("@Clave", SqlDbType.Int, Clave)
        BaseII.CreateMyParameter("@Clv_Session", SqlDbType.Int, Clv_Session)
        BaseII.CreateMyParameter("@Op", SqlDbType.Int, Op)
        BaseII.Inserta("BORSeleccionaUsuariosTmp")
    End Sub

    Private Sub FrmSelUsuariosVendedores_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
        Dame_clv_session_Reportes()
        MUESTRASeleccionaUsuariosPro(eClv_Grupo, eClv_Session, 0)
    End Sub

    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles Button1.Click
        If dgvIzquierda.Rows.Count = 0 Then
            Exit Sub
        End If
        NUESeleccionaUsuariosTmp(dgvIzquierda.SelectedCells(0).Value, dgvIzquierda.SelectedCells(1).Value, eClv_Session, 0)
        MUESTRASeleccionaUsuariosPro(0, eClv_Session, 1)
        MUESTRASeleccionaUsuariosTmp(eClv_Session)
    End Sub

    Private Sub Button2_Click(sender As System.Object, e As System.EventArgs) Handles Button2.Click
        If dgvIzquierda.Rows.Count = 0 Then
            Exit Sub
        End If
        NUESeleccionaUsuariosTmp(0, 0, eClv_Session, 1)
        MUESTRASeleccionaUsuariosPro(0, eClv_Session, 1)
        MUESTRASeleccionaUsuariosTmp(eClv_Session)
    End Sub

    Private Sub Button3_Click(sender As System.Object, e As System.EventArgs) Handles Button3.Click
        If dgvDerecha.Rows.Count = 0 Then
            Exit Sub
        End If
        BORSeleccionaUsuariosTmp(dgvDerecha.SelectedCells(0).Value, dgvDerecha.SelectedCells(1).Value, eClv_Session, 0)
        MUESTRASeleccionaUsuariosPro(0, eClv_Session, 1)
        MUESTRASeleccionaUsuariosTmp(eClv_Session)
    End Sub

    Private Sub Button4_Click(sender As System.Object, e As System.EventArgs) Handles Button4.Click
        If dgvDerecha.Rows.Count = 0 Then
            Exit Sub
        End If
        BORSeleccionaUsuariosTmp(dgvDerecha.SelectedCells(0).Value, dgvDerecha.SelectedCells(1).Value, eClv_Session, 0)
        MUESTRASeleccionaUsuariosPro(0, eClv_Session, 1)
        MUESTRASeleccionaUsuariosTmp(eClv_Session)
    End Sub

    Private Sub bnAceptar_Click(sender As System.Object, e As System.EventArgs) Handles bnAceptar.Click
        If dgvDerecha.Rows.Count = 0 Then
            MessageBox.Show("Selecciona un Usuario y/o Vendedor.")
            Exit Sub
        End If

        If eOpVentas = 1 Then
            FrmImprimirComision.Show()
        Else
            FrmSelServicioE.Show()
        End If


        Me.Close()
    End Sub

    Private Sub bnSalir_Click(sender As System.Object, e As System.EventArgs) Handles bnSalir.Click
        Me.close()
    End Sub
End Class
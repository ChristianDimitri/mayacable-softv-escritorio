Imports System.Data.SqlClient
Imports System.Drawing.Drawing2D
Imports System.Text
Imports sofTV.BAL
Imports System.Collections.Generic
Public Class FrmCAMDO

    Private opcionlocal As String = "N"

    Private Sub CONCAMDOBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CONCAMDOBindingNavigatorSaveItem.Click
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        If IsNumeric(Me.Clv_CalleComboBox.SelectedValue) = False Then
            MsgBox("Seleccione la Calle por favor", MsgBoxStyle.Information)
            Exit Sub
        End If
        If IsNumeric(Me.Clv_ColoniaComboBox.SelectedValue) = False Then
            MsgBox("Seleccione la Colonia por favor", MsgBoxStyle.Information)
            Exit Sub
        End If
        If IsNumeric(Me.Clv_CiudadComboBox.SelectedValue) = False Then
            MsgBox("Seleccione la Ciudad por favor", MsgBoxStyle.Information)
            Exit Sub
        End If
        If Len(Trim(Me.NUMEROTextBox.Text)) = 0 Then
            MsgBox("Capture el Numero por favor", MsgBoxStyle.Information)
            Exit Sub
        End If
        If Len(Trim(Me.ENTRECALLESTextBox.Text)) = 0 Then
            MsgBox("Capture el Numero por favor", MsgBoxStyle.Information)
            Exit Sub
        End If
        If Len(Trim(Me.TELEFONOTextBox.Text)) = 0 Then
            MsgBox("Capture el Numero Telef�nico por favor ", MsgBoxStyle.Information)
            Exit Sub
        End If
        'Me.Validate()
        'Me.CONCAMDOBindingSource.EndEdit()
        'Me.CONCAMDOTableAdapter.Connection = CON
        'Me.CONCAMDOTableAdapter.Update(Me.NewSofTvDataSet.CONCAMDO)
        'MsgBox(mensaje5)
        bndCAMDO = True
        'Me.Close()
        'CON.Close()
        'GloBndExt = True
        guardarSector(GloDetClave, gloClv_Orden, Contrato, CInt(Me.Clv_CalleComboBox.SelectedValue), Me.NUMEROTextBox.Text, Me.ENTRECALLESTextBox.Text, CInt(Me.Clv_ColoniaComboBox.SelectedValue), Me.TELEFONOTextBox.Text, 0, CInt(Me.Clv_CiudadComboBox.SelectedValue), CInt(Me.ComboBox1.SelectedValue))
        Me.Close()
    End Sub

    Private Sub guardarSector(ByVal clv As Long, ByVal clvorden As Long, ByVal contrato As Long, ByVal cvecalle As Integer, ByVal num As String, ByVal entrecalles As String, ByVal ccolonia As Integer, ByVal tel As String, ByVal ou As Decimal, ByVal cciu As Integer, ByVal sec As Integer)

        Dim CON As New SqlConnection(MiConexion)
        Dim CMD As New SqlCommand("ALTACAMDO", CON)
        CMD.CommandType = CommandType.StoredProcedure

        Dim PRM0 As New SqlParameter("@clv", SqlDbType.BigInt)
        PRM0.Direction = ParameterDirection.Input
        PRM0.Value = clv
        CMD.Parameters.Add(PRM0)

        Dim PRM1 As New SqlParameter("@clvorden", SqlDbType.Int)
        PRM1.Direction = ParameterDirection.Input
        PRM1.Value = clvorden
        CMD.Parameters.Add(PRM1)

        Dim PRM2 As New SqlParameter("@con", SqlDbType.BigInt)
        PRM2.Direction = ParameterDirection.Input
        PRM2.Value = contrato
        CMD.Parameters.Add(PRM2)

        Dim PRM3 As New SqlParameter("@cal", SqlDbType.Int)
        PRM3.Direction = ParameterDirection.Input
        PRM3.Value = cvecalle
        CMD.Parameters.Add(PRM3)

        Dim PRM4 As New SqlParameter("@num", SqlDbType.VarChar, 100)
        PRM4.Direction = ParameterDirection.Input
        PRM4.Value = num
        CMD.Parameters.Add(PRM4)

        Dim PRM5 As New SqlParameter("@ent", SqlDbType.VarChar, 250)
        PRM5.Direction = ParameterDirection.Input
        PRM5.Value = entrecalles
        CMD.Parameters.Add(PRM5)

        Dim PRM6 As New SqlParameter("@col", SqlDbType.Int)
        PRM6.Direction = ParameterDirection.Input
        PRM6.Value = ccolonia
        CMD.Parameters.Add(PRM6)

        Dim PRM7 As New SqlParameter("@tel", SqlDbType.VarChar, 250)
        PRM7.Direction = ParameterDirection.Input
        PRM7.Value = tel
        CMD.Parameters.Add(PRM7)

        Dim PRM8 As New SqlParameter("@ou", SqlDbType.Decimal)
        PRM8.Direction = ParameterDirection.Input
        PRM8.Value = ou
        CMD.Parameters.Add(PRM8)

        Dim PRM9 As New SqlParameter("@ciu", SqlDbType.Int)
        PRM9.Direction = ParameterDirection.Input
        PRM9.Value = cciu
        CMD.Parameters.Add(PRM9)

        Dim PRM10 As New SqlParameter("@sec", SqlDbType.Int)
        PRM10.Direction = ParameterDirection.Input
        PRM10.Value = sec
        CMD.Parameters.Add(PRM10)

        Try
            CON.Open()
            CMD.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub

    Private Sub Busca()
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Try
            Me.CONCAMDOTableAdapter.Connection = CON
            Me.CONCAMDOTableAdapter.Fill(Me.NewSofTvDataSet.CONCAMDO, New System.Nullable(Of Long)(CType(GloDetClave, Long)), New System.Nullable(Of Long)(CType(gloClv_Orden, Long)), New System.Nullable(Of Long)(CType(Contrato, Long)))
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
        CON.Close()
    End Sub

    Private Sub asignacolonia()
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Try
            If IsNumeric(Me.Clv_ColoniaComboBox.SelectedValue) = True Then
                Me.MuestraCVECOLCIUTableAdapter.Connection = CON
                Me.MuestraCVECOLCIUTableAdapter.Fill(Me.NewSofTvDataSet.MuestraCVECOLCIU, Me.Clv_ColoniaComboBox.SelectedValue)
                If opcionlocal = "N" Then Me.Clv_CiudadComboBox.Text = ""
                Me.Clv_CiudadComboBox.Enabled = True
            End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
        CON.Close()
    End Sub

    Private Sub asiganacalle()
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Try
            If IsNumeric(Me.Clv_CalleComboBox.SelectedValue) = True Then
                Me.DAMECOLONIA_CALLETableAdapter.Connection = CON
                Me.DAMECOLONIA_CALLETableAdapter.Fill(Me.NewSofTvDataSet.DAMECOLONIA_CALLE, New System.Nullable(Of Integer)(CType(Me.Clv_CalleComboBox.SelectedValue, Integer)))
                Me.Clv_ColoniaComboBox.Enabled = True
                If opcionlocal = "N" Then Me.Clv_ColoniaComboBox.Text = ""
            End If

        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
        CON.Close()
    End Sub

    Private Sub FrmCAMDO_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.BORDetOrdSer_INTELIGENTETableAdapter.Connection = CON
        Me.BORDetOrdSer_INTELIGENTETableAdapter.Fill(Me.NewSofTvDataSet.BORDetOrdSer_INTELIGENTE, New System.Nullable(Of Long)(CType(GloDetClave, Long)))
        GloBndTrabajo = True
        GloBloqueaDetalle = True
        CON.Close()
    End Sub

    Private Sub FrmCAMDO_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        colorea(Me, Me.Name)
        'TODO: esta l�nea de c�digo carga datos en la tabla 'NewSofTvDataSet.MUESTRACALLES' Puede moverla o quitarla seg�n sea necesario.
        Me.MUESTRACALLESTableAdapter.Connection = CON
        Me.MUESTRACALLESTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRACALLES)
        Me.Clv_ColoniaComboBox.Enabled = False
        Me.Clv_CiudadComboBox.Enabled = False
        Busca()
        If IsNumeric(Me.Clv_OrdenTextBox.Text) = False Then
            opcionlocal = "N"
            Me.CONCAMDOBindingSource.AddNew()
            Me.CONTRATOTextBox.Text = Contrato
            Me.Clv_OrdenTextBox.Text = gloClv_Orden
            Me.CLAVETextBox.Text = GloDetClave


        Else
            opcionlocal = "M"
            Me.DAMECOLONIA_CALLETableAdapter.Connection = CON
            Me.DAMECOLONIA_CALLETableAdapter.Fill(Me.NewSofTvDataSet.DAMECOLONIA_CALLE, Me.Clv_CalleTextBox.Text)
            Me.MuestraCVECOLCIUTableAdapter.Connection = CON
            Me.MuestraCVECOLCIUTableAdapter.Fill(Me.NewSofTvDataSet.MuestraCVECOLCIU, Me.Clv_ColoniaTextBox.Text)
            Busca()
        End If
        If Bloquea = True Or opcion = "M" Then
            Me.CONCAMDOBindingNavigator.Enabled = False
            Me.Panel1.Enabled = False
        End If
        CON.Close()
    End Sub

    Private Sub Clv_CalleComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Clv_CalleComboBox.SelectedIndexChanged
        asiganacalle()
    End Sub

    Private Sub Clv_ColoniaComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Clv_ColoniaComboBox.SelectedIndexChanged
        Me.asignacolonia()
        Try
            Me.uspConsultaTap(0, CInt(Me.Clv_CalleComboBox.SelectedValue), CInt(Me.Clv_ColoniaComboBox.SelectedValue))
            If Clv_ColoniaComboBox.Text.Length > 0 Then
                MUESTRASectorOrdenes(gloClv_Orden, Clv_ColoniaComboBox.SelectedValue)
            End If
        Catch ex As Exception
        End Try
    End Sub

    Private Sub uspConsultaTap(ByVal prmContrato As Long, ByVal prmClvCalle As Integer, ByVal prmClvColonia As Integer)
        Dim CON As New SqlConnection(MiConexion)
        Dim STR As New StringBuilder

        STR.Append("EXEC uspConsultaTap ")
        STR.Append(CStr(prmContrato) & ", ")
        STR.Append(CStr(prmClvCalle) & ", ")
        STR.Append(CStr(prmClvColonia))

        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(STR.ToString, CON)

        Try
            CON.Open()
            DA.Fill(DT)
            Me.cmbTap.DataSource = DT
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub

    Private Sub MUESTRASectorOrdenes(ByVal ClvOrden As Integer, ByVal Clv_Colonia As Integer)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@ORDENSER", SqlDbType.Int, ClvOrden)
        BaseII.CreateMyParameter("@Clv_Colonia", SqlDbType.Int, Clv_Colonia)
        ComboBox1.DataSource = BaseII.ConsultaDT("MUESTRASectorORDENES")
    End Sub

    Private Sub BindingNavigatorDeleteItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BindingNavigatorDeleteItem.Click
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        If IsNumeric(Me.CLAVETextBox.Text) = False Then
            MsgBox("No ahi Datos para eliminar ", MsgBoxStyle.Information)
            Exit Sub
        End If
        If IsNumeric(Me.Clv_OrdenTextBox.Text) = False Then
            MsgBox("No ahi Datos para eliminar ", MsgBoxStyle.Information)
            Exit Sub
        End If
        If IsNumeric(Me.CONTRATOTextBox.Text) = False Then
            MsgBox("No ahi Datos para eliminar ", MsgBoxStyle.Information)
            Exit Sub
        End If
        Me.CONCAMDOTableAdapter.Connection = CON
        Me.CONCAMDOTableAdapter.Delete(Me.CLAVETextBox.Text, Me.Clv_OrdenTextBox.Text, Me.CONTRATOTextBox.Text)
        CON.Close()
        Me.Close()
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        bndCAMDO = True
        Me.Close()
    End Sub

    Private Sub Clv_CiudadComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Clv_CiudadComboBox.SelectedIndexChanged
        If IsNumeric(Me.Clv_CiudadComboBox.SelectedValue) = True Then
            Me.Clv_CiudadComboBox.Enabled = True
        End If
    End Sub

    Private Sub Clv_CiudadTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Clv_CiudadTextBox.TextChanged

    End Sub

    Private Sub Clv_CiudadLabel1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub


    Private Sub CONCAMDOBindingNavigator_RefreshItems(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CONCAMDOBindingNavigator.RefreshItems

    End Sub

    Private Sub CONCAMDOBindingSource_CurrentChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CONCAMDOBindingSource.CurrentChanged

    End Sub
End Class
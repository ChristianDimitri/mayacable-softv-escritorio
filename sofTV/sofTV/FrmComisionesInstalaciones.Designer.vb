﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmComisionesInstalaciones
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.tbRangoInicial = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.tbRangoFinal = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.tbComision = New System.Windows.Forms.TextBox()
        Me.dgvComisiones = New System.Windows.Forms.DataGridView()
        Me.IdComision = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Rango = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Comision = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.bnAgregar = New System.Windows.Forms.Button()
        Me.bnEliminar = New System.Windows.Forms.Button()
        Me.bnSalir = New System.Windows.Forms.Button()
        Me.cbTipSer = New System.Windows.Forms.ComboBox()
        Me.Label4 = New System.Windows.Forms.Label()
        CType(Me.dgvComisiones, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'tbRangoInicial
        '
        Me.tbRangoInicial.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbRangoInicial.Location = New System.Drawing.Point(200, 52)
        Me.tbRangoInicial.Name = "tbRangoInicial"
        Me.tbRangoInicial.Size = New System.Drawing.Size(220, 21)
        Me.tbRangoInicial.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(98, 60)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(96, 15)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Rango Inicial:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(105, 87)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(89, 15)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Rango Final:"
        '
        'tbRangoFinal
        '
        Me.tbRangoFinal.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbRangoFinal.Location = New System.Drawing.Point(200, 79)
        Me.tbRangoFinal.Name = "tbRangoFinal"
        Me.tbRangoFinal.Size = New System.Drawing.Size(220, 21)
        Me.tbRangoFinal.TabIndex = 2
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(123, 114)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(71, 15)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "Comisión:"
        '
        'tbComision
        '
        Me.tbComision.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbComision.Location = New System.Drawing.Point(200, 106)
        Me.tbComision.Name = "tbComision"
        Me.tbComision.Size = New System.Drawing.Size(220, 21)
        Me.tbComision.TabIndex = 3
        '
        'dgvComisiones
        '
        Me.dgvComisiones.AllowUserToAddRows = False
        Me.dgvComisiones.AllowUserToDeleteRows = False
        Me.dgvComisiones.BackgroundColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvComisiones.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvComisiones.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvComisiones.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.IdComision, Me.Rango, Me.Comision})
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvComisiones.DefaultCellStyle = DataGridViewCellStyle3
        Me.dgvComisiones.Location = New System.Drawing.Point(55, 153)
        Me.dgvComisiones.Name = "dgvComisiones"
        Me.dgvComisiones.ReadOnly = True
        Me.dgvComisiones.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvComisiones.Size = New System.Drawing.Size(365, 284)
        Me.dgvComisiones.TabIndex = 6
        Me.dgvComisiones.TabStop = False
        '
        'IdComision
        '
        Me.IdComision.DataPropertyName = "IdComision"
        Me.IdComision.HeaderText = "IdComision"
        Me.IdComision.Name = "IdComision"
        Me.IdComision.ReadOnly = True
        Me.IdComision.Visible = False
        '
        'Rango
        '
        Me.Rango.DataPropertyName = "Rango"
        Me.Rango.HeaderText = "Rango"
        Me.Rango.Name = "Rango"
        Me.Rango.ReadOnly = True
        Me.Rango.Width = 150
        '
        'Comision
        '
        Me.Comision.DataPropertyName = "Comision"
        DataGridViewCellStyle2.Format = "C2"
        DataGridViewCellStyle2.NullValue = Nothing
        Me.Comision.DefaultCellStyle = DataGridViewCellStyle2
        Me.Comision.HeaderText = "Comision"
        Me.Comision.Name = "Comision"
        Me.Comision.ReadOnly = True
        Me.Comision.Width = 150
        '
        'bnAgregar
        '
        Me.bnAgregar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnAgregar.Location = New System.Drawing.Point(426, 104)
        Me.bnAgregar.Name = "bnAgregar"
        Me.bnAgregar.Size = New System.Drawing.Size(75, 23)
        Me.bnAgregar.TabIndex = 4
        Me.bnAgregar.Text = "&Agregar"
        Me.bnAgregar.UseVisualStyleBackColor = True
        '
        'bnEliminar
        '
        Me.bnEliminar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnEliminar.Location = New System.Drawing.Point(426, 153)
        Me.bnEliminar.Name = "bnEliminar"
        Me.bnEliminar.Size = New System.Drawing.Size(75, 23)
        Me.bnEliminar.TabIndex = 5
        Me.bnEliminar.Text = "&Eliminar"
        Me.bnEliminar.UseVisualStyleBackColor = True
        '
        'bnSalir
        '
        Me.bnSalir.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnSalir.Location = New System.Drawing.Point(412, 470)
        Me.bnSalir.Name = "bnSalir"
        Me.bnSalir.Size = New System.Drawing.Size(136, 36)
        Me.bnSalir.TabIndex = 6
        Me.bnSalir.Text = "&SALIR"
        Me.bnSalir.UseVisualStyleBackColor = True
        '
        'cbTipSer
        '
        Me.cbTipSer.DisplayMember = "Concepto"
        Me.cbTipSer.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold)
        Me.cbTipSer.FormattingEnabled = True
        Me.cbTipSer.Location = New System.Drawing.Point(200, 25)
        Me.cbTipSer.Name = "cbTipSer"
        Me.cbTipSer.Size = New System.Drawing.Size(220, 23)
        Me.cbTipSer.TabIndex = 0
        Me.cbTipSer.ValueMember = "Clv_TipSer"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(80, 33)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(114, 15)
        Me.Label4.TabIndex = 11
        Me.Label4.Text = "Tipo de Servicio:"
        '
        'FrmComisionesInstalaciones
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(560, 518)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.cbTipSer)
        Me.Controls.Add(Me.bnSalir)
        Me.Controls.Add(Me.bnEliminar)
        Me.Controls.Add(Me.bnAgregar)
        Me.Controls.Add(Me.dgvComisiones)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.tbComision)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.tbRangoFinal)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.tbRangoInicial)
        Me.Name = "FrmComisionesInstalaciones"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Comisiones de Instalaciones"
        CType(Me.dgvComisiones, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents tbRangoInicial As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents tbRangoFinal As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents tbComision As System.Windows.Forms.TextBox
    Friend WithEvents dgvComisiones As System.Windows.Forms.DataGridView
    Friend WithEvents IdComision As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Rango As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Comision As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents bnAgregar As System.Windows.Forms.Button
    Friend WithEvents bnEliminar As System.Windows.Forms.Button
    Friend WithEvents bnSalir As System.Windows.Forms.Button
    Friend WithEvents cbTipSer As System.Windows.Forms.ComboBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
End Class

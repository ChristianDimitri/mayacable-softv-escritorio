﻿Public Class FrmProrrogas

    Private Sub MUESTRAClientesCandidatosProrroga(ByVal Op As Integer, ByVal Contrato As Integer, ByVal Nombre As String)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Op", SqlDbType.Int, CObj(Op))
        BaseII.CreateMyParameter("@Contrato", SqlDbType.Int, CObj(Contrato))
        BaseII.CreateMyParameter("@Nombre", SqlDbType.VarChar, CObj(Nombre), Nombre.Length)
        dgvClientesCandidatos.DataSource = BaseII.ConsultaDT("MUESTRAClientesCandidatosProrroga")
    End Sub

    Private Sub NUEtbl_Prorrogas(ByVal Contrato As Integer, ByVal FechaProrroga As DateTime)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Contrato", SqlDbType.Int, CObj(Contrato))
        BaseII.CreateMyParameter("@FechaProrroga", SqlDbType.DateTime, CObj(FechaProrroga))
        BaseII.Inserta("NUEtbl_Prorrogas")
    End Sub

    Private Sub DAMEFECHADELSERVIDOR()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Fecha", ParameterDirection.Output, SqlDbType.DateTime)
        BaseII.ProcedimientoOutPut("DAMEFECHADELSERVIDOR")
        dtpFechaProrroga.Value = DateTime.Parse(BaseII.dicoPar("@Fecha").ToString())
        dtpFechaProrroga.MinDate = DateTime.Parse(BaseII.dicoPar("@Fecha").ToString())
    End Sub

    Private Sub FrmProrrogas_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        MUESTRAClientesCandidatosProrroga(0, 0, "")
        DAMEFECHADELSERVIDOR()
    End Sub

    Private Sub bnBuscar_Click(sender As System.Object, e As System.EventArgs) Handles bnBuscar.Click
        If tbContrato.Text.Length = 0 Then tbContrato.Text = "0"
        If IsNumeric(tbContrato.Text) = False Then tbContrato.Text = "0"
        If tbNombre.Text.Length = 0 Then tbNombre.Text = ""

        MUESTRAClientesCandidatosProrroga(1, tbContrato.Text, tbNombre.Text)

    End Sub

    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles Button1.Click
        If dgvClientesCandidatos.Rows.Count = 0 Then
            MessageBox.Show("Selecciona un Cliente.")
            Exit Sub
        End If

        NUEtbl_Prorrogas(dgvClientesCandidatos.SelectedCells(0).Value, dtpFechaProrroga.Value)
        MessageBox.Show("Se agregó con éxito.")
        MUESTRAClientesCandidatosProrroga(0, 0, "")

    End Sub

    Private Sub tbContrato_KeyDown(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles tbContrato.KeyDown
        If e.KeyValue <> 13 Then Exit Sub

        If tbContrato.Text.Length = 0 Then tbContrato.Text = "0"
        If IsNumeric(tbContrato.Text) = False Then tbContrato.Text = "0"
        If tbNombre.Text.Length = 0 Then tbNombre.Text = ""

        MUESTRAClientesCandidatosProrroga(1, tbContrato.Text, tbNombre.Text)

    End Sub

    Private Sub tbNombre_KeyDown(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles tbNombre.KeyDown
        If e.KeyValue <> 13 Then Exit Sub

        If tbContrato.Text.Length = 0 Then tbContrato.Text = "0"
        If IsNumeric(tbContrato.Text) = False Then tbContrato.Text = "0"
        If tbNombre.Text.Length = 0 Then tbNombre.Text = ""

        MUESTRAClientesCandidatosProrroga(1, tbContrato.Text, tbNombre.Text)

    End Sub

    Private Sub bnSalir_Click(sender As System.Object, e As System.EventArgs) Handles bnSalir.Click
        Me.Close()
    End Sub
End Class
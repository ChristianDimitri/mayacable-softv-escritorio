﻿Imports System.Collections.Generic
Public Class FrmCambioDeTipoDeServicio

    Private Sub MuestraServiciosEric(ByVal Clv_TipSer As Integer, ByVal Clv_Servicio As Integer, ByVal Op As Integer)
        Dim dTable As New DataTable
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_TipSer", SqlDbType.Int, Clv_TipSer)
        BaseII.CreateMyParameter("@Clv_Servicio", SqlDbType.Int, Clv_Servicio)
        BaseII.CreateMyParameter("@Op", SqlDbType.Int, Op)
        dTable = BaseII.ConsultaDT("MuestraServiciosEric")
        If Clv_TipSer = 1 Then
            cbServicioTv.DataSource = dTable
        Else
            cbServicioDig.DataSource = dTable
        End If
    End Sub

    Private Sub MUESTRAClientesCambioDeTipoDeServicio(ByVal Op As Integer, ByVal Clv_TipSer As Integer, ByVal Contrato As Integer, ByVal Nombre As String)
        Dim dSet As New DataSet

        Dim l As New List(Of String)
        l.Add("Consulta")
        l.Add("Mensaje")

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Op", SqlDbType.Int, Op)
        BaseII.CreateMyParameter("@Clv_TipSer", SqlDbType.Int, Clv_TipSer)
        BaseII.CreateMyParameter("@Contrato", SqlDbType.Int, Contrato)
        BaseII.CreateMyParameter("@Nombre", SqlDbType.VarChar, Nombre, 250)
        dSet = BaseII.ConsultaDS("MUESTRAClientesCambioDeTipoDeServicio", l)


        eClv_Servicio = 0
        eClv_Servicio = dSet.Tables("Consulta").Rows(0)(2).ToString
        eMsj = ""
        eMsj = dSet.Tables("Mensaje").Rows(0)(0).ToString


        gbServicioDig.Enabled = False
        gbServicioTv.Enabled = False

        If eMsj.Length = 0 Then
            If cbDeTvADig.Checked = True Then
                gbServicioDig.Enabled = True
            Else
                gbServicioTv.Enabled = True
            End If
            bnGuardar.Enabled = True
        Else
            MessageBox.Show(eMsj)
            Limpiar()
        End If

    End Sub

    Private Sub NUECambioDeTipoDeServicio(ByVal Contrato As Integer, ByVal Clv_Usuario As String, ByVal DeTvADig As Boolean, ByVal DeDigATv As Boolean, ByVal Clv_ServicioTV As Integer, ByVal TvSinPago As Integer, ByVal TvConPago As Integer, ByVal Clv_ServicioDig As Integer, ByVal NumeroDeCajas As Integer, ByVal ExtensionesAnalogas As Integer)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Contrato", SqlDbType.Int, Contrato)
        BaseII.CreateMyParameter("@Clv_Usuario", SqlDbType.VarChar, Clv_Usuario, 10)
        BaseII.CreateMyParameter("@DeTvADig", SqlDbType.Bit, DeTvADig)
        BaseII.CreateMyParameter("@DeDigATv", SqlDbType.Bit, DeDigATv)
        BaseII.CreateMyParameter("@Clv_ServicioTV", SqlDbType.Int, Clv_ServicioTV)
        BaseII.CreateMyParameter("@TvSinPago", SqlDbType.Int, TvSinPago)
        BaseII.CreateMyParameter("@TvConPago", SqlDbType.Int, TvConPago)
        BaseII.CreateMyParameter("@Clv_ServicioDig", SqlDbType.Int, Clv_ServicioDig)
        BaseII.CreateMyParameter("@NumeroDeCajas", SqlDbType.Int, NumeroDeCajas)
        BaseII.CreateMyParameter("@ExtensionesAnalogas", SqlDbType.Int, ExtensionesAnalogas)
        BaseII.CreateMyParameter("@Msj", ParameterDirection.Output, SqlDbType.VarChar, 150)
        BaseII.ProcedimientoOutPut("NUECambioDeTipoDeServicio")
        eMsj = ""
        eMsj = BaseII.dicoPar("@Msj").ToString

        eRefrescar = True
        MessageBox.Show(eMsj)
        Limpiar()

    End Sub

    Private Sub Limpiar()
        eContrato = 0
        eClv_TipSer = 1
        eClv_Servicio = 0
        tbContrato.Clear()
        nudCajas.Value = 1
        nudExtensiones.Value = 0
        nudTvSinPago.Value = 0
        nudTvConPago.Value = 0
        gbServicioDig.Enabled = False
        gbServicioTv.Enabled = False
        bnGuardar.Enabled = False
        cbDeTvADig.Checked = True
    End Sub

    Private Sub FrmCambioDeTipoDeServicio_Activated(sender As Object, e As System.EventArgs) Handles Me.Activated
        If eContrato > 0 Then
            tbContrato.Text = eContrato
            eContrato = 0
            If cbDeTvADig.Checked = True Then
                MUESTRAClientesCambioDeTipoDeServicio(1, 1, tbContrato.Text, "")
            Else
                MUESTRAClientesCambioDeTipoDeServicio(1, 3, tbContrato.Text, "")
            End If
        End If
    End Sub

    Private Sub FrmCambioDeTipoDeServicio_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
        Limpiar()
        MuestraServiciosEric(1, 0, 0)
        MuestraServiciosEric(3, 0, 0)
    End Sub

    Private Sub bnBuscar_Click(sender As System.Object, e As System.EventArgs) Handles bnBuscar.Click
        eContrato = 0
        FrmMUESTRAClientesCambioDeTipoDeServicio.Show()
    End Sub

    Private Sub bnGuardar_Click(sender As System.Object, e As System.EventArgs) Handles bnGuardar.Click

        If cbDeTvADig.Checked = True Then
            NUECambioDeTipoDeServicio(tbContrato.Text, GloUsuario, cbDeTvADig.Checked, cbDeDigATv.Checked, eClv_Servicio, 0, 0, cbServicioDig.SelectedValue, nudCajas.Value, nudExtensiones.Value)
        Else
            NUECambioDeTipoDeServicio(tbContrato.Text, GloUsuario, cbDeTvADig.Checked, cbDeDigATv.Checked, cbServicioTv.SelectedValue, nudTvSinPago.Value, nudTvConPago.Value, eClv_Servicio, 0, 0)
        End If

    End Sub

    Private Sub bnSalir_Click(sender As System.Object, e As System.EventArgs) Handles bnSalir.Click
        Me.Close()
    End Sub

    Private Sub cbDeTvADig_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles cbDeTvADig.CheckedChanged
        eClv_TipSer = 1
        If tbContrato.Text.Length = 0 Then Exit Sub
        If IsNumeric(tbContrato.Text) = False Then Exit Sub
        If cbDeTvADig.Checked = True Then
            MUESTRAClientesCambioDeTipoDeServicio(1, 1, tbContrato.Text, "")
        Else
            MUESTRAClientesCambioDeTipoDeServicio(1, 3, tbContrato.Text, "")
        End If
    End Sub

    Private Sub cbDeDigATv_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles cbDeDigATv.CheckedChanged
        eClv_TipSer = 3
        If tbContrato.Text.Length = 0 Then Exit Sub
        If IsNumeric(tbContrato.Text) = False Then Exit Sub
        If cbDeTvADig.Checked = True Then
            MUESTRAClientesCambioDeTipoDeServicio(1, 1, tbContrato.Text, "")
        Else
            MUESTRAClientesCambioDeTipoDeServicio(1, 3, tbContrato.Text, "")
        End If
    End Sub

    Private Sub tbContrato_KeyDown(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles tbContrato.KeyDown
        If e.KeyValue <> 13 Then Exit Sub
        If tbContrato.Text.Length = 0 Then Exit Sub
        If IsNumeric(tbContrato.Text) = False Then Exit Sub
        If cbDeTvADig.Checked = True Then
            MUESTRAClientesCambioDeTipoDeServicio(1, 1, tbContrato.Text, "")
        Else
            MUESTRAClientesCambioDeTipoDeServicio(1, 3, tbContrato.Text, "")
        End If
    End Sub
End Class
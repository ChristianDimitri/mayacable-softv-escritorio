Public Class FrmSeguridad

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Validar()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub

    Private Sub Validar()

        If TextBox1.Text.Length = 0 Then
            MessageBox.Show("Tecle� la contrase�a.")
            Exit Sub
        End If

        If GloTipoUsuario = 40 Then
            Dim resp As String
            resp = Me.TextBox1.Text
            If resp = "softvautomac" Then
                If opcFrm = 17 Then
                    FrmMiMenu.CierredeMesSA()
                    Me.Close()
                ElseIf opcFrm = 16 Then
                    FrmMiMenu.desconexionSA()
                    Me.Close()
                ElseIf opcFrm = 26 Then
                    FrmMiMenu.desconexionSAInternet()
                    Me.Close()
                ElseIf opcFrm = 20 Then
                    opcFrm = 0
                    FrmDesconexionSeleccionCiudades.Show()
                    Me.Close()
                End If
            Else
                MsgBox("No tiene acceso.", MsgBoxStyle.Information)
                Me.Close()
            End If
        Else
            MsgBox("No tiene acceso.")
            Me.Close()
        End If

    End Sub

    Private Sub TextBox1_KeyDown(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles TextBox1.KeyDown
        If e.KeyValue <> 13 Then Exit Sub
        Validar()
    End Sub

    Private Sub FrmSeguridad_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
    End Sub
End Class

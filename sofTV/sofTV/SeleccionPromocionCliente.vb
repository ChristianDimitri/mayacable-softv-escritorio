Imports System.Data.SqlClient
Public Class SeleccionPromocionCliente

    Private Sub SeleccionPromocionCliente_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        Muestra()
    End Sub
    Private Sub Guarda(ByVal clv_promocion As Integer, ByVal NoMeses As String, ByVal Inicial As String, ByVal RestoMeses As String, ByVal PlazoForzoso As String)
        Dim CON2 As New SqlConnection(MiConexion)
        Dim cmd As New SqlClient.SqlCommand()
        Dim BndTipoPromocion As Boolean = False
        Dim Meses As Integer = Nothing

        If IsNumeric(NoMeses) = True Then
            BndTipoPromocion = True
            Meses = CInt(NoMeses)
        ElseIf IsNumeric(NoMeses) = False Then
            If IsNumeric(RestoMeses) = True Then
                BndTipoPromocion = False
                Meses = CInt(RestoMeses)
            End If
        End If

        If Meses = Nothing Then
            Meses = 0
        End If

        Try
            cmd = New SqlCommand()
            CON2.Open()
            With cmd
                .CommandText = "Guarda_Promocion_Cte"
                .Connection = CON2
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure
                '@clv_promocion bigint,@contrato bigint,@Bnd_Tipo_Promocion bit,@Inicial int,@RestoMeses int,@clv_servicio bigint,@clv_tipser int
                Dim prm As New SqlParameter("@clv_promocion", SqlDbType.BigInt)
                prm.Direction = ParameterDirection.Input
                prm.Value = clv_promocion
                .Parameters.Add(prm)

                Dim prm1 As New SqlParameter("@contrato", SqlDbType.BigInt)
                prm1.Direction = ParameterDirection.Input
                prm1.Value = LocContratoLog
                .Parameters.Add(prm1)

                Dim prm2 As New SqlParameter("@Bnd_Tipo_Promocion", SqlDbType.Bit)
                prm2.Direction = ParameterDirection.Input
                prm2.Value = BndTipoPromocion
                .Parameters.Add(prm2)

                Dim prm3 As New SqlParameter("@Inicial", SqlDbType.Int)
                prm3.Direction = ParameterDirection.Input
                If IsNumeric(Inicial) = True Then
                    prm3.Value = CInt(Inicial)
                ElseIf IsNumeric(Inicial) = False Then
                    prm3.Value = 0
                End If
                .Parameters.Add(prm3)

                Dim prm4 As New SqlParameter("@RestoMeses", SqlDbType.Int)
                prm4.Direction = ParameterDirection.Input
                prm4.Value = Meses
                .Parameters.Add(prm4)
                '@Plazo_Forzoso int

                Dim prm7 As New SqlParameter("@Plazo_Forzoso", SqlDbType.Int)
                prm7.Direction = ParameterDirection.Input
                prm7.Value = CInt(PlazoForzoso)
                .Parameters.Add(prm7)

                Dim prm5 As New SqlParameter("@clv_servicio", SqlDbType.BigInt)
                prm5.Direction = ParameterDirection.Input
                prm5.Value = locGloClv_servicioPromocion
                .Parameters.Add(prm5)

                Dim prm6 As New SqlParameter("@clv_tipser", SqlDbType.Int)
                prm6.Direction = ParameterDirection.Input
                If Gloclv_servicioPromocion = 3 Then
                    Gloclv_servicioPromocion = 5
                End If
                prm6.Value = Gloclv_servicioPromocion
                .Parameters.Add(prm6)
                Dim i As Integer = .ExecuteNonQuery()
            End With
            CON2.Close()
            MsgBox("Se Guardo Con Exito", MsgBoxStyle.Information)
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub Muestra()
        Dim CON As New SqlConnection(MiConexion)
        Dim cmd As New SqlCommand()
        Dim error1 As Integer = Nothing
        Try
            cmd = New SqlClient.SqlCommand()
            CON.Open()
            With cmd
                .CommandText = "Checa_si_Aplica_promocion"
                .Connection = CON
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure

                '@contrato bigint,@Muestra int,@error int output,@opcion_checar int,@clv_servicio bigint)
                Dim prm As New SqlParameter("@contrato", SqlDbType.BigInt)
                prm.Direction = ParameterDirection.Input
                prm.Value = LocContratoLog
                .Parameters.Add(prm)

                Dim prm1 As New SqlParameter("@Muestra", SqlDbType.Int)
                prm1.Direction = ParameterDirection.Input
                prm1.Value = 1
                .Parameters.Add(prm1)

                Dim prm2 As New SqlParameter("@error", SqlDbType.Int)
                prm2.Direction = ParameterDirection.Output
                prm2.Value = 0
                .Parameters.Add(prm2)

                Dim prm3 As New SqlParameter("@opcion_checar", SqlDbType.Int)
                prm3.Direction = ParameterDirection.Input
                prm3.Value = Gloclv_servicioPromocion
                .Parameters.Add(prm3)

                Dim prm4 As New SqlParameter("@clv_servicio", SqlDbType.BigInt)
                prm4.Direction = ParameterDirection.Input
                prm4.Value = locGloClv_servicioPromocion
                .Parameters.Add(prm4)

                Dim lector As SqlDataReader = .ExecuteReader()
                error1 = prm2.Value

                Me.DGVPromociones.Rows.Clear()
                While lector.Read()
                    Me.DGVPromociones.Rows.Add(lector(0), lector(1), lector(2), lector(3), lector(4), lector(5))
                End While
            End With
            CON.Close()
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub CMBBtnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CMBBtnSalir.Click
        Me.Close()
    End Sub

    Private Sub CMBBtnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CMBBtnAceptar.Click
        If IsNumeric(Me.DGVPromociones.SelectedCells(0).Value) = True Then
            Guarda(Me.DGVPromociones.SelectedCells(0).Value, Me.DGVPromociones.SelectedCells(2).Value, Me.DGVPromociones.SelectedCells(3).Value, Me.DGVPromociones.SelectedCells(4).Value, Me.DGVPromociones.SelectedCells(5).Value)
        End If
        Me.Close()
    End Sub
End Class
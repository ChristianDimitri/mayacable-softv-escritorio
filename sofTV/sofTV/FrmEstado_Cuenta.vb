Imports System.Data.SqlClient
Public Class FrmEstado_Cuenta

    Public Sub Borrar()
        Dim consoftv3 As New SqlConnection(MiConexion)
        consoftv3.Open()
        Me.Borrar_Session_ServiciosTableAdapter.Connection = consoftv3
        Me.Borrar_Session_ServiciosTableAdapter.Fill(Me.DataSetarnoldo.Borrar_Session_Servicios, LocClv_session)
        Me.Borra_temporalesTableAdapter.Connection = consoftv3
        Me.Borra_temporalesTableAdapter.Fill(Me.ProcedimientosArnoldo2.Borra_temporales, LocClv_session)
        consoftv3.Close()
    End Sub


    Private Sub FrmEstado_Cuenta_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        If LEdo_Cuenta = True Then
            Me.CMBPanel1.Show()
            Me.CMBPanel3.Hide()
        ElseIf LEdo_Cuenta2 = True Then
            Me.CMBPanel1.Hide()
            Me.CMBPanel3.Show()
            Me.Text = "Impresi�n de Listado de Recuperaci�n para Cartera"
            Me.Button5.Text = "Imprimir Listado "
        End If
        LocBndB = False
        LocBndI = False
        LocBndD = False
        LocBndS = False
        LocBndF = False
        LocBndDT = False
        LocBndC = False
        Dim conex As New SqlConnection(MiConexion)
        conex.Open()
        Me.Muestra_MesesTableAdapter.Connection = conex
        Me.Muestra_MesesTableAdapter.Fill(Me.ProcedimientosArnoldo2.Muestra_Meses)
        conex.Close()
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Dim COn As New SqlConnection(MiConexion)

        If (Me.CMBCheckBox1.Checked = False And Me.CMBCheckBox3.Checked = False And LEdo_Cuenta = True) Or (Me.CheckBox1.Checked = False And Me.CheckBox2.Checked = False And Me.CheckBox3.Checked = False And LEdo_Cuenta2 = True) Then
            MsgBox("Primero selecciona m�nimo un tipo de Status", MsgBoxStyle.Information)
        Else
            If Me.CMBCheckBox8.CheckState = CheckState.Checked Then
                If IsNumeric(Me.TextBox1.Text) = True Then
                    If Me.CMBCheckBox8.CheckState = CheckState.Checked And (Len(Me.TextBox1.Text) = 0 Or Me.TextBox1.Text = "") Then
                        MsgBox("No Se Ha Introducido un A�o", MsgBoxStyle.Information)
                        Exit Sub
                    Else
                        If Me.CaracterComboBox.Text = "" Then
                            MsgBox("No Se Ha Seleccionado El Rango de B�squeda", MsgBoxStyle.Information)
                            Exit Sub
                        Else
                            COn.Open()
                            Me.InsertaRel_Reporte_Ciudad_Ultimo_mesTableAdapter.Connection = COn
                            Me.InsertaRel_Reporte_Ciudad_Ultimo_mesTableAdapter.Fill(Me.ProcedimientosArnoldo2.InsertaRel_Reporte_Ciudad_Ultimo_mes, LocClv_session, CInt(Me.ComboBox1.SelectedValue), CInt(Me.TextBox1.Text), Me.CaracterComboBox.SelectedValue)
                            COn.Close()
                        End If
                    End If
                Else
                    MsgBox("No Se Ha Introducido un A�o", MsgBoxStyle.Information)
                    Exit Sub
                End If
            End If

            If LocValidaHab = 0 Then
                ' If LocOp = 20 Then
                'Comienzo_Reporte()
                bec_bnd = True
                FrmSelPeriodo.Show()
                Me.Close()
                'End If
            End If
        End If

    End Sub
    Public Sub Comienzo_Reporte()

        Dim ConSoftv As New SqlConnection(MiConexion)

        If LocClv_session > 0 Then
            ConSoftv.Open()
            Me.Borrar_Session_ServiciosTableAdapter.Connection = ConSoftv
            Me.Borrar_Session_ServiciosTableAdapter.Fill(Me.DataSetarnoldo.Borrar_Session_Servicios, LocClv_session)
            Me.Borra_temporalesTableAdapter.Connection = ConSoftv
            Me.Borra_temporalesTableAdapter.Fill(Me.ProcedimientosArnoldo2.Borra_temporales, LocClv_session)
            Me.Borra_Rel_Reportes_SoloInternetTableAdapter.Connection = ConSoftv
            Me.Borra_Rel_Reportes_SoloInternetTableAdapter.Fill(Me.ProcedimientosArnoldo2.Borra_Rel_Reportes_SoloInternet, LocClv_session)
            Me.DameClv_Session_ServiciosTableAdapter.Connection = ConSoftv
            Me.DameClv_Session_ServiciosTableAdapter.Fill(Me.DataSetarnoldo.DameClv_Session_Servicios, LocClv_session)
            ConSoftv.Close()
        Else
            ConSoftv.Open()
            Me.DameClv_Session_ServiciosTableAdapter.Connection = ConSoftv
            Me.DameClv_Session_ServiciosTableAdapter.Fill(Me.DataSetarnoldo.DameClv_Session_Servicios, LocClv_session)
            Me.Inserta_Rel_Reportes_SoloInternetTableAdapter.Connection = ConSoftv
            Me.Inserta_Rel_Reportes_SoloInternetTableAdapter.Fill(Me.ProcedimientosArnoldo2.Inserta_Rel_Reportes_SoloInternet, LocClv_session, 1)
            ConSoftv.Close()
        End If

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim consoftv2 As New SqlConnection(MiConexion)
        consoftv2.Open()
        Me.Borrar_Session_ServiciosTableAdapter.Connection = consoftv2
        Me.Borrar_Session_ServiciosTableAdapter.Fill(Me.DataSetarnoldo.Borrar_Session_Servicios, LocClv_session)
        Me.Borra_temporalesTableAdapter.Connection = consoftv2
        Me.Borra_temporalesTableAdapter.Fill(Me.ProcedimientosArnoldo2.Borra_temporales, LocClv_session)
        consoftv2.Close()
        LEdo_Cuenta = False
        LEdo_Cuenta2 = False
        Me.Close()
    End Sub


    Private Sub CMBCheckBox8_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CMBCheckBox8.CheckedChanged
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        If Me.CMBCheckBox8.CheckState = CheckState.Checked Then
            Me.CMBPanel2.Enabled = True
            Me.Muestra_CaracteresTableAdapter.Connection = CON
            Me.Muestra_CaracteresTableAdapter.Fill(Me.ProcedimientosArnoldo2.Muestra_Caracteres, 0)
            Me.CaracterComboBox.Text = ""
        Else
            Me.CMBPanel2.Enabled = False
        End If
        CON.Close()
    End Sub

    Private Sub CMBCheckBox1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CMBCheckBox1.CheckedChanged
        If Me.CMBCheckBox1.CheckState = CheckState.Checked Then
            LocBndC = True
        Else
            LocBndC = False
        End If
    End Sub

    Private Sub CMBCheckBox3_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CMBCheckBox3.CheckedChanged
        If Me.CMBCheckBox3.CheckState = CheckState.Checked Then
            LocBndI = True
        Else
            LocBndI = False
        End If
    End Sub

   
    Private Sub CheckBox2_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox2.CheckedChanged
        If Me.CheckBox2.CheckState = CheckState.Checked Then
            LocBndD = True
        Else
            LocBndD = False
        End If
    End Sub

    Private Sub CheckBox3_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox3.CheckedChanged
        If Me.CheckBox3.CheckState = CheckState.Checked Then
            LocBndS = True
        Else
            LocBndS = False
        End If
    End Sub

    Private Sub CheckBox1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox1.CheckedChanged
        If Me.CheckBox3.CheckState = CheckState.Checked Then
            LocBndDT = True
        Else
            LocBndDT = False
        End If
    End Sub
End Class
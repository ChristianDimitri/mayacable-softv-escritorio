﻿Public Class FrmComisionesCobroADomicilio

    Private Sub CONtbl_ComisionesCobroADomicilio()
        BaseII.limpiaParametros()
        dgvComisiones.DataSource = BaseII.ConsultaDT("CONtbl_ComisionesCobroADomicilio")
    End Sub

    Private Sub NUEtbl_ComisionesCobroADomicilio(ByVal RangoIni As Integer, RangoFin As Integer, ByVal Comision As Decimal)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@RangoIni", SqlDbType.Int, CObj(RangoIni))
        BaseII.CreateMyParameter("@RangoFin", SqlDbType.Int, CObj(RangoFin))
        BaseII.CreateMyParameter("@Comision", SqlDbType.Decimal, CObj(Comision))
        BaseII.CreateMyParameter("@Msj", ParameterDirection.Output, SqlDbType.VarChar, 150)
        BaseII.ProcedimientoOutPut("NUEtbl_ComisionesCobroADomicilio")
        eMsj = String.Empty
        eMsj = BaseII.dicoPar("@Msj").ToString()

        If eMsj.Length > 0 Then
            MessageBox.Show(eMsj)
        Else
            CONtbl_ComisionesCobroADomicilio()
        End If

    End Sub

    Private Sub BORtbl_ComisionesCobroADomicilio(ByVal Id As Integer)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Id", SqlDbType.Int, CObj(Id))
        BaseII.Inserta("BORtbl_ComisionesCobroADomicilio")

    End Sub

    Private Sub bnAgregar_Click(sender As System.Object, e As System.EventArgs) Handles bnAgregar.Click

        If tbRangoIni.Text.Length = 0 Then
            MessageBox.Show("Captura el rango inicial.")
            Exit Sub
        End If

        If IsNumeric(tbRangoIni.Text) = False Then
            MessageBox.Show("Captura un rango inicial válido.")
            tbRangoIni.Clear()
            Exit Sub
        End If

        If tbRangoFin.Text.Length = 0 Then
            MessageBox.Show("Captura el rango final.")
            Exit Sub
        End If

        If IsNumeric(tbRangoFin.Text) = False Then
            MessageBox.Show("Captura un rango final válido.")
            Exit Sub
        End If

        If tbComision.Text.Length = 0 Then
            MessageBox.Show("Captura una comisión.")
            Exit Sub
        End If

        If IsNumeric(tbComision.Text) = False Then
            MessageBox.Show("Captura una comisión válida.")
            Exit Sub
        End If

        NUEtbl_ComisionesCobroADomicilio(tbRangoIni.Text, tbRangoFin.Text, tbComision.Text)
        CONtbl_ComisionesCobroADomicilio()

        tbRangoIni.Clear()
        tbRangoFin.Clear()
        tbComision.Clear()


    End Sub


    Private Sub bnEliminar_Click(sender As System.Object, e As System.EventArgs) Handles bnEliminar.Click

        If dgvComisiones.RowCount = 0 Then
            MessageBox.Show("Selecciona una comisión a eliminar.")
            Exit Sub
        End If

        BORtbl_ComisionesCobroADomicilio(dgvComisiones.SelectedCells(0).Value)
        CONtbl_ComisionesCobroADomicilio()

    End Sub


    Private Sub bnSalir_Click(sender As System.Object, e As System.EventArgs) Handles bnSalir.Click
        Me.Close()
    End Sub

    Private Sub FrmComisionesCobroADomicilio_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
        CONtbl_ComisionesCobroADomicilio()
    End Sub
End Class
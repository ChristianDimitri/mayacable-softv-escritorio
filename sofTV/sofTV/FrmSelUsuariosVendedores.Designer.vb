﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmSelUsuariosVendedores
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.bnSalir = New System.Windows.Forms.Button()
        Me.bnAceptar = New System.Windows.Forms.Button()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.dgvIzquierda = New System.Windows.Forms.DataGridView()
        Me.CLV_GRUPOI = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CLAVEI = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NOMBREI = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgvDerecha = New System.Windows.Forms.DataGridView()
        Me.CLV_GRUPOD = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CLAVED = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NOMBRED = New System.Windows.Forms.DataGridViewTextBoxColumn()
        CType(Me.dgvIzquierda, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvDerecha, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'bnSalir
        '
        Me.bnSalir.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bnSalir.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnSalir.Location = New System.Drawing.Point(644, 405)
        Me.bnSalir.Name = "bnSalir"
        Me.bnSalir.Size = New System.Drawing.Size(136, 36)
        Me.bnSalir.TabIndex = 19
        Me.bnSalir.Text = "&SALIR"
        Me.bnSalir.UseVisualStyleBackColor = True
        '
        'bnAceptar
        '
        Me.bnAceptar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bnAceptar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnAceptar.Location = New System.Drawing.Point(486, 405)
        Me.bnAceptar.Name = "bnAceptar"
        Me.bnAceptar.Size = New System.Drawing.Size(136, 36)
        Me.bnAceptar.TabIndex = 18
        Me.bnAceptar.Text = "&ACEPTAR"
        Me.bnAceptar.UseVisualStyleBackColor = True
        '
        'Button4
        '
        Me.Button4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button4.Location = New System.Drawing.Point(362, 284)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(64, 25)
        Me.Button4.TabIndex = 17
        Me.Button4.Text = "<<"
        Me.Button4.UseVisualStyleBackColor = True
        '
        'Button3
        '
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.Location = New System.Drawing.Point(362, 253)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(64, 25)
        Me.Button3.TabIndex = 16
        Me.Button3.Text = "<"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.Location = New System.Drawing.Point(362, 130)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(64, 25)
        Me.Button2.TabIndex = 15
        Me.Button2.Text = ">>"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Button1
        '
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(362, 99)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(64, 25)
        Me.Button1.TabIndex = 14
        Me.Button1.Text = ">"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'dgvIzquierda
        '
        Me.dgvIzquierda.AllowUserToAddRows = False
        Me.dgvIzquierda.AllowUserToDeleteRows = False
        Me.dgvIzquierda.BackgroundColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvIzquierda.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvIzquierda.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvIzquierda.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.CLV_GRUPOI, Me.CLAVEI, Me.NOMBREI})
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvIzquierda.DefaultCellStyle = DataGridViewCellStyle2
        Me.dgvIzquierda.Location = New System.Drawing.Point(33, 23)
        Me.dgvIzquierda.Name = "dgvIzquierda"
        Me.dgvIzquierda.ReadOnly = True
        Me.dgvIzquierda.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvIzquierda.Size = New System.Drawing.Size(298, 364)
        Me.dgvIzquierda.TabIndex = 20
        '
        'CLV_GRUPOI
        '
        Me.CLV_GRUPOI.DataPropertyName = "CLV_GRUPO"
        Me.CLV_GRUPOI.HeaderText = "CLV_GRUPO"
        Me.CLV_GRUPOI.Name = "CLV_GRUPOI"
        Me.CLV_GRUPOI.ReadOnly = True
        Me.CLV_GRUPOI.Visible = False
        '
        'CLAVEI
        '
        Me.CLAVEI.DataPropertyName = "CLAVE"
        Me.CLAVEI.HeaderText = "CLAVE"
        Me.CLAVEI.Name = "CLAVEI"
        Me.CLAVEI.ReadOnly = True
        Me.CLAVEI.Visible = False
        '
        'NOMBREI
        '
        Me.NOMBREI.DataPropertyName = "NOMBRE"
        Me.NOMBREI.HeaderText = "Usuarios/Vendedores"
        Me.NOMBREI.Name = "NOMBREI"
        Me.NOMBREI.ReadOnly = True
        Me.NOMBREI.Width = 230
        '
        'dgvDerecha
        '
        Me.dgvDerecha.AllowUserToAddRows = False
        Me.dgvDerecha.AllowUserToDeleteRows = False
        Me.dgvDerecha.BackgroundColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvDerecha.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.dgvDerecha.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvDerecha.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.CLV_GRUPOD, Me.CLAVED, Me.NOMBRED})
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvDerecha.DefaultCellStyle = DataGridViewCellStyle4
        Me.dgvDerecha.Location = New System.Drawing.Point(459, 23)
        Me.dgvDerecha.Name = "dgvDerecha"
        Me.dgvDerecha.ReadOnly = True
        Me.dgvDerecha.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvDerecha.Size = New System.Drawing.Size(298, 364)
        Me.dgvDerecha.TabIndex = 21
        '
        'CLV_GRUPOD
        '
        Me.CLV_GRUPOD.DataPropertyName = "CLV_GRUPO"
        Me.CLV_GRUPOD.HeaderText = "CLV_GRUPO"
        Me.CLV_GRUPOD.Name = "CLV_GRUPOD"
        Me.CLV_GRUPOD.ReadOnly = True
        Me.CLV_GRUPOD.Visible = False
        '
        'CLAVED
        '
        Me.CLAVED.DataPropertyName = "CLAVE"
        Me.CLAVED.HeaderText = "CLAVE"
        Me.CLAVED.Name = "CLAVED"
        Me.CLAVED.ReadOnly = True
        Me.CLAVED.Visible = False
        '
        'NOMBRED
        '
        Me.NOMBRED.DataPropertyName = "NOMBRE"
        Me.NOMBRED.HeaderText = "Usuarios/Vendedores"
        Me.NOMBRED.Name = "NOMBRED"
        Me.NOMBRED.ReadOnly = True
        Me.NOMBRED.Width = 230
        '
        'FrmSelUsuariosVendedores
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(792, 453)
        Me.Controls.Add(Me.dgvDerecha)
        Me.Controls.Add(Me.dgvIzquierda)
        Me.Controls.Add(Me.bnSalir)
        Me.Controls.Add(Me.bnAceptar)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Name = "FrmSelUsuariosVendedores"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Selecciona Usuarios y/o Vendedores"
        CType(Me.dgvIzquierda, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvDerecha, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents bnSalir As System.Windows.Forms.Button
    Friend WithEvents bnAceptar As System.Windows.Forms.Button
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents dgvIzquierda As System.Windows.Forms.DataGridView
    Friend WithEvents CLV_GRUPOI As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CLAVEI As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NOMBREI As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgvDerecha As System.Windows.Forms.DataGridView
    Friend WithEvents CLV_GRUPOD As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CLAVED As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NOMBRED As System.Windows.Forms.DataGridViewTextBoxColumn
End Class

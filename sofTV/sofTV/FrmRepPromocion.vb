﻿Public Class FrmRepPromocion

    Private Sub dtFechaIni_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtFechaIni.ValueChanged
        dtFechaFin.MinDate = dtFechaIni.Value
    End Sub

    Private Sub dtFechaFin_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtFechaFin.ValueChanged
        dtFechaIni.MaxDate = dtFechaFin.Value
    End Sub

    Private Sub btAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btAceptar.Click
        eFechaIni = dtFechaIni.Value
        eFechaFin = dtFechaFin.Value
        eC = cbC.Checked
        eI = cbI.Checked
        eD = cbD.Checked
        eS = cbS.Checked
        eB = cbB.Checked
        eF = cbF.Checked
    End Sub
End Class
Imports System.Data.SqlClient
Public Class BrwServiciosDig

    Private Sub Busca(ByVal opt As Integer)
        Dim cone As New SqlConnection(MiConexion)
        Dim par As Integer
        Dim par2 As String = Nothing
        Try
            If IsNumeric(Me.TextClave.Text) = False Then par = 0 Else par = Me.TextClave.Text
            If Len(Me.TextBox2.Text) = 0 Then par2 = 0 Else par2 = Me.TextBox2.Text
          
          
            cone.Open()
            Me.BUSCASERVICIOS_DIGITALESTableAdapter.Connection = cone
            Me.BUSCASERVICIOS_DIGITALESTableAdapter.Fill(Me.DataSetLidia2.BUSCASERVICIOS_DIGITALES, par, par2, 0, opt)
            cone.Close()
            Me.TextClave.Text = ""
            Me.TextBox2.Text = ""
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub BrwServiciosDig_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        If bec_bnd = True Then
            bec_bnd = False
            Busca(3)
        End If
    End Sub

    Private Sub BrwServiciosDig_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        Busca(3)
    End Sub

    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        If TextClave.Text = "" Then
            Busca(3)
        Else
            Busca(0)
        End If
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If TextBox2.Text = "" Then
            Busca(3)
        Else
            Busca(1)
        End If
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        Busca(2)
    End Sub

    'Private Sub MaskedTextBox2_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
    '   If (Asc(e.KeyChar) = 13) Then
    '      Busca(0)
    ' End If
    'End Sub

    Private Sub TextBox2_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox2.KeyPress
        If Asc(e.KeyChar) = 13 Then
            If TextBox2.Text = "" Then
                Busca(3)
            Else
                Busca(1)
            End If
        End If
    End Sub
    Private Sub TextClave_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextClave.KeyPress
        Dim KeyAscii As Short = CShort(Asc(e.KeyChar))
        KeyAscii = CShort(FrmMarcaciones.SoloNumeros(KeyAscii))
        If KeyAscii = 0 Then
            e.Handled = True
        End If
        If Asc(e.KeyChar) = 13 Then
            If TextClave.Text = "" Then
                Busca(3)
            Else
                Busca(0)
            End If
        End If

    End Sub

    Private Sub MaskedTextBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MaskedTextBox1.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Busca(2)
        End If
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        'If IsNumeric(Me.Clv_tipo_paquete_AdiocionalLabel1.Text) = True Then
        'gloclv_servicioDigital = Me.Clv_tipo_paquete_AdiocionalLabel1.Text
        opcion = "N"
        FrmServiciosDigitales.Show()
        'End If
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click

        'If IsNumeric(Me.Clv_tipo_paquete_AdiocionalLabel1.Text) = True Then
        gloclv_servicioDigital = Me.Clv_tipo_paquete_AdiocionalLabel1.Text
        opcion = "C"
        FrmServiciosDigitales.Show()
        'End If
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        'If IsNumeric(Me.Clv_tipo_paquete_AdiocionalLabel1.Text) = True Then
        gloclv_servicioDigital = Me.Clv_tipo_paquete_AdiocionalLabel1.Text
        opcion = "M"
        FrmServiciosDigitales.Show()
        ' End If
    End Sub

End Class
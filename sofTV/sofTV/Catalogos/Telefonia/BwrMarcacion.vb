Imports System.Diagnostics
Imports System.Data.SqlClient
Public Class BwrMarcacion
    Dim Trazado As TraceListener
    Public KeyAscii As Short
    Function SoloNumeros(ByVal Keyascii As Short) As Short
        If InStr("1234567890", Chr(Keyascii)) = 0 Then
            SoloNumeros = 0
        Else
            SoloNumeros = Keyascii
        End If
        Select Case Keyascii
            Case 8
                SoloNumeros = Keyascii
            Case 13
                SoloNumeros = Keyascii
        End Select
    End Function
    Private Sub Busca(ByVal opc As Integer)
        Dim ConBus As New SqlConnection(MiConexion)

        Select Case opc
            Case 0
                ConBus.Open()
                Me.Busca_MarcacionesTableAdapter.Connection = ConBus
                Me.Busca_MarcacionesTableAdapter.Fill(Me.DataSetLidia2.Busca_Marcaciones, 0, 0, 0, opc)
                ConBus.Close()
            Case 1
                ConBus.Open()
                Me.Busca_MarcacionesTableAdapter.Connection = ConBus
                Me.Busca_MarcacionesTableAdapter.Fill(Me.DataSetLidia2.Busca_Marcaciones, Me.TextBox1.Text, 0, 0, opc)
                ConBus.Close()
                Me.TextBox1.Text = ""
            Case 2
                ConBus.Open()
                Me.Busca_MarcacionesTableAdapter.Connection = ConBus
                Me.Busca_MarcacionesTableAdapter.Fill(Me.DataSetLidia2.Busca_Marcaciones, 0, Me.TextBox2.Text, 0, opc)
                ConBus.Close()
                Me.TextBox2.Text = ""
            Case 4
                ConBus.Open()
                Me.Busca_MarcacionesTableAdapter.Connection = ConBus
                Me.Busca_MarcacionesTableAdapter.Fill(Me.DataSetLidia2.Busca_Marcaciones, 0, 0, 0, opc)
                ConBus.Close()
                Me.TextBox2.Text = ""
        End Select


    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub BwrMarcacion_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        If bec_bnd = True Then
            bec_bnd = False
            Busca(4)
        End If
    End Sub

    Private Sub BwrMarcacion_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        Busca(4)
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        opcion = "N" 
        FrmMarcaciones.Show()
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        opcion = "C"
        If (IsNumeric(Me.Clv_calleLabel2.Text) = True) Then
            Clv_Marcacion = Me.Clv_calleLabel2.Text
            FrmMarcaciones.Show()
        End If
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        opcion = "M"
        If (IsNumeric(Me.Clv_calleLabel2.Text) = True) Then
            Clv_Marcacion = Me.Clv_calleLabel2.Text
            FrmMarcaciones.Show()
        End If
    End Sub

    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        If Len(TextBox1.Text) > 0 Then
            Busca(1)
        ElseIf Len(TextBox1.Text) = 0 Then
            Busca(4)
        End If
    End Sub

    Private Sub TextBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox1.KeyPress
        If Asc(e.KeyChar) = 13 Then
            If Len(TextBox1.Text) > 0 Then
                Busca(1)
            ElseIf Len(TextBox1.Text) = 0 Then
                Busca(4)
            End If
        End If
        Dim KeyAscii As Short = CShort(Asc(e.KeyChar))
        KeyAscii = CShort(SoloNumeros(KeyAscii))
        If KeyAscii = 0 Then
            e.Handled = True
        End If
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If Len(TextBox2.Text) > 0 Then
            Busca(2)
        ElseIf Len(TextBox2.Text) = 0 Then
            Busca(4)
        End If
    End Sub

    Private Sub TextBox2_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox2.KeyPress
        If Asc(e.KeyChar) = 13 Then
            If Len(TextBox2.Text) > 0 Then
                Busca(2)
            ElseIf Len(TextBox2.Text) = 0 Then
                Busca(4)
            End If
        End If
        Dim KeyAscii As Short = CShort(Asc(e.KeyChar))
        KeyAscii = CShort(SoloNumeros(KeyAscii))
        If KeyAscii = 0 Then
            e.Handled = True
        End If
    End Sub

    Private Sub DataGridView1_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles DataGridView1.DoubleClick
        opcion = "C"
        If (IsNumeric(Me.Clv_calleLabel2.Text) = True) Then
            Clv_Marcacion = Me.Clv_calleLabel2.Text
            FrmMarcaciones.Show()
        End If
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click

    End Sub
End Class
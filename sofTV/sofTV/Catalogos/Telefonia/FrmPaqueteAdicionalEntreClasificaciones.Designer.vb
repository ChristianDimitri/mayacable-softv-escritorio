﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmPaqueteAdicionalEntreClasificaciones
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim CMBPaqueteAdicionalLabel As System.Windows.Forms.Label
        Dim Clv_PaqAdiLabel As System.Windows.Forms.Label
        Dim CMBContratacionLabel1 As System.Windows.Forms.Label
        Dim CMBMensualidadLabel1 As System.Windows.Forms.Label
        Dim CMBDescripcionLabel1 As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmPaqueteAdicionalEntreClasificaciones))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.panelGeneralesPaqAdic = New System.Windows.Forms.Panel()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.chbxAplicaTarifa_PaqAdic = New System.Windows.Forms.CheckBox()
        Me.CMBLabel4 = New System.Windows.Forms.Label()
        Me.txtCosto = New System.Windows.Forms.TextBox()
        Me.txtNombrePaqAdic = New System.Windows.Forms.TextBox()
        Me.CMBlblTipoClasificacion = New System.Windows.Forms.Label()
        Me.NumeroTextBox = New System.Windows.Forms.TextBox()
        Me.btnEliminaPaqAdic = New System.Windows.Forms.Button()
        Me.btnGuardarPaqAdic = New System.Windows.Forms.Button()
        Me.NumericUpDown1 = New System.Windows.Forms.NumericUpDown()
        Me.txtIDPaqAdic = New System.Windows.Forms.TextBox()
        Me.lblTitulo = New System.Windows.Forms.Label()
        Me.panelRelPlanTarifario = New System.Windows.Forms.Panel()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.pbAyuda01 = New System.Windows.Forms.PictureBox()
        Me.txtCostoMensualidad = New System.Windows.Forms.TextBox()
        Me.txtCostoContratacion = New System.Windows.Forms.TextBox()
        Me.cmbxServicio = New System.Windows.Forms.ComboBox()
        Me.CMBlblCostosPlanTarifario = New System.Windows.Forms.Label()
        Me.dgvRelServiciosPaqAdic = New System.Windows.Forms.DataGridView()
        Me.btnCancela_RelPlanTarifario = New System.Windows.Forms.Button()
        Me.btnEliminar_RelPlanTarifario = New System.Windows.Forms.Button()
        Me.btnGuardar_RelPlanTarifario = New System.Windows.Forms.Button()
        Me.btnModifica_RelPlanTarifario = New System.Windows.Forms.Button()
        Me.btnNuevo_RelPlanTarifario = New System.Windows.Forms.Button()
        Me.lblClasificacines = New System.Windows.Forms.Label()
        Me.ListBox2 = New System.Windows.Forms.ListBox()
        Me.ListBox1 = New System.Windows.Forms.ListBox()
        Me.btnAgregar = New System.Windows.Forms.Button()
        Me.BtnQuitar = New System.Windows.Forms.Button()
        Me.BtnAgregarTodas = New System.Windows.Forms.Button()
        Me.BtnQuitarTodas = New System.Windows.Forms.Button()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.Muestra_ServiciosDigitalesTableAdapter1 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.bnPaquetesAdicionales = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.tsbEliminar = New System.Windows.Forms.ToolStripButton()
        Me.tsbGuardar = New System.Windows.Forms.ToolStripButton()
        Me.Muestra_ServiciosDigitalesTableAdapter2 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter3 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter4 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter5 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter6 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter7 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        CMBPaqueteAdicionalLabel = New System.Windows.Forms.Label()
        Clv_PaqAdiLabel = New System.Windows.Forms.Label()
        CMBContratacionLabel1 = New System.Windows.Forms.Label()
        CMBMensualidadLabel1 = New System.Windows.Forms.Label()
        CMBDescripcionLabel1 = New System.Windows.Forms.Label()
        CType(Me.NumericUpDown1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.panelRelPlanTarifario.SuspendLayout()
        CType(Me.pbAyuda01, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvRelServiciosPaqAdic, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bnPaquetesAdicionales, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.bnPaquetesAdicionales.SuspendLayout()
        Me.SuspendLayout()
        '
        'CMBPaqueteAdicionalLabel
        '
        CMBPaqueteAdicionalLabel.AutoSize = True
        CMBPaqueteAdicionalLabel.Font = New System.Drawing.Font("MS Reference Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CMBPaqueteAdicionalLabel.ForeColor = System.Drawing.Color.LightSlateGray
        CMBPaqueteAdicionalLabel.Location = New System.Drawing.Point(94, 55)
        CMBPaqueteAdicionalLabel.Name = "CMBPaqueteAdicionalLabel"
        CMBPaqueteAdicionalLabel.Size = New System.Drawing.Size(211, 16)
        CMBPaqueteAdicionalLabel.TabIndex = 8
        CMBPaqueteAdicionalLabel.Text = "Nombre del Paquete Adicional:"
        '
        'Clv_PaqAdiLabel
        '
        Clv_PaqAdiLabel.AutoSize = True
        Clv_PaqAdiLabel.Font = New System.Drawing.Font("MS Reference Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Clv_PaqAdiLabel.ForeColor = System.Drawing.Color.LightSlateGray
        Clv_PaqAdiLabel.Location = New System.Drawing.Point(612, 224)
        Clv_PaqAdiLabel.Name = "Clv_PaqAdiLabel"
        Clv_PaqAdiLabel.Size = New System.Drawing.Size(54, 16)
        Clv_PaqAdiLabel.TabIndex = 2
        Clv_PaqAdiLabel.Text = "Clave :"
        Clv_PaqAdiLabel.Visible = False
        '
        'CMBContratacionLabel1
        '
        CMBContratacionLabel1.AutoSize = True
        CMBContratacionLabel1.Font = New System.Drawing.Font("MS Reference Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CMBContratacionLabel1.ForeColor = System.Drawing.Color.LightSlateGray
        CMBContratacionLabel1.Location = New System.Drawing.Point(442, 405)
        CMBContratacionLabel1.Name = "CMBContratacionLabel1"
        CMBContratacionLabel1.Size = New System.Drawing.Size(97, 16)
        CMBContratacionLabel1.TabIndex = 25
        CMBContratacionLabel1.Text = "Contratación:"
        '
        'CMBMensualidadLabel1
        '
        CMBMensualidadLabel1.AutoSize = True
        CMBMensualidadLabel1.Font = New System.Drawing.Font("MS Reference Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CMBMensualidadLabel1.ForeColor = System.Drawing.Color.LightSlateGray
        CMBMensualidadLabel1.Location = New System.Drawing.Point(625, 404)
        CMBMensualidadLabel1.Name = "CMBMensualidadLabel1"
        CMBMensualidadLabel1.Size = New System.Drawing.Size(95, 16)
        CMBMensualidadLabel1.TabIndex = 27
        CMBMensualidadLabel1.Text = "Mensualidad:"
        '
        'CMBDescripcionLabel1
        '
        CMBDescripcionLabel1.AutoSize = True
        CMBDescripcionLabel1.Font = New System.Drawing.Font("MS Reference Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CMBDescripcionLabel1.ForeColor = System.Drawing.Color.LightSlateGray
        CMBDescripcionLabel1.Location = New System.Drawing.Point(140, 404)
        CMBDescripcionLabel1.Name = "CMBDescripcionLabel1"
        CMBDescripcionLabel1.Size = New System.Drawing.Size(67, 16)
        CMBDescripcionLabel1.TabIndex = 0
        CMBDescripcionLabel1.Text = "Servicio:"
        '
        'panelGeneralesPaqAdic
        '
        Me.panelGeneralesPaqAdic.Location = New System.Drawing.Point(658, 205)
        Me.panelGeneralesPaqAdic.Name = "panelGeneralesPaqAdic"
        Me.panelGeneralesPaqAdic.Size = New System.Drawing.Size(21, 16)
        Me.panelGeneralesPaqAdic.TabIndex = 41
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label5.Location = New System.Drawing.Point(685, 195)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(128, 15)
        Me.Label5.TabIndex = 48
        Me.Label5.Text = "Minutos Incluidos :"
        Me.Label5.Visible = False
        '
        'chbxAplicaTarifa_PaqAdic
        '
        Me.chbxAplicaTarifa_PaqAdic.AutoSize = True
        Me.chbxAplicaTarifa_PaqAdic.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chbxAplicaTarifa_PaqAdic.Location = New System.Drawing.Point(620, 99)
        Me.chbxAplicaTarifa_PaqAdic.Name = "chbxAplicaTarifa_PaqAdic"
        Me.chbxAplicaTarifa_PaqAdic.Size = New System.Drawing.Size(177, 18)
        Me.chbxAplicaTarifa_PaqAdic.TabIndex = 3
        Me.chbxAplicaTarifa_PaqAdic.Text = "Aplicará la tarifa preferencial"
        Me.chbxAplicaTarifa_PaqAdic.UseVisualStyleBackColor = True
        '
        'CMBLabel4
        '
        Me.CMBLabel4.AutoSize = True
        Me.CMBLabel4.Font = New System.Drawing.Font("MS Reference Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel4.ForeColor = System.Drawing.Color.LightSlateGray
        Me.CMBLabel4.Location = New System.Drawing.Point(617, 55)
        Me.CMBLabel4.Name = "CMBLabel4"
        Me.CMBLabel4.Size = New System.Drawing.Size(174, 16)
        Me.CMBLabel4.TabIndex = 47
        Me.CMBLabel4.Text = "Costo Llamada Adicional:"
        '
        'txtCosto
        '
        Me.txtCosto.Enabled = False
        Me.txtCosto.Font = New System.Drawing.Font("MS Reference Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCosto.Location = New System.Drawing.Point(620, 72)
        Me.txtCosto.Name = "txtCosto"
        Me.txtCosto.Size = New System.Drawing.Size(149, 23)
        Me.txtCosto.TabIndex = 2
        Me.txtCosto.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtNombrePaqAdic
        '
        Me.txtNombrePaqAdic.Font = New System.Drawing.Font("MS Reference Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNombrePaqAdic.Location = New System.Drawing.Point(97, 74)
        Me.txtNombrePaqAdic.Name = "txtNombrePaqAdic"
        Me.txtNombrePaqAdic.Size = New System.Drawing.Size(362, 23)
        Me.txtNombrePaqAdic.TabIndex = 0
        '
        'CMBlblTipoClasificacion
        '
        Me.CMBlblTipoClasificacion.AutoSize = True
        Me.CMBlblTipoClasificacion.Font = New System.Drawing.Font("MS Reference Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBlblTipoClasificacion.ForeColor = System.Drawing.Color.LightSlateGray
        Me.CMBlblTipoClasificacion.Location = New System.Drawing.Point(462, 53)
        Me.CMBlblTipoClasificacion.Name = "CMBlblTipoClasificacion"
        Me.CMBlblTipoClasificacion.Size = New System.Drawing.Size(54, 16)
        Me.CMBlblTipoClasificacion.TabIndex = 20
        Me.CMBlblTipoClasificacion.Text = "Xxxxx:"
        '
        'NumeroTextBox
        '
        Me.NumeroTextBox.Font = New System.Drawing.Font("MS Reference Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NumeroTextBox.Location = New System.Drawing.Point(465, 72)
        Me.NumeroTextBox.Name = "NumeroTextBox"
        Me.NumeroTextBox.Size = New System.Drawing.Size(149, 23)
        Me.NumeroTextBox.TabIndex = 1
        '
        'btnEliminaPaqAdic
        '
        Me.btnEliminaPaqAdic.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEliminaPaqAdic.Font = New System.Drawing.Font("MS Reference Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEliminaPaqAdic.Location = New System.Drawing.Point(688, 211)
        Me.btnEliminaPaqAdic.Name = "btnEliminaPaqAdic"
        Me.btnEliminaPaqAdic.Size = New System.Drawing.Size(10, 10)
        Me.btnEliminaPaqAdic.TabIndex = 51
        Me.btnEliminaPaqAdic.TabStop = False
        Me.btnEliminaPaqAdic.Text = "&Eliminar"
        Me.btnEliminaPaqAdic.UseVisualStyleBackColor = True
        '
        'btnGuardarPaqAdic
        '
        Me.btnGuardarPaqAdic.Enabled = False
        Me.btnGuardarPaqAdic.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnGuardarPaqAdic.Font = New System.Drawing.Font("MS Reference Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGuardarPaqAdic.Location = New System.Drawing.Point(672, 211)
        Me.btnGuardarPaqAdic.Name = "btnGuardarPaqAdic"
        Me.btnGuardarPaqAdic.Size = New System.Drawing.Size(10, 10)
        Me.btnGuardarPaqAdic.TabIndex = 50
        Me.btnGuardarPaqAdic.TabStop = False
        Me.btnGuardarPaqAdic.Text = "&Guardar"
        Me.btnGuardarPaqAdic.UseVisualStyleBackColor = True
        '
        'NumericUpDown1
        '
        Me.NumericUpDown1.Location = New System.Drawing.Point(836, 2)
        Me.NumericUpDown1.Name = "NumericUpDown1"
        Me.NumericUpDown1.Size = New System.Drawing.Size(53, 20)
        Me.NumericUpDown1.TabIndex = 49
        Me.NumericUpDown1.Visible = False
        '
        'txtIDPaqAdic
        '
        Me.txtIDPaqAdic.BackColor = System.Drawing.Color.White
        Me.txtIDPaqAdic.Enabled = False
        Me.txtIDPaqAdic.Font = New System.Drawing.Font("MS Reference Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtIDPaqAdic.Location = New System.Drawing.Point(691, 221)
        Me.txtIDPaqAdic.Name = "txtIDPaqAdic"
        Me.txtIDPaqAdic.ReadOnly = True
        Me.txtIDPaqAdic.Size = New System.Drawing.Size(100, 23)
        Me.txtIDPaqAdic.TabIndex = 3
        Me.txtIDPaqAdic.TabStop = False
        Me.txtIDPaqAdic.Visible = False
        '
        'lblTitulo
        '
        Me.lblTitulo.AutoSize = True
        Me.lblTitulo.Font = New System.Drawing.Font("MS Reference Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTitulo.ForeColor = System.Drawing.Color.LightSlateGray
        Me.lblTitulo.Location = New System.Drawing.Point(401, 2)
        Me.lblTitulo.Name = "lblTitulo"
        Me.lblTitulo.Size = New System.Drawing.Size(164, 16)
        Me.lblTitulo.TabIndex = 45
        Me.lblTitulo.Text = "PAQUETE ADICIONAL:"
        '
        'panelRelPlanTarifario
        '
        Me.panelRelPlanTarifario.Controls.Add(Me.Panel3)
        Me.panelRelPlanTarifario.Controls.Add(Me.Panel2)
        Me.panelRelPlanTarifario.Location = New System.Drawing.Point(688, 227)
        Me.panelRelPlanTarifario.Name = "panelRelPlanTarifario"
        Me.panelRelPlanTarifario.Size = New System.Drawing.Size(21, 16)
        Me.panelRelPlanTarifario.TabIndex = 42
        '
        'Panel3
        '
        Me.Panel3.Location = New System.Drawing.Point(368, 23)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(509, 68)
        Me.Panel3.TabIndex = 43
        '
        'Panel2
        '
        Me.Panel2.Location = New System.Drawing.Point(16, 23)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(334, 68)
        Me.Panel2.TabIndex = 42
        '
        'pbAyuda01
        '
        Me.pbAyuda01.Image = CType(resources.GetObject("pbAyuda01.Image"), System.Drawing.Image)
        Me.pbAyuda01.Location = New System.Drawing.Point(801, 392)
        Me.pbAyuda01.Name = "pbAyuda01"
        Me.pbAyuda01.Size = New System.Drawing.Size(22, 26)
        Me.pbAyuda01.TabIndex = 660
        Me.pbAyuda01.TabStop = False
        '
        'txtCostoMensualidad
        '
        Me.txtCostoMensualidad.Font = New System.Drawing.Font("MS Reference Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCostoMensualidad.Location = New System.Drawing.Point(628, 424)
        Me.txtCostoMensualidad.Name = "txtCostoMensualidad"
        Me.txtCostoMensualidad.Size = New System.Drawing.Size(177, 23)
        Me.txtCostoMensualidad.TabIndex = 12
        '
        'txtCostoContratacion
        '
        Me.txtCostoContratacion.Font = New System.Drawing.Font("MS Reference Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCostoContratacion.Location = New System.Drawing.Point(445, 424)
        Me.txtCostoContratacion.Name = "txtCostoContratacion"
        Me.txtCostoContratacion.Size = New System.Drawing.Size(177, 23)
        Me.txtCostoContratacion.TabIndex = 11
        '
        'cmbxServicio
        '
        Me.cmbxServicio.DisplayMember = "Descripcion"
        Me.cmbxServicio.Font = New System.Drawing.Font("MS Reference Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbxServicio.FormattingEnabled = True
        Me.cmbxServicio.Location = New System.Drawing.Point(143, 423)
        Me.cmbxServicio.Name = "cmbxServicio"
        Me.cmbxServicio.Size = New System.Drawing.Size(296, 24)
        Me.cmbxServicio.TabIndex = 10
        Me.cmbxServicio.ValueMember = "Clv_Servicio"
        '
        'CMBlblCostosPlanTarifario
        '
        Me.CMBlblCostosPlanTarifario.AutoSize = True
        Me.CMBlblCostosPlanTarifario.Font = New System.Drawing.Font("MS Reference Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBlblCostosPlanTarifario.ForeColor = System.Drawing.Color.LightSlateGray
        Me.CMBlblCostosPlanTarifario.Location = New System.Drawing.Point(94, 371)
        Me.CMBlblCostosPlanTarifario.Name = "CMBlblCostosPlanTarifario"
        Me.CMBlblCostosPlanTarifario.Size = New System.Drawing.Size(313, 16)
        Me.CMBlblCostosPlanTarifario.TabIndex = 44
        Me.CMBlblCostosPlanTarifario.Text = "Costos Unitarios para los Planes Tarifarios"
        '
        'dgvRelServiciosPaqAdic
        '
        Me.dgvRelServiciosPaqAdic.AllowUserToAddRows = False
        Me.dgvRelServiciosPaqAdic.AllowUserToDeleteRows = False
        Me.dgvRelServiciosPaqAdic.BackgroundColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("MS Reference Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvRelServiciosPaqAdic.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("MS Reference Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvRelServiciosPaqAdic.DefaultCellStyle = DataGridViewCellStyle2
        Me.dgvRelServiciosPaqAdic.Location = New System.Drawing.Point(178, 517)
        Me.dgvRelServiciosPaqAdic.Name = "dgvRelServiciosPaqAdic"
        Me.dgvRelServiciosPaqAdic.ReadOnly = True
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvRelServiciosPaqAdic.RowHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.dgvRelServiciosPaqAdic.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvRelServiciosPaqAdic.Size = New System.Drawing.Size(627, 184)
        Me.dgvRelServiciosPaqAdic.TabIndex = 17
        '
        'btnCancela_RelPlanTarifario
        '
        Me.btnCancela_RelPlanTarifario.Enabled = False
        Me.btnCancela_RelPlanTarifario.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancela_RelPlanTarifario.Location = New System.Drawing.Point(502, 464)
        Me.btnCancela_RelPlanTarifario.Name = "btnCancela_RelPlanTarifario"
        Me.btnCancela_RelPlanTarifario.Size = New System.Drawing.Size(120, 32)
        Me.btnCancela_RelPlanTarifario.TabIndex = 15
        Me.btnCancela_RelPlanTarifario.Text = "&Cancelar"
        Me.btnCancela_RelPlanTarifario.UseVisualStyleBackColor = True
        '
        'btnEliminar_RelPlanTarifario
        '
        Me.btnEliminar_RelPlanTarifario.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEliminar_RelPlanTarifario.Location = New System.Drawing.Point(630, 464)
        Me.btnEliminar_RelPlanTarifario.Name = "btnEliminar_RelPlanTarifario"
        Me.btnEliminar_RelPlanTarifario.Size = New System.Drawing.Size(120, 32)
        Me.btnEliminar_RelPlanTarifario.TabIndex = 16
        Me.btnEliminar_RelPlanTarifario.Text = "&Eliminar"
        Me.btnEliminar_RelPlanTarifario.UseVisualStyleBackColor = True
        '
        'btnGuardar_RelPlanTarifario
        '
        Me.btnGuardar_RelPlanTarifario.Enabled = False
        Me.btnGuardar_RelPlanTarifario.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGuardar_RelPlanTarifario.Location = New System.Drawing.Point(250, 464)
        Me.btnGuardar_RelPlanTarifario.Name = "btnGuardar_RelPlanTarifario"
        Me.btnGuardar_RelPlanTarifario.Size = New System.Drawing.Size(120, 32)
        Me.btnGuardar_RelPlanTarifario.TabIndex = 13
        Me.btnGuardar_RelPlanTarifario.Text = "&Guardar"
        Me.btnGuardar_RelPlanTarifario.UseVisualStyleBackColor = True
        '
        'btnModifica_RelPlanTarifario
        '
        Me.btnModifica_RelPlanTarifario.Enabled = False
        Me.btnModifica_RelPlanTarifario.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnModifica_RelPlanTarifario.Location = New System.Drawing.Point(376, 464)
        Me.btnModifica_RelPlanTarifario.Name = "btnModifica_RelPlanTarifario"
        Me.btnModifica_RelPlanTarifario.Size = New System.Drawing.Size(120, 32)
        Me.btnModifica_RelPlanTarifario.TabIndex = 14
        Me.btnModifica_RelPlanTarifario.Text = "&Modificar"
        Me.btnModifica_RelPlanTarifario.UseVisualStyleBackColor = True
        '
        'btnNuevo_RelPlanTarifario
        '
        Me.btnNuevo_RelPlanTarifario.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnNuevo_RelPlanTarifario.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNuevo_RelPlanTarifario.Location = New System.Drawing.Point(717, 240)
        Me.btnNuevo_RelPlanTarifario.Name = "btnNuevo_RelPlanTarifario"
        Me.btnNuevo_RelPlanTarifario.Size = New System.Drawing.Size(120, 31)
        Me.btnNuevo_RelPlanTarifario.TabIndex = 34
        Me.btnNuevo_RelPlanTarifario.TabStop = False
        Me.btnNuevo_RelPlanTarifario.Text = "&Nuevo"
        Me.btnNuevo_RelPlanTarifario.UseVisualStyleBackColor = True
        Me.btnNuevo_RelPlanTarifario.Visible = False
        '
        'lblClasificacines
        '
        Me.lblClasificacines.AutoSize = True
        Me.lblClasificacines.Font = New System.Drawing.Font("MS Reference Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblClasificacines.ForeColor = System.Drawing.Color.LightSlateGray
        Me.lblClasificacines.Location = New System.Drawing.Point(94, 127)
        Me.lblClasificacines.Name = "lblClasificacines"
        Me.lblClasificacines.Size = New System.Drawing.Size(233, 16)
        Me.lblClasificacines.TabIndex = 46
        Me.lblClasificacines.Text = "Clasificaciones a las que Aplica"
        '
        'ListBox2
        '
        Me.ListBox2.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ListBox2.FormattingEnabled = True
        Me.ListBox2.ItemHeight = 18
        Me.ListBox2.Location = New System.Drawing.Point(567, 146)
        Me.ListBox2.Name = "ListBox2"
        Me.ListBox2.Size = New System.Drawing.Size(360, 184)
        Me.ListBox2.TabIndex = 9
        '
        'ListBox1
        '
        Me.ListBox1.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ListBox1.FormattingEnabled = True
        Me.ListBox1.ItemHeight = 18
        Me.ListBox1.Location = New System.Drawing.Point(97, 146)
        Me.ListBox1.Name = "ListBox1"
        Me.ListBox1.Size = New System.Drawing.Size(360, 184)
        Me.ListBox1.TabIndex = 4
        '
        'btnAgregar
        '
        Me.btnAgregar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAgregar.Location = New System.Drawing.Point(489, 166)
        Me.btnAgregar.Name = "btnAgregar"
        Me.btnAgregar.Size = New System.Drawing.Size(50, 31)
        Me.btnAgregar.TabIndex = 5
        Me.btnAgregar.Text = ">"
        Me.btnAgregar.UseVisualStyleBackColor = True
        '
        'BtnQuitar
        '
        Me.BtnQuitar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnQuitar.Location = New System.Drawing.Point(489, 240)
        Me.BtnQuitar.Name = "BtnQuitar"
        Me.BtnQuitar.Size = New System.Drawing.Size(50, 31)
        Me.BtnQuitar.TabIndex = 7
        Me.BtnQuitar.Text = "<"
        Me.BtnQuitar.UseVisualStyleBackColor = True
        '
        'BtnAgregarTodas
        '
        Me.BtnAgregarTodas.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnAgregarTodas.Location = New System.Drawing.Point(489, 203)
        Me.BtnAgregarTodas.Name = "BtnAgregarTodas"
        Me.BtnAgregarTodas.Size = New System.Drawing.Size(50, 31)
        Me.BtnAgregarTodas.TabIndex = 6
        Me.BtnAgregarTodas.Text = ">>"
        Me.BtnAgregarTodas.UseVisualStyleBackColor = True
        '
        'BtnQuitarTodas
        '
        Me.BtnQuitarTodas.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnQuitarTodas.Location = New System.Drawing.Point(489, 281)
        Me.BtnQuitarTodas.Name = "BtnQuitarTodas"
        Me.BtnQuitarTodas.Size = New System.Drawing.Size(50, 31)
        Me.BtnQuitarTodas.TabIndex = 8
        Me.BtnQuitarTodas.Text = "<<"
        Me.BtnQuitarTodas.UseVisualStyleBackColor = True
        '
        'btnSalir
        '
        Me.btnSalir.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSalir.Location = New System.Drawing.Point(860, 682)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(136, 36)
        Me.btnSalir.TabIndex = 18
        Me.btnSalir.Text = "&SALIR"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'Muestra_ServiciosDigitalesTableAdapter1
        '
        Me.Muestra_ServiciosDigitalesTableAdapter1.ClearBeforeFill = True
        '
        'bnPaquetesAdicionales
        '
        Me.bnPaquetesAdicionales.AddNewItem = Nothing
        Me.bnPaquetesAdicionales.CountItem = Nothing
        Me.bnPaquetesAdicionales.DeleteItem = Nothing
        Me.bnPaquetesAdicionales.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnPaquetesAdicionales.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsbEliminar, Me.tsbGuardar})
        Me.bnPaquetesAdicionales.Location = New System.Drawing.Point(0, 0)
        Me.bnPaquetesAdicionales.MoveFirstItem = Nothing
        Me.bnPaquetesAdicionales.MoveLastItem = Nothing
        Me.bnPaquetesAdicionales.MoveNextItem = Nothing
        Me.bnPaquetesAdicionales.MovePreviousItem = Nothing
        Me.bnPaquetesAdicionales.Name = "bnPaquetesAdicionales"
        Me.bnPaquetesAdicionales.PositionItem = Nothing
        Me.bnPaquetesAdicionales.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.bnPaquetesAdicionales.Size = New System.Drawing.Size(1008, 25)
        Me.bnPaquetesAdicionales.TabIndex = 19
        Me.bnPaquetesAdicionales.Text = "BindingNavigator1"
        '
        'tsbEliminar
        '
        Me.tsbEliminar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.tsbEliminar.Image = CType(resources.GetObject("tsbEliminar.Image"), System.Drawing.Image)
        Me.tsbEliminar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsbEliminar.Name = "tsbEliminar"
        Me.tsbEliminar.Size = New System.Drawing.Size(72, 22)
        Me.tsbEliminar.Text = "&ELIMINAR"
        '
        'tsbGuardar
        '
        Me.tsbGuardar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.tsbGuardar.Image = CType(resources.GetObject("tsbGuardar.Image"), System.Drawing.Image)
        Me.tsbGuardar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsbGuardar.Name = "tsbGuardar"
        Me.tsbGuardar.Size = New System.Drawing.Size(72, 22)
        Me.tsbGuardar.Text = "&GUARDAR"
        '
        'Muestra_ServiciosDigitalesTableAdapter2
        '
        Me.Muestra_ServiciosDigitalesTableAdapter2.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter3
        '
        Me.Muestra_ServiciosDigitalesTableAdapter3.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter4
        '
        Me.Muestra_ServiciosDigitalesTableAdapter4.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter5
        '
        Me.Muestra_ServiciosDigitalesTableAdapter5.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter6
        '
        Me.Muestra_ServiciosDigitalesTableAdapter6.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter7
        '
        Me.Muestra_ServiciosDigitalesTableAdapter7.ClearBeforeFill = True
        '
        'FrmPaqueteAdicionalEntreClasificaciones
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1008, 730)
        Me.Controls.Add(Me.ListBox2)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.txtCostoMensualidad)
        Me.Controls.Add(CMBMensualidadLabel1)
        Me.Controls.Add(Me.pbAyuda01)
        Me.Controls.Add(CMBDescripcionLabel1)
        Me.Controls.Add(CMBContratacionLabel1)
        Me.Controls.Add(Me.chbxAplicaTarifa_PaqAdic)
        Me.Controls.Add(Me.txtCostoContratacion)
        Me.Controls.Add(Me.cmbxServicio)
        Me.Controls.Add(Me.bnPaquetesAdicionales)
        Me.Controls.Add(Me.CMBLabel4)
        Me.Controls.Add(Me.CMBlblCostosPlanTarifario)
        Me.Controls.Add(Me.btnEliminaPaqAdic)
        Me.Controls.Add(Me.txtCosto)
        Me.Controls.Add(Me.dgvRelServiciosPaqAdic)
        Me.Controls.Add(Me.txtNombrePaqAdic)
        Me.Controls.Add(Me.btnGuardarPaqAdic)
        Me.Controls.Add(Me.CMBlblTipoClasificacion)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(CMBPaqueteAdicionalLabel)
        Me.Controls.Add(Me.NumeroTextBox)
        Me.Controls.Add(Me.NumericUpDown1)
        Me.Controls.Add(Me.BtnQuitarTodas)
        Me.Controls.Add(Me.BtnAgregarTodas)
        Me.Controls.Add(Me.BtnQuitar)
        Me.Controls.Add(Me.btnAgregar)
        Me.Controls.Add(Me.btnCancela_RelPlanTarifario)
        Me.Controls.Add(Clv_PaqAdiLabel)
        Me.Controls.Add(Me.txtIDPaqAdic)
        Me.Controls.Add(Me.btnEliminar_RelPlanTarifario)
        Me.Controls.Add(Me.btnGuardar_RelPlanTarifario)
        Me.Controls.Add(Me.btnModifica_RelPlanTarifario)
        Me.Controls.Add(Me.btnNuevo_RelPlanTarifario)
        Me.Controls.Add(Me.ListBox1)
        Me.Controls.Add(Me.lblClasificacines)
        Me.Controls.Add(Me.panelRelPlanTarifario)
        Me.Controls.Add(Me.panelGeneralesPaqAdic)
        Me.Controls.Add(Me.lblTitulo)
        Me.MaximizeBox = False
        Me.Name = "FrmPaqueteAdicionalEntreClasificaciones"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Paquetes Adicionales"
        CType(Me.NumericUpDown1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.panelRelPlanTarifario.ResumeLayout(False)
        CType(Me.pbAyuda01, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvRelServiciosPaqAdic, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bnPaquetesAdicionales, System.ComponentModel.ISupportInitialize).EndInit()
        Me.bnPaquetesAdicionales.ResumeLayout(False)
        Me.bnPaquetesAdicionales.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents panelGeneralesPaqAdic As System.Windows.Forms.Panel
    Friend WithEvents NumericUpDown1 As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents CMBLabel4 As System.Windows.Forms.Label
    Friend WithEvents txtCosto As System.Windows.Forms.TextBox
    Friend WithEvents lblTitulo As System.Windows.Forms.Label
    Friend WithEvents txtNombrePaqAdic As System.Windows.Forms.TextBox
    Friend WithEvents CMBlblTipoClasificacion As System.Windows.Forms.Label
    Friend WithEvents NumeroTextBox As System.Windows.Forms.TextBox
    Friend WithEvents txtIDPaqAdic As System.Windows.Forms.TextBox
    Friend WithEvents panelRelPlanTarifario As System.Windows.Forms.Panel
    Friend WithEvents CMBlblCostosPlanTarifario As System.Windows.Forms.Label
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents txtCostoMensualidad As System.Windows.Forms.TextBox
    Friend WithEvents txtCostoContratacion As System.Windows.Forms.TextBox
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents cmbxServicio As System.Windows.Forms.ComboBox
    Friend WithEvents btnCancela_RelPlanTarifario As System.Windows.Forms.Button
    Friend WithEvents btnEliminar_RelPlanTarifario As System.Windows.Forms.Button
    Friend WithEvents btnGuardar_RelPlanTarifario As System.Windows.Forms.Button
    Friend WithEvents btnModifica_RelPlanTarifario As System.Windows.Forms.Button
    Friend WithEvents btnNuevo_RelPlanTarifario As System.Windows.Forms.Button
    Friend WithEvents dgvRelServiciosPaqAdic As System.Windows.Forms.DataGridView
    Friend WithEvents lblClasificacines As System.Windows.Forms.Label
    Friend WithEvents ListBox2 As System.Windows.Forms.ListBox
    Friend WithEvents ListBox1 As System.Windows.Forms.ListBox
    Friend WithEvents btnAgregar As System.Windows.Forms.Button
    Friend WithEvents BtnQuitar As System.Windows.Forms.Button
    Friend WithEvents BtnAgregarTodas As System.Windows.Forms.Button
    Friend WithEvents BtnQuitarTodas As System.Windows.Forms.Button
    Friend WithEvents btnEliminaPaqAdic As System.Windows.Forms.Button
    Friend WithEvents btnGuardarPaqAdic As System.Windows.Forms.Button
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents pbAyuda01 As System.Windows.Forms.PictureBox
    Friend WithEvents chbxAplicaTarifa_PaqAdic As System.Windows.Forms.CheckBox
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter1 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents bnPaquetesAdicionales As System.Windows.Forms.BindingNavigator
    Friend WithEvents tsbGuardar As System.Windows.Forms.ToolStripButton
    Friend WithEvents tsbEliminar As System.Windows.Forms.ToolStripButton
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter2 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter3 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter4 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter5 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter6 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter7 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
End Class

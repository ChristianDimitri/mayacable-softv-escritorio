﻿
Imports System.Data.SqlClient
Imports System.Collections.Generic
Imports System.IO
Imports System.Xml
Imports System.Text
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports System
Imports System.IO.StreamReader
Imports System.IO.File
Public Class FrmImprimirAux

    Private customersByCityReport As ReportDocument

    Private Sub ReportsPortabilidad(ByVal FolioS As Long)
        Dim impresora As String = Nothing


        customersByCityReport = New ReportDocument
        Dim connectionInfo As New ConnectionInfo

        connectionInfo.ServerName = GloServerName
        connectionInfo.DatabaseName = GloDatabaseName
        connectionInfo.UserID = GloUserID
        connectionInfo.Password = GloPassword

        Dim reportPath As String = Nothing
        reportPath = RutaReportes + "\repotrtePortabilidad.rpt"

        customersByCityReport.Load(reportPath)


        SetDBLogonForReport(connectionInfo, customersByCityReport)

        '@FolioS 
        customersByCityReport.SetParameterValue(0, FolioS)
        CrystalReportViewer1.ReportSource = customersByCityReport
        CrystalReportViewer1.ShowPrintButton = True

        'Me.Selecciona_ImpresoraTableAdapter.Fill(Me.DataSetarnoldo.Selecciona_Impresora, 1, impresora)
        'customersByCityReport.PrintOptions.PrinterName = Impresora_Contratos
        'customersByCityReport.PrintToPrinter(0, True, 1, 1)
        'customersByCityReport = Nothing


    End Sub
    Private Sub ReportsPortabilidad_NewXml(ByVal FolioS As Long)
        Dim impresora As String = Nothing


        customersByCityReport = New ReportDocument
        Dim connectionInfo As New ConnectionInfo

        connectionInfo.ServerName = GloServerName
        connectionInfo.DatabaseName = GloDatabaseName
        connectionInfo.UserID = GloUserID
        connectionInfo.Password = GloPassword

        Dim reportPath As String = Nothing
        reportPath = RutaReportes + "\repotrtePortabilidad.rpt"

        Dim cnn As New SqlConnection(MiConexion)

        Dim cmd As New SqlCommand("ReportePortabilidad", cnn)
        cmd.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Folio", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = FolioS
        cmd.Parameters.Add(parametro)


        Dim da As New SqlDataAdapter(cmd)

        Dim ds As New DataSet()

        Dim data1 As New DataTable()
        Dim data2 As New DataTable()
        Dim data3 As New DataTable()
        Dim data4 As New DataTable()

        Dim cmd2 As New SqlCommand("RPTrelClienTelPortabilidad", cnn)
        cmd2.CommandType = CommandType.StoredProcedure

        Dim parametro1 As New SqlParameter("@Folio", SqlDbType.BigInt)
        parametro1.Direction = ParameterDirection.Input
        parametro1.Value = FolioS
        cmd2.Parameters.Add(parametro1)
        Dim da2 As New SqlDataAdapter(cmd2)


        Dim cmd3 As New SqlCommand("Select * from General", cnn)
        cmd3.CommandType = CommandType.Text
        Dim da3 As New SqlDataAdapter(cmd3)

        Dim cmd4 As New SqlCommand("select * from IntevaloClienTelPortabilidad", cnn)
        cmd4.CommandType = CommandType.Text
        Dim da4 As New SqlDataAdapter(cmd4)

        da.Fill(data1)
        da2.Fill(data2)
        da3.Fill(data3)
        da4.Fill(data4)
        data1.TableName = "ReportePortabilidad"
        data2.TableName = "RPTrelClienTelPortabilidad"
        data3.TableName = "General"
        data4.TableName = "IntevaloClienTelPortabilidad"

        ds.Tables.Add(data1)
        ds.Tables.Add(data2)
        ds.Tables.Add(data3)
        ds.Tables.Add(data4)

        customersByCityReport.Load(reportPath)
        customersByCityReport.SetDataSource(ds)
        CrystalReportViewer1.ReportSource = customersByCityReport
        CrystalReportViewer1.ShowPrintButton = True

        'Me.Selecciona_ImpresoraTableAdapter.Fill(Me.DataSetarnoldo.Selecciona_Impresora, 1, impresora)
        'customersByCityReport.PrintOptions.PrinterName = Impresora_Contratos
        'customersByCityReport.PrintToPrinter(0, True, 1, 1)
        'customersByCityReport = Nothing


    End Sub

    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        customersByCityReport.DataSourceConnections(0).SetConnection(GloServerName, GloDatabaseName, GloUserID, GloPassword)
        'customersByCityReport.SetDatabaseLogon(GloUserID, GloPassword, GloServerName, GloDatabaseName)

        Dim myTables As Tables = myReportDocument.Database.Tables
        Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
        For Each myTable In myTables
            Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            myTable.ApplyLogOnInfo(myTableLogonInfo)
            myTable.Location = GloDatabaseName & ".dbo." & myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)
        Next
    End Sub

    Private Sub FrmImprimirAux_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ReportsPortabilidad_NewXml(folioport)
    End Sub
End Class
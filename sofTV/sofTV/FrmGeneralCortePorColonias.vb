﻿Public Class FrmGeneralCortePorColonias

    Private Sub MuestraCiudades()
        BaseII.limpiaParametros()
        cbCiudades.DataSource = BaseII.ConsultaDT("MuestraCiudades")
    End Sub

    Private Sub MUESTRAPERIODOS(ByVal Clv_Periodo As Integer)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_Periodo", SqlDbType.Int, Clv_Periodo)
        cbPeriodo.DataSource = BaseII.ConsultaDT("MUESTRAPERIODOS")
    End Sub

    Private Sub CONGeneralCorteColonias(ByVal Op As Integer, ByVal Clv_Ciudad As Integer, ByVal Clv_Periodo As Integer)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Op", SqlDbType.Int, Op)
        BaseII.CreateMyParameter("@Clv_Ciudad", SqlDbType.Int, Clv_Ciudad)
        BaseII.CreateMyParameter("@Clv_Periodo", SqlDbType.Int, Clv_Periodo)
        If Op = 0 Then
            dgvIzq.DataSource = BaseII.ConsultaDT("CONGeneralCorteColonias")
        ElseIf Op = 1 Then
            dgvDer.DataSource = BaseII.ConsultaDT("CONGeneralCorteColonias")
        End If
    End Sub

    Private Sub NUEGeneralCorteColonias(ByVal Op As Integer, ByVal Clv_Colonia As Integer, ByVal Clv_Periodo As Integer)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Op", SqlDbType.Int, Op)
        BaseII.CreateMyParameter("@Clv_Colonia", SqlDbType.Int, Clv_Colonia)
        BaseII.CreateMyParameter("@Clv_Periodo", SqlDbType.Int, Clv_Periodo)
        BaseII.Inserta("NUEGeneralCorteColonias")
    End Sub

    Private Sub BORGeneralCorteColonias(ByVal Op As Integer, ByVal Clv_Colonia As Integer, ByVal Clv_Periodo As Integer)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Op", SqlDbType.Int, Op)
        BaseII.CreateMyParameter("@Clv_Colonia", SqlDbType.Int, Clv_Colonia)
        BaseII.CreateMyParameter("@Clv_Periodo", SqlDbType.Int, Clv_Periodo)
        BaseII.Inserta("BORGeneralCorteColonias")
    End Sub


    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles Button1.Click
        If dgvIzq.Rows.Count = 0 Then
            Exit Sub
        End If
        If cbPeriodo.Text.Length = 0 Then
            Exit Sub
        End If
        NUEGeneralCorteColonias(0, dgvIzq.SelectedCells(0).Value, cbPeriodo.SelectedValue)
        CONGeneralCorteColonias(0, cbCiudades.SelectedValue, cbPeriodo.SelectedValue)
        CONGeneralCorteColonias(1, cbCiudades.SelectedValue, cbPeriodo.SelectedValue)
    End Sub

    Private Sub Button2_Click(sender As System.Object, e As System.EventArgs) Handles Button2.Click
        If dgvIzq.Rows.Count = 0 Then
            Exit Sub
        End If
        If cbPeriodo.Text.Length = 0 Then
            Exit Sub
        End If
        NUEGeneralCorteColonias(1, 0, cbPeriodo.SelectedValue)
        CONGeneralCorteColonias(0, cbCiudades.SelectedValue, cbPeriodo.SelectedValue)
        CONGeneralCorteColonias(1, cbCiudades.SelectedValue, cbPeriodo.SelectedValue)
    End Sub

    Private Sub Button3_Click(sender As System.Object, e As System.EventArgs) Handles Button3.Click
        If dgvDer.Rows.Count = 0 Then
            Exit Sub
        End If
        If cbPeriodo.Text.Length = 0 Then
            Exit Sub
        End If
        BORGeneralCorteColonias(0, dgvDer.SelectedCells(0).Value, cbPeriodo.SelectedValue)
        CONGeneralCorteColonias(0, cbCiudades.SelectedValue, cbPeriodo.SelectedValue)
        CONGeneralCorteColonias(1, cbCiudades.SelectedValue, cbPeriodo.SelectedValue)
    End Sub

    Private Sub Button4_Click(sender As System.Object, e As System.EventArgs) Handles Button4.Click
        If dgvDer.Rows.Count = 0 Then
            Exit Sub
        End If
        If cbPeriodo.Text.Length = 0 Then
            Exit Sub
        End If
        BORGeneralCorteColonias(1, 0, cbPeriodo.SelectedValue)
        CONGeneralCorteColonias(0, cbCiudades.SelectedValue, cbPeriodo.SelectedValue)
        CONGeneralCorteColonias(1, cbCiudades.SelectedValue, cbPeriodo.SelectedValue)
    End Sub

    Private Sub bnSalir_Click(sender As System.Object, e As System.EventArgs) Handles bnSalir.Click
        Me.Close()
    End Sub

    Private Sub FrmGeneralCorteColonias_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
        MuestraCiudades()
        MUESTRAPERIODOS(0)
    End Sub

    Private Sub cbCiudades_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles cbCiudades.SelectedIndexChanged

        If cbCiudades.Text.Length = 0 Then
            Exit Sub
        End If

        If cbPeriodo.Text.Length = 0 Then
            Exit Sub
        End If

        CONGeneralCorteColonias(0, cbCiudades.SelectedValue, cbPeriodo.SelectedValue)
        CONGeneralCorteColonias(1, cbCiudades.SelectedValue, cbPeriodo.SelectedValue)

    End Sub

    Private Sub cbPeriodo_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles cbPeriodo.SelectedIndexChanged

        If cbCiudades.Text.Length = 0 Then
            Exit Sub
        End If

        If cbPeriodo.Text.Length = 0 Then
            Exit Sub
        End If

        CONGeneralCorteColonias(0, cbCiudades.SelectedValue, cbPeriodo.SelectedValue)
        CONGeneralCorteColonias(1, cbCiudades.SelectedValue, cbPeriodo.SelectedValue)

    End Sub


End Class
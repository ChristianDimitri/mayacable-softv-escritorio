﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmTECNICOS
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim ClaveLabel As System.Windows.Forms.Label
        Dim NombreLabel As System.Windows.Forms.Label
        Dim PuestoLabel As System.Windows.Forms.Label
        Dim CMBActivoLabel As System.Windows.Forms.Label
        Dim CuadrillaLabel As System.Windows.Forms.Label
        Dim Label3 As System.Windows.Forms.Label
        Dim Label4 As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmTECNICOS))
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.ComboBox1 = New System.Windows.Forms.ComboBox
        Me.CONTECNICOSBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.NewSofTvDataSet = New sofTV.NewSofTvDataSet
        Me.MUESTRApuestoTecBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Panel3 = New System.Windows.Forms.Panel
        Me.TreeView2 = New System.Windows.Forms.TreeView
        Me.ComboBox4 = New System.Windows.Forms.ComboBox
        Me.MuestraTipSerPrincipalBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Button3 = New System.Windows.Forms.Button
        Me.Button4 = New System.Windows.Forms.Button
        Me.CMBLabel5 = New System.Windows.Forms.Label
        Me.CONTECNICOSBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorDeleteItem = New System.Windows.Forms.ToolStripButton
        Me.ToolStripButton1 = New System.Windows.Forms.ToolStripButton
        Me.CONTECNICOSBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton
        Me.ClaveTextBox = New System.Windows.Forms.TextBox
        Me.NombreTextBox = New System.Windows.Forms.TextBox
        Me.PuestoTextBox = New System.Windows.Forms.TextBox
        Me.ActivoCheckBox = New System.Windows.Forms.CheckBox
        Me.CuadrillaTextBox = New System.Windows.Forms.TextBox
        Me.Button5 = New System.Windows.Forms.Button
        Me.CONTECNICOSTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.CONTECNICOSTableAdapter
        Me.MuestraTipSerPrincipalTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.MuestraTipSerPrincipalTableAdapter
        Me.CONRelSerTecBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.CONRelSerTecTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.CONRelSerTecTableAdapter
        Me.MUESTRApuestoTecTableAdapter = New sofTV.NewSofTvDataSetTableAdapters.MUESTRApuestoTecTableAdapter
        ClaveLabel = New System.Windows.Forms.Label
        NombreLabel = New System.Windows.Forms.Label
        PuestoLabel = New System.Windows.Forms.Label
        CMBActivoLabel = New System.Windows.Forms.Label
        CuadrillaLabel = New System.Windows.Forms.Label
        Label3 = New System.Windows.Forms.Label
        Label4 = New System.Windows.Forms.Label
        Me.Panel1.SuspendLayout()
        CType(Me.CONTECNICOSBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NewSofTvDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MUESTRApuestoTecBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel3.SuspendLayout()
        CType(Me.MuestraTipSerPrincipalBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CONTECNICOSBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.CONTECNICOSBindingNavigator.SuspendLayout()
        CType(Me.CONRelSerTecBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ClaveLabel
        '
        ClaveLabel.AutoSize = True
        ClaveLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ClaveLabel.ForeColor = System.Drawing.Color.LightSlateGray
        ClaveLabel.Location = New System.Drawing.Point(91, 53)
        ClaveLabel.Name = "ClaveLabel"
        ClaveLabel.Size = New System.Drawing.Size(50, 15)
        ClaveLabel.TabIndex = 0
        ClaveLabel.Text = "Clave :"
        '
        'NombreLabel
        '
        NombreLabel.AutoSize = True
        NombreLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        NombreLabel.ForeColor = System.Drawing.Color.LightSlateGray
        NombreLabel.Location = New System.Drawing.Point(75, 80)
        NombreLabel.Name = "NombreLabel"
        NombreLabel.Size = New System.Drawing.Size(66, 15)
        NombreLabel.TabIndex = 2
        NombreLabel.Text = "Nombre :"
        '
        'PuestoLabel
        '
        PuestoLabel.AutoSize = True
        PuestoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        PuestoLabel.ForeColor = System.Drawing.Color.LightSlateGray
        PuestoLabel.Location = New System.Drawing.Point(85, 109)
        PuestoLabel.Name = "PuestoLabel"
        PuestoLabel.Size = New System.Drawing.Size(55, 15)
        PuestoLabel.TabIndex = 4
        PuestoLabel.Text = "Puesto:"
        '
        'CMBActivoLabel
        '
        CMBActivoLabel.AutoSize = True
        CMBActivoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CMBActivoLabel.ForeColor = System.Drawing.Color.OrangeRed
        CMBActivoLabel.Location = New System.Drawing.Point(91, 180)
        CMBActivoLabel.Name = "CMBActivoLabel"
        CMBActivoLabel.Size = New System.Drawing.Size(115, 16)
        CMBActivoLabel.TabIndex = 6
        CMBActivoLabel.Text = "Técnico Activo:"
        '
        'CuadrillaLabel
        '
        CuadrillaLabel.AutoSize = True
        CuadrillaLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CuadrillaLabel.ForeColor = System.Drawing.Color.LightSlateGray
        CuadrillaLabel.Location = New System.Drawing.Point(72, 139)
        CuadrillaLabel.Name = "CuadrillaLabel"
        CuadrillaLabel.Size = New System.Drawing.Size(69, 15)
        CuadrillaLabel.TabIndex = 8
        CuadrillaLabel.Text = "Cuadrilla:"
        '
        'Label3
        '
        Label3.AutoSize = True
        Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label3.ForeColor = System.Drawing.Color.LightSlateGray
        Label3.Location = New System.Drawing.Point(3, 95)
        Label3.Name = "Label3"
        Label3.Size = New System.Drawing.Size(352, 15)
        Label3.TabIndex = 6
        Label3.Text = "Tipos de Servicio que se relaccionan con el Técnico  :"
        '
        'Label4
        '
        Label4.AutoSize = True
        Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label4.ForeColor = System.Drawing.Color.LightSlateGray
        Label4.Location = New System.Drawing.Point(15, 20)
        Label4.Name = "Label4"
        Label4.Size = New System.Drawing.Size(193, 15)
        Label4.TabIndex = 4
        Label4.Text = "Seleccione un Tipo Servicio :"
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Panel1.Controls.Add(Me.ComboBox1)
        Me.Panel1.Controls.Add(Me.Panel3)
        Me.Panel1.Controls.Add(Me.CONTECNICOSBindingNavigator)
        Me.Panel1.Controls.Add(ClaveLabel)
        Me.Panel1.Controls.Add(Me.ClaveTextBox)
        Me.Panel1.Controls.Add(NombreLabel)
        Me.Panel1.Controls.Add(Me.NombreTextBox)
        Me.Panel1.Controls.Add(PuestoLabel)
        Me.Panel1.Controls.Add(Me.PuestoTextBox)
        Me.Panel1.Controls.Add(CMBActivoLabel)
        Me.Panel1.Controls.Add(Me.ActivoCheckBox)
        Me.Panel1.Controls.Add(CuadrillaLabel)
        Me.Panel1.Controls.Add(Me.CuadrillaTextBox)
        Me.Panel1.Location = New System.Drawing.Point(12, 12)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(610, 604)
        Me.Panel1.TabIndex = 0
        Me.Panel1.TabStop = True
        '
        'ComboBox1
        '
        Me.ComboBox1.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.CONTECNICOSBindingSource, "Puesto", True))
        Me.ComboBox1.DataSource = Me.MUESTRApuestoTecBindingSource
        Me.ComboBox1.DisplayMember = "Descripcion"
        Me.ComboBox1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ComboBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Location = New System.Drawing.Point(147, 108)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(305, 23)
        Me.ComboBox1.TabIndex = 1
        Me.ComboBox1.ValueMember = "Clave"
        '
        'CONTECNICOSBindingSource
        '
        Me.CONTECNICOSBindingSource.DataMember = "CONTECNICOS"
        Me.CONTECNICOSBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'NewSofTvDataSet
        '
        Me.NewSofTvDataSet.DataSetName = "NewSofTvDataSet"
        Me.NewSofTvDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'MUESTRApuestoTecBindingSource
        '
        Me.MUESTRApuestoTecBindingSource.DataMember = "MUESTRApuestoTec"
        Me.MUESTRApuestoTecBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.TreeView2)
        Me.Panel3.Controls.Add(Me.ComboBox4)
        Me.Panel3.Controls.Add(Label3)
        Me.Panel3.Controls.Add(Label4)
        Me.Panel3.Controls.Add(Me.Button3)
        Me.Panel3.Controls.Add(Me.Button4)
        Me.Panel3.Controls.Add(Me.CMBLabel5)
        Me.Panel3.Enabled = False
        Me.Panel3.Location = New System.Drawing.Point(88, 233)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(487, 329)
        Me.Panel3.TabIndex = 4
        Me.Panel3.TabStop = True
        '
        'TreeView2
        '
        Me.TreeView2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TreeView2.Location = New System.Drawing.Point(8, 115)
        Me.TreeView2.Name = "TreeView2"
        Me.TreeView2.Size = New System.Drawing.Size(356, 200)
        Me.TreeView2.TabIndex = 5
        '
        'ComboBox4
        '
        Me.ComboBox4.DataSource = Me.MuestraTipSerPrincipalBindingSource
        Me.ComboBox4.DisplayMember = "Concepto"
        Me.ComboBox4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox4.FormattingEnabled = True
        Me.ComboBox4.Location = New System.Drawing.Point(18, 38)
        Me.ComboBox4.Name = "ComboBox4"
        Me.ComboBox4.Size = New System.Drawing.Size(320, 23)
        Me.ComboBox4.TabIndex = 4
        Me.ComboBox4.ValueMember = "Clv_TipSerPrincipal"
        '
        'MuestraTipSerPrincipalBindingSource
        '
        Me.MuestraTipSerPrincipalBindingSource.DataMember = "MuestraTipSerPrincipal"
        Me.MuestraTipSerPrincipalBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'Button3
        '
        Me.Button3.BackColor = System.Drawing.Color.DarkRed
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.ForeColor = System.Drawing.Color.White
        Me.Button3.Location = New System.Drawing.Point(376, 149)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(96, 30)
        Me.Button3.TabIndex = 7
        Me.Button3.Text = "&Quitar"
        Me.Button3.UseVisualStyleBackColor = False
        '
        'Button4
        '
        Me.Button4.BackColor = System.Drawing.Color.DarkRed
        Me.Button4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button4.ForeColor = System.Drawing.Color.White
        Me.Button4.Location = New System.Drawing.Point(376, 113)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(96, 30)
        Me.Button4.TabIndex = 6
        Me.Button4.Text = "&Agregar"
        Me.Button4.UseVisualStyleBackColor = False
        '
        'CMBLabel5
        '
        Me.CMBLabel5.AutoSize = True
        Me.CMBLabel5.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel5.ForeColor = System.Drawing.Color.OrangeRed
        Me.CMBLabel5.Location = New System.Drawing.Point(3, 0)
        Me.CMBLabel5.Name = "CMBLabel5"
        Me.CMBLabel5.Size = New System.Drawing.Size(387, 20)
        Me.CMBLabel5.TabIndex = 0
        Me.CMBLabel5.Text = "Relacción del Técnico con los Tipos de Servicio"
        '
        'CONTECNICOSBindingNavigator
        '
        Me.CONTECNICOSBindingNavigator.AddNewItem = Nothing
        Me.CONTECNICOSBindingNavigator.BindingSource = Me.CONTECNICOSBindingSource
        Me.CONTECNICOSBindingNavigator.CountItem = Nothing
        Me.CONTECNICOSBindingNavigator.DeleteItem = Me.BindingNavigatorDeleteItem
        Me.CONTECNICOSBindingNavigator.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CONTECNICOSBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripButton1, Me.BindingNavigatorDeleteItem, Me.CONTECNICOSBindingNavigatorSaveItem})
        Me.CONTECNICOSBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.CONTECNICOSBindingNavigator.MoveFirstItem = Nothing
        Me.CONTECNICOSBindingNavigator.MoveLastItem = Nothing
        Me.CONTECNICOSBindingNavigator.MoveNextItem = Nothing
        Me.CONTECNICOSBindingNavigator.MovePreviousItem = Nothing
        Me.CONTECNICOSBindingNavigator.Name = "CONTECNICOSBindingNavigator"
        Me.CONTECNICOSBindingNavigator.PositionItem = Nothing
        Me.CONTECNICOSBindingNavigator.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.CONTECNICOSBindingNavigator.Size = New System.Drawing.Size(610, 25)
        Me.CONTECNICOSBindingNavigator.TabIndex = 8
        Me.CONTECNICOSBindingNavigator.TabStop = True
        Me.CONTECNICOSBindingNavigator.Text = "BindingNavigator1"
        '
        'BindingNavigatorDeleteItem
        '
        Me.BindingNavigatorDeleteItem.Image = CType(resources.GetObject("BindingNavigatorDeleteItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem"
        Me.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem.Size = New System.Drawing.Size(90, 22)
        Me.BindingNavigatorDeleteItem.Text = "&ELIMINAR"
        '
        'ToolStripButton1
        '
        Me.ToolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.ToolStripButton1.Image = CType(resources.GetObject("ToolStripButton1.Image"), System.Drawing.Image)
        Me.ToolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton1.Name = "ToolStripButton1"
        Me.ToolStripButton1.Size = New System.Drawing.Size(79, 22)
        Me.ToolStripButton1.Text = "&CANCELAR"
        '
        'CONTECNICOSBindingNavigatorSaveItem
        '
        Me.CONTECNICOSBindingNavigatorSaveItem.Image = CType(resources.GetObject("CONTECNICOSBindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.CONTECNICOSBindingNavigatorSaveItem.Name = "CONTECNICOSBindingNavigatorSaveItem"
        Me.CONTECNICOSBindingNavigatorSaveItem.Size = New System.Drawing.Size(91, 22)
        Me.CONTECNICOSBindingNavigatorSaveItem.Text = "&GUARDAR"
        '
        'ClaveTextBox
        '
        Me.ClaveTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.ClaveTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONTECNICOSBindingSource, "clave", True))
        Me.ClaveTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ClaveTextBox.Location = New System.Drawing.Point(147, 51)
        Me.ClaveTextBox.Name = "ClaveTextBox"
        Me.ClaveTextBox.ReadOnly = True
        Me.ClaveTextBox.Size = New System.Drawing.Size(104, 21)
        Me.ClaveTextBox.TabIndex = 100
        Me.ClaveTextBox.TabStop = False
        '
        'NombreTextBox
        '
        Me.NombreTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.NombreTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONTECNICOSBindingSource, "nombre", True))
        Me.NombreTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NombreTextBox.Location = New System.Drawing.Point(147, 78)
        Me.NombreTextBox.Name = "NombreTextBox"
        Me.NombreTextBox.Size = New System.Drawing.Size(429, 21)
        Me.NombreTextBox.TabIndex = 0
        '
        'PuestoTextBox
        '
        Me.PuestoTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PuestoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONTECNICOSBindingSource, "Puesto", True))
        Me.PuestoTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PuestoTextBox.Location = New System.Drawing.Point(147, 109)
        Me.PuestoTextBox.Name = "PuestoTextBox"
        Me.PuestoTextBox.Size = New System.Drawing.Size(33, 21)
        Me.PuestoTextBox.TabIndex = 5
        Me.PuestoTextBox.TabStop = False
        '
        'ActivoCheckBox
        '
        Me.ActivoCheckBox.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.CONTECNICOSBindingSource, "Activo", True))
        Me.ActivoCheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ActivoCheckBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ActivoCheckBox.Location = New System.Drawing.Point(212, 175)
        Me.ActivoCheckBox.Name = "ActivoCheckBox"
        Me.ActivoCheckBox.Size = New System.Drawing.Size(104, 24)
        Me.ActivoCheckBox.TabIndex = 3
        '
        'CuadrillaTextBox
        '
        Me.CuadrillaTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONTECNICOSBindingSource, "Cuadrilla", True))
        Me.CuadrillaTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CuadrillaTextBox.Location = New System.Drawing.Point(147, 136)
        Me.CuadrillaTextBox.Name = "CuadrillaTextBox"
        Me.CuadrillaTextBox.Size = New System.Drawing.Size(104, 21)
        Me.CuadrillaTextBox.TabIndex = 2
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.DarkOrange
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.Black
        Me.Button5.Location = New System.Drawing.Point(486, 622)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(136, 33)
        Me.Button5.TabIndex = 9
        Me.Button5.Text = "&SALIR"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'CONTECNICOSTableAdapter
        '
        Me.CONTECNICOSTableAdapter.ClearBeforeFill = True
        '
        'MuestraTipSerPrincipalTableAdapter
        '
        Me.MuestraTipSerPrincipalTableAdapter.ClearBeforeFill = True
        '
        'CONRelSerTecBindingSource
        '
        Me.CONRelSerTecBindingSource.DataMember = "CONRelSerTec"
        Me.CONRelSerTecBindingSource.DataSource = Me.NewSofTvDataSet
        '
        'CONRelSerTecTableAdapter
        '
        Me.CONRelSerTecTableAdapter.ClearBeforeFill = True
        '
        'MUESTRApuestoTecTableAdapter
        '
        Me.MUESTRApuestoTecTableAdapter.ClearBeforeFill = True
        '
        'FrmTECNICOS
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(633, 667)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.Panel1)
        Me.Name = "FrmTECNICOS"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Catálogo de Técnicos"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.CONTECNICOSBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NewSofTvDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MUESTRApuestoTecBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        CType(Me.MuestraTipSerPrincipalBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CONTECNICOSBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.CONTECNICOSBindingNavigator.ResumeLayout(False)
        Me.CONTECNICOSBindingNavigator.PerformLayout()
        CType(Me.CONRelSerTecBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents NewSofTvDataSet As sofTV.NewSofTvDataSet
    Friend WithEvents CONTECNICOSBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CONTECNICOSTableAdapter As sofTV.NewSofTvDataSetTableAdapters.CONTECNICOSTableAdapter
    Friend WithEvents CONTECNICOSBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorDeleteItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripButton1 As System.Windows.Forms.ToolStripButton
    Friend WithEvents CONTECNICOSBindingNavigatorSaveItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents ClaveTextBox As System.Windows.Forms.TextBox
    Friend WithEvents NombreTextBox As System.Windows.Forms.TextBox
    Friend WithEvents PuestoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ActivoCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents CuadrillaTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents TreeView2 As System.Windows.Forms.TreeView
    Friend WithEvents ComboBox4 As System.Windows.Forms.ComboBox
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents CMBLabel5 As System.Windows.Forms.Label
    Friend WithEvents MuestraTipSerPrincipalBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MuestraTipSerPrincipalTableAdapter As sofTV.NewSofTvDataSetTableAdapters.MuestraTipSerPrincipalTableAdapter
    Friend WithEvents CONRelSerTecBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CONRelSerTecTableAdapter As sofTV.NewSofTvDataSetTableAdapters.CONRelSerTecTableAdapter
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents MUESTRApuestoTecBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MUESTRApuestoTecTableAdapter As sofTV.NewSofTvDataSetTableAdapters.MUESTRApuestoTecTableAdapter
End Class

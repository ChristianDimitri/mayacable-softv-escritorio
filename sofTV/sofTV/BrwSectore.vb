Imports System.Data.SqlClient
Public Class BrwSectore

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.ConSectorTableAdapter.Connection = CON
        Me.ConSectorTableAdapter.Fill(Me.DataSetEric.ConSector, 0, Me.TextBox1.Text, "", 1)
        'muestrabrowser(0, Me.TextBox1.Text, "", 1)
        CON.Close()

    End Sub

    Private Sub TextBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox1.KeyPress
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        If Asc(e.KeyChar) = 13 Then
            Me.ConSectorTableAdapter.Connection = CON
            Me.ConSectorTableAdapter.Fill(Me.DataSetEric.ConSector, 0, Me.TextBox1.Text, "", 1)
            'muestrabrowser(0, Me.TextBox1.Text, "", 1)
        End If
        CON.Close()

    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.ConSectorTableAdapter.Connection = CON
        Me.ConSectorTableAdapter.Fill(Me.DataSetEric.ConSector, 0, "", Me.TextBox2.Text, 2)
        'muestrabrowser(0, "", Me.TextBox2.Text, 2)
        CON.Close()
    End Sub

    Private Sub TextBox2_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox2.KeyPress

        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        If Asc(e.KeyChar) = 13 Then
            Me.ConSectorTableAdapter.Connection = CON
            Me.ConSectorTableAdapter.Fill(Me.DataSetEric.ConSector, 0, "", Me.TextBox2.Text, 2)
            'muestrabrowser(0, "", Me.TextBox2.Text, 2)
        End If
        CON.Close()
    End Sub


    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        eOpcion = "N"
        FrmSectore.Show()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        If Me.ConSectorDataGridView.RowCount > 0 Then
            eOpcion = "C"
            eClv_Sector = Me.Clv_SectorLabel1.Text
            FrmSectore.Show()
        Else
            MsgBox("Selecciona un Registro a Consultar.", , "Atenci�n")
        End If


    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        If Me.ConSectorDataGridView.RowCount > 0 Then
            eOpcion = "M"
            eClv_Sector = Me.Clv_SectorLabel1.Text
            FrmSectore.Show()
        End If
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Me.Close()
    End Sub

    Private Sub BrwSectore_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.ConSectorTableAdapter.Connection = CON
        Me.ConSectorTableAdapter.Fill(Me.DataSetEric.ConSector, 0, "", "", 0)
        'muestrabrowser(0, "", "", 0)
        CON.Close()
    End Sub

    Private Sub BrwSectore_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.ConSectorTableAdapter.Connection = CON
        Me.ConSectorTableAdapter.Fill(Me.DataSetEric.ConSector, 0, "", "", 0)
        'muestrabrowser(0, "", "", 0)
        CON.Close()
    End Sub

    Private Sub TextBox2_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox2.TextChanged

    End Sub

    Private Sub muestrabrowser(ByVal clv_sector As Integer, ByVal clv_txt As String, ByVal descripcion As String, ByVal opcion As Integer)
        Dim datatable As New DataTable
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_Sector", SqlDbType.BigInt, clv_sector)
        BaseII.CreateMyParameter("@Clv_Txt", SqlDbType.VarChar, clv_txt, 50)
        BaseII.CreateMyParameter("@Descripcion", SqlDbType.VarChar, descripcion, 150)
        BaseII.CreateMyParameter("@Op", SqlDbType.Int, opcion)
        datatable = BaseII.ConsultaDT("ConSector")
        If datatable.Rows.Count = 0 Then Exit Sub
        Clv_TxtLabel1.Text = datatable.Rows(0)(1).ToString
        DescripcionLabel1.Text = datatable.Rows(0)(2).ToString
        DiaCorteLabel1.Text = datatable.Rows(0)(3).ToString
        DiaGenOrdLabel1.Text = datatable.Rows(0)(4).ToString
        Me.TextBox1.Text = ""
        Me.TextBox2.Text = ""
    End Sub

End Class
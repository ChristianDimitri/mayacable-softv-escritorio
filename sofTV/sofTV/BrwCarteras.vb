
Imports System.Data.SqlClient
Public Class BrwCarteras

    Private Sub bUSCA(ByVal OP As Integer)
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            If gloPorClv_Periodo = 0 Then
                If OP = 0 Then 'NUM. CARTERA
                    If IsNumeric(Me.TextBox1.Text) = True Then
                        Me.BuscaCarterasTableAdapter.Connection = CON
                        Me.BuscaCarterasTableAdapter.Fill(Me.DataSetEDGAR.BuscaCarteras, glocdscartera, GloClv_TipSer, Me.TextBox1.Text, "01/01/1900", "01/01/1900", OP)
                    Else
                        MsgBox("Capture datos validos para realizar la busqueda ", MsgBoxStyle.Information)
                    End If
                ElseIf OP = 1 Then 'FECHA
                    If IsDate(Me.DateTimePicker1.Text) = True Then
                        Me.BuscaCarterasTableAdapter.Connection = CON
                        Me.BuscaCarterasTableAdapter.Fill(Me.DataSetEDGAR.BuscaCarteras, glocdscartera, GloClv_TipSer, 0, Me.DateTimePicker1.Text, "01/01/1900", OP)
                    Else
                        MsgBox("Capture datos validos para realizar la busqueda ", MsgBoxStyle.Information)
                    End If
                ElseIf OP = 3 Then 'TODOS
                    Me.BuscaCarterasTableAdapter.Connection = CON
                    Me.BuscaCarterasTableAdapter.Fill(Me.DataSetEDGAR.BuscaCarteras, glocdscartera, GloClv_TipSer, 0, "01/01/1900", "01/01/1900", OP)
                End If
            Else
                If OP = 0 Then 'NUM. CARTERA
                    If IsNumeric(Me.TextBox1.Text) = True Then
                        OP = gloPorClv_Periodo & OP
                        Me.BuscaCarterasTableAdapter.Connection = CON
                        Me.BuscaCarterasTableAdapter.Fill(Me.DataSetEDGAR.BuscaCarteras, glocdscartera, GloClv_TipSer, Me.TextBox1.Text, "01/01/1900", "01/01/1900", OP)
                    Else
                        MsgBox("Capture datos validos para realizar la busqueda ", MsgBoxStyle.Information)
                    End If
                ElseIf OP = 1 Then 'FECHA
                    OP = gloPorClv_Periodo & OP
                    If IsDate(Me.DateTimePicker1.Text) = True Then
                        Me.BuscaCarterasTableAdapter.Connection = CON
                        Me.BuscaCarterasTableAdapter.Fill(Me.DataSetEDGAR.BuscaCarteras, glocdscartera, GloClv_TipSer, 0, Me.DateTimePicker1.Text, "01/01/1900", OP)
                    Else
                        MsgBox("Capture datos validos para realizar la busqueda ", MsgBoxStyle.Information)
                    End If
                ElseIf OP = 3 Then 'TODOS
                    OP = gloPorClv_Periodo & OP
                    Me.BuscaCarterasTableAdapter.Connection = CON
                    Me.BuscaCarterasTableAdapter.Fill(Me.DataSetEDGAR.BuscaCarteras, glocdscartera, GloClv_TipSer, 0, "01/01/1900", "01/01/1900", OP)
                End If
            End If
            CON.Close()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub BrwCarteras_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        Me.bUSCA(3)
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        GloBndGenCartera = False
        Me.Close()
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        GloBndGenCartera = False
        If IsNumeric(Me.Clv_CarteraTextBox.Text) = True Then
            GLOClv_Cartera = Me.Clv_CarteraTextBox.Text
            GloBndGenCartera = True
            execar = True
            Me.Close()
        Else
            MsgBox("Seleccione la Cartera ", MsgBoxStyle.Information)
        End If
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        bUSCA(0)
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        bUSCA(1)
    End Sub

    Private Sub TextBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox1.KeyPress
        If Asc(e.KeyChar) = 13 Then
            bUSCA(0)
        End If
    End Sub

    Private Sub TextBox1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox1.TextChanged

    End Sub

    Private Sub DateTimePicker1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles DateTimePicker1.KeyPress
        If Asc(e.KeyChar) = 13 Then
            bUSCA(1)
        End If
    End Sub

    Private Sub DateTimePicker1_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DateTimePicker1.ValueChanged

    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        bUSCA(3)
    End Sub

    Private Sub Panel1_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles Panel1.Paint

    End Sub

    Private Sub Panel3_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles Panel3.Paint

    End Sub
End Class
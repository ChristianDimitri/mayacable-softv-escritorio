Imports System.Data.SqlClient
Public Class BrwGeneralesBancos


    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        eOpcion = "N"
        FrmGeneralesBancos.Show()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        If Me.ConGeneralesBancosDataGridView.RowCount > 0 Then
            eOpcion = "C"
            eClv_Unica = Me.Clv_UnicaTextBox.Text
            FrmGeneralesBancos.Show()
        Else
            MsgBox("No existen Registros para Consultar.", , "Atenci�n")

        End If

    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        If Me.ConGeneralesBancosDataGridView.RowCount > 0 Then
            eOpcion = "M"
            eClv_Unica = Me.Clv_UnicaTextBox.Text
            FrmGeneralesBancos.Show()
        Else
            MsgBox("No existen Registros para Modificar.", , "Atenci�n")
        End If
    End Sub

    Private Sub BrwGeneralesBancos_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.ConGeneralesBancosTableAdapter.Connection = CON
        Me.ConGeneralesBancosTableAdapter.Fill(Me.DataSetEric.ConGeneralesBancos, 0, 0)
        CON.Close()
    End Sub

    Private Sub BrwGeneralesBancos_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        colorea(Me, Me.Name)
        Me.ConGeneralesBancosTableAdapter.Connection = CON
        Me.ConGeneralesBancosTableAdapter.Fill(Me.DataSetEric.ConGeneralesBancos, 0, 0)
        CON.Close()
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Me.Close()
    End Sub
End Class
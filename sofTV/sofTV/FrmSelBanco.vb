Imports System.Data.SqlClient

Public Class FrmSelBanco

    Private Sub FrmSelBanco_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        colorea(Me, Me.Name)
        'TODO: esta l�nea de c�digo carga datos en la tabla 'NewSofTvDataSet.MUESTRABANCOS' Puede moverla o quitarla seg�n sea necesario.
        Me.MUESTRABANCOSTableAdapter.Connection = CON
        Me.MUESTRABANCOSTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRABANCOS)
        CON.Close()

    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If LocValidaHab = 0 Then
            If IsNumeric(Me.ComboBox1.SelectedValue) = True And Len(Trim(Me.ComboBox1.Text)) > 0 Then
                GloSelBanco = Me.ComboBox1.SelectedValue
                FrmSelPeriodo.Show()
                Me.Close()
            Else
                MsgBox("Seleccione el Banco", MsgBoxStyle.Information)
            End If
        ElseIf LocValidaHab = 1 Then
            If IsNumeric(Me.ComboBox1.SelectedValue) = True And Len(Trim(Me.ComboBox1.Text)) > 0 Then
                GloBndSelBanco = True
                GloSelBanco = Me.ComboBox1.SelectedValue
                Me.Close()
            Else
                MsgBox("Seleccione el Banco", MsgBoxStyle.Information)
            End If

        End If


    End Sub
End Class
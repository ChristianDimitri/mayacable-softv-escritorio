﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmComisiones
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmComisiones))
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.CMBLabel1 = New System.Windows.Forms.Label()
        Me.cbGrupoVentas = New System.Windows.Forms.ComboBox()
        Me.dgvComisiones = New System.Windows.Forms.DataGridView()
        Me.cbCatalogoDeRangos = New System.Windows.Forms.ComboBox()
        Me.CMBLabel10 = New System.Windows.Forms.Label()
        Me.bnComisiones = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.tsbEliminar = New System.Windows.Forms.ToolStripButton()
        Me.tsbGuardar = New System.Windows.Forms.ToolStripButton()
        Me.bnSalir = New System.Windows.Forms.Button()
        Me.IdComision = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Servicio = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ComisionVenta = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ComisionRecuperado = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ComisionCompartida = New System.Windows.Forms.DataGridViewTextBoxColumn()
        CType(Me.dgvComisiones, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bnComisiones, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.bnComisiones.SuspendLayout()
        Me.SuspendLayout()
        '
        'CMBLabel1
        '
        Me.CMBLabel1.AutoSize = True
        Me.CMBLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel1.Location = New System.Drawing.Point(595, 61)
        Me.CMBLabel1.Name = "CMBLabel1"
        Me.CMBLabel1.Size = New System.Drawing.Size(117, 15)
        Me.CMBLabel1.TabIndex = 0
        Me.CMBLabel1.Text = "Grupo de Ventas:"
        '
        'cbGrupoVentas
        '
        Me.cbGrupoVentas.DisplayMember = "Grupo"
        Me.cbGrupoVentas.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbGrupoVentas.FormattingEnabled = True
        Me.cbGrupoVentas.Location = New System.Drawing.Point(718, 53)
        Me.cbGrupoVentas.Name = "cbGrupoVentas"
        Me.cbGrupoVentas.Size = New System.Drawing.Size(234, 23)
        Me.cbGrupoVentas.TabIndex = 0
        Me.cbGrupoVentas.ValueMember = "Clv_Grupo"
        '
        'dgvComisiones
        '
        Me.dgvComisiones.AllowUserToAddRows = False
        Me.dgvComisiones.AllowUserToDeleteRows = False
        Me.dgvComisiones.BackgroundColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvComisiones.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvComisiones.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvComisiones.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.IdComision, Me.Servicio, Me.ComisionVenta, Me.ComisionRecuperado, Me.ComisionCompartida})
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvComisiones.DefaultCellStyle = DataGridViewCellStyle5
        Me.dgvComisiones.Location = New System.Drawing.Point(32, 124)
        Me.dgvComisiones.Name = "dgvComisiones"
        Me.dgvComisiones.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgvComisiones.Size = New System.Drawing.Size(920, 314)
        Me.dgvComisiones.TabIndex = 2
        Me.dgvComisiones.TabStop = False
        '
        'cbCatalogoDeRangos
        '
        Me.cbCatalogoDeRangos.DisplayMember = "Rango"
        Me.cbCatalogoDeRangos.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbCatalogoDeRangos.FormattingEnabled = True
        Me.cbCatalogoDeRangos.Location = New System.Drawing.Point(718, 82)
        Me.cbCatalogoDeRangos.Name = "cbCatalogoDeRangos"
        Me.cbCatalogoDeRangos.Size = New System.Drawing.Size(234, 23)
        Me.cbCatalogoDeRangos.TabIndex = 1
        Me.cbCatalogoDeRangos.ValueMember = "CveRango"
        '
        'CMBLabel10
        '
        Me.CMBLabel10.AutoSize = True
        Me.CMBLabel10.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel10.Location = New System.Drawing.Point(652, 90)
        Me.CMBLabel10.Name = "CMBLabel10"
        Me.CMBLabel10.Size = New System.Drawing.Size(60, 15)
        Me.CMBLabel10.TabIndex = 3
        Me.CMBLabel10.Text = "Rangos:"
        '
        'bnComisiones
        '
        Me.bnComisiones.AddNewItem = Nothing
        Me.bnComisiones.CountItem = Nothing
        Me.bnComisiones.DeleteItem = Nothing
        Me.bnComisiones.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnComisiones.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsbEliminar, Me.tsbGuardar})
        Me.bnComisiones.Location = New System.Drawing.Point(0, 0)
        Me.bnComisiones.MoveFirstItem = Nothing
        Me.bnComisiones.MoveLastItem = Nothing
        Me.bnComisiones.MoveNextItem = Nothing
        Me.bnComisiones.MovePreviousItem = Nothing
        Me.bnComisiones.Name = "bnComisiones"
        Me.bnComisiones.PositionItem = Nothing
        Me.bnComisiones.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.bnComisiones.Size = New System.Drawing.Size(1008, 25)
        Me.bnComisiones.TabIndex = 2
        Me.bnComisiones.Text = "BindingNavigator1"
        '
        'tsbEliminar
        '
        Me.tsbEliminar.Image = CType(resources.GetObject("tsbEliminar.Image"), System.Drawing.Image)
        Me.tsbEliminar.Name = "tsbEliminar"
        Me.tsbEliminar.RightToLeftAutoMirrorImage = True
        Me.tsbEliminar.Size = New System.Drawing.Size(74, 22)
        Me.tsbEliminar.Text = "&Eliminar"
        '
        'tsbGuardar
        '
        Me.tsbGuardar.Image = CType(resources.GetObject("tsbGuardar.Image"), System.Drawing.Image)
        Me.tsbGuardar.Name = "tsbGuardar"
        Me.tsbGuardar.Size = New System.Drawing.Size(75, 22)
        Me.tsbGuardar.Text = "&Guardar"
        '
        'bnSalir
        '
        Me.bnSalir.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnSalir.Location = New System.Drawing.Point(860, 462)
        Me.bnSalir.Name = "bnSalir"
        Me.bnSalir.Size = New System.Drawing.Size(136, 36)
        Me.bnSalir.TabIndex = 3
        Me.bnSalir.Text = "&SALIR"
        Me.bnSalir.UseVisualStyleBackColor = True
        '
        'IdComision
        '
        Me.IdComision.DataPropertyName = "IdComision"
        Me.IdComision.HeaderText = "IdComision"
        Me.IdComision.Name = "IdComision"
        Me.IdComision.Visible = False
        '
        'Servicio
        '
        Me.Servicio.DataPropertyName = "Servicio"
        Me.Servicio.HeaderText = "Servicio"
        Me.Servicio.Name = "Servicio"
        Me.Servicio.Width = 250
        '
        'ComisionVenta
        '
        Me.ComisionVenta.DataPropertyName = "ComisionVenta"
        DataGridViewCellStyle2.Format = "C2"
        DataGridViewCellStyle2.NullValue = Nothing
        Me.ComisionVenta.DefaultCellStyle = DataGridViewCellStyle2
        Me.ComisionVenta.HeaderText = "Comisión Venta"
        Me.ComisionVenta.Name = "ComisionVenta"
        Me.ComisionVenta.Width = 200
        '
        'ComisionRecuperado
        '
        Me.ComisionRecuperado.DataPropertyName = "ComisionRecuperado"
        DataGridViewCellStyle3.Format = "C2"
        Me.ComisionRecuperado.DefaultCellStyle = DataGridViewCellStyle3
        Me.ComisionRecuperado.HeaderText = "Comisión Recuperado"
        Me.ComisionRecuperado.Name = "ComisionRecuperado"
        Me.ComisionRecuperado.Width = 200
        '
        'ComisionCompartida
        '
        Me.ComisionCompartida.DataPropertyName = "ComisionCompartida"
        DataGridViewCellStyle4.Format = "C2"
        Me.ComisionCompartida.DefaultCellStyle = DataGridViewCellStyle4
        Me.ComisionCompartida.HeaderText = "Comisión Compartida"
        Me.ComisionCompartida.Name = "ComisionCompartida"
        Me.ComisionCompartida.Width = 200
        '
        'FrmComisiones
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1008, 510)
        Me.Controls.Add(Me.bnSalir)
        Me.Controls.Add(Me.bnComisiones)
        Me.Controls.Add(Me.cbCatalogoDeRangos)
        Me.Controls.Add(Me.CMBLabel10)
        Me.Controls.Add(Me.dgvComisiones)
        Me.Controls.Add(Me.cbGrupoVentas)
        Me.Controls.Add(Me.CMBLabel1)
        Me.Name = "FrmComisiones"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Comisiones"
        CType(Me.dgvComisiones, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bnComisiones, System.ComponentModel.ISupportInitialize).EndInit()
        Me.bnComisiones.ResumeLayout(False)
        Me.bnComisiones.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents CMBLabel1 As System.Windows.Forms.Label
    Friend WithEvents cbGrupoVentas As System.Windows.Forms.ComboBox
    Friend WithEvents dgvComisiones As System.Windows.Forms.DataGridView
    Friend WithEvents cbCatalogoDeRangos As System.Windows.Forms.ComboBox
    Friend WithEvents CMBLabel10 As System.Windows.Forms.Label
    Friend WithEvents bnComisiones As System.Windows.Forms.BindingNavigator
    Friend WithEvents tsbEliminar As System.Windows.Forms.ToolStripButton
    Friend WithEvents tsbGuardar As System.Windows.Forms.ToolStripButton
    Friend WithEvents bnSalir As System.Windows.Forms.Button
    Friend WithEvents IdComision As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Servicio As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ComisionVenta As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ComisionRecuperado As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ComisionCompartida As System.Windows.Forms.DataGridViewTextBoxColumn
End Class

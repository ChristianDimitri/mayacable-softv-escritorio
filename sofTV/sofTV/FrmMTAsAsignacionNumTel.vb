﻿Imports Softv.BAL
Public Class FrmMTAsAsignacionNumTel

#Region "Atributos"
    Public _idAparato As Long = 0, _MAC As String = "", _MACMTA As String = ""
#End Region
#Region "Métodos y funciones"
    Public Sub BindControles()
        Try
            txtIDAparato.Text = _idAparato.ToString()
            txtMAC.Text = _MAC
            txtMACMTA.Text = _MACMTA
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub
    Public Sub GetTelefonosDisponibles()
        Try
            cmbNumerosDisponibles.DataSource = Nothing
            cmbNumerosDisponibles.ValueMember = "idTelefono"
            cmbNumerosDisponibles.DisplayMember = "numero_tel"
            cmbNumerosDisponibles.DataSource = RelMTATelefono.GetTelefonosDisponiblesAsignacionMTA()

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub
    Public Sub GetRelMTATelefonos(ByVal idAparato As Integer)
        Try
            dgvNumerosAsignados.DataSource = Nothing
            dgvNumerosAsignados.DataSource = RelMTATelefono.GetRelMTATelefonoByIDAparato(idAparato)
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub
    Public Function AddRelMTATelefono() As Long
        Dim idIngresado As Long = 0
        If (ValidateFields()) Then
            idIngresado = RelMTATelefono.Add(0, Me._idAparato, Long.Parse(cmbNumerosDisponibles.SelectedValue.ToString()), Nothing, GloUsuario)
        End If
        Return idIngresado
    End Function
    Public Sub DeleteRelMTATelefono(ByVal idRel As Long)
        Try
            RelMTATelefono.Delete(idRel)
            GetRelMTATelefonos(Me._idAparato)
            MsgBox("Se ha eliminado correctamente la relación.", MsgBoxStyle.Information)
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        End Try
    End Sub
    Public Function ValidateFields() As Boolean
        Dim validos As Boolean = True
        If (cmbNumerosDisponibles.Items.Count <= 0 Or Me._idAparato <= 0) Then
            validos = False
        End If
        Return validos
    End Function
    Public Sub ValidaAgregar()
        If (cmbNumerosDisponibles.Items.Count <= 0) Then
            btnAgregar.Enabled = False
        Else
            btnAgregar.Enabled = True
        End If
    End Sub
    Public Sub ValidaEliminar()
        If (dgvNumerosAsignados.RowCount <= 0) Then
            btnEliminar.Enabled = False
        Else
            btnEliminar.Enabled = True
        End If
    End Sub
    Public Sub ValidaControles()
        ValidaAgregar()
        ValidaEliminar()
    End Sub
#End Region
#Region "Eventos y controles"
    Private Sub btnAgregar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAgregar.Click
        If dgvNumerosAsignados.Rows.Count = 2 Then
            MessageBox.Show("Sólo se pueden agregar 2 números telefónicos por ATA.")
            Return
        End If
        If (AddRelMTATelefono()) Then
            GetTelefonosDisponibles()
            GetRelMTATelefonos(Me._idAparato)
            ValidaControles()
            MsgBox("Se ha ingresado correctamente", MsgBoxStyle.Information)
        Else
            MsgBox("No se ha ingresado la relación.", MsgBoxStyle.Exclamation)
        End If
    End Sub

    Private Sub btnEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEliminar.Click
        Try
            DeleteRelMTATelefono(Long.Parse(dgvNumerosAsignados.CurrentRow.Cells(0).Value.ToString()))
            GetTelefonosDisponibles()
            ValidaControles()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub FrmMTAsAsignacionNumTel_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        colorea(Me, Me.Name)
        BindControles()
        GetTelefonosDisponibles()
        GetRelMTATelefonos(Me._idAparato)
        ValidaControles()
        VALIDANumero(txtMAC.Text) '***jonathan 02/05/2013**
    End Sub

#End Region


    Private Sub VALIDANumero(ByVal mac As String)
        Dim NumATA As Boolean = False
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@MAC", SqlDbType.VarChar, mac)
        BaseII.CreateMyParameter("@EXISTE", ParameterDirection.Output, SqlDbType.Bit)
        BaseII.ProcedimientoOutPut("telAsignadoATA")
        NumATA = Boolean.Parse(BaseII.dicoPar("@EXISTE").ToString())
        If NumATA = True Then
            btnAgregar.Enabled = False
            btnEliminar.Enabled = False
        End If
    End Sub

End Class
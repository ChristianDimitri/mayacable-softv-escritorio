﻿Public Class FrmComisiones

    Dim refrescar As Boolean = False

    Private Sub MUESTRAGrupoVentas()
        BaseII.limpiaParametros()
        cbGrupoVentas.DataSource = BaseII.ConsultaDT("MUESTRAGrupoVentas")
    End Sub

    Private Sub CONSULTACatalogoDeRangos()
        BaseII.limpiaParametros()
        cbCatalogoDeRangos.DataSource = BaseII.ConsultaDT("CONSULTACatalogoDeRangos")
    End Sub

    Private Sub CONComisiones(ByVal IdGrupo As Integer, ByVal IdRango As Integer)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@IdGrupo", SqlDbType.Int, IdGrupo)
        BaseII.CreateMyParameter("@IdRango", SqlDbType.Int, IdRango)
        dgvComisiones.DataSource = BaseII.ConsultaDT("CONComisiones")
    End Sub

    Private Sub MODComisiones(ByVal IdComision As Integer, ByVal ComisionVenta As Decimal, ByVal ComisionRecuperado As Decimal, ByVal ComisionCompartida As Decimal)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@IdComision", SqlDbType.Int, IdComision)
        BaseII.CreateMyParameter("@ComisionVenta", SqlDbType.Decimal, ComisionVenta)
        BaseII.CreateMyParameter("@ComisionRecuperado", SqlDbType.Decimal, ComisionRecuperado)
        BaseII.CreateMyParameter("@ComisionCompartida", SqlDbType.Decimal, ComisionCompartida)
        BaseII.Inserta("MODComisiones")
    End Sub

    Private Sub BORComisiones(ByVal IdGrupo As Integer, ByVal IdRango As Integer)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@IdGrupo", SqlDbType.Int, IdGrupo)
        BaseII.CreateMyParameter("@IdRango", SqlDbType.Int, IdRango)
        BaseII.Inserta("BORComisiones")
    End Sub

    Private Sub FrmComisiones_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
        MUESTRAGrupoVentas()
        CONSULTACatalogoDeRangos()
        CONComisiones(cbGrupoVentas.SelectedValue, cbCatalogoDeRangos.SelectedValue)
        refrescar = True
    End Sub

    Private Sub tsbGuardar_Click(sender As System.Object, e As System.EventArgs) Handles tsbGuardar.Click
        dgvComisiones.EndEdit()
        Dim x As Integer
        For x = 0 To dgvComisiones.RowCount - 1
            MODComisiones(dgvComisiones.Item("IdComision", x).Value, dgvComisiones.Item("ComisionVenta", x).Value, dgvComisiones.Item("ComisionRecuperado", x).Value, dgvComisiones.Item("ComisionCompartida", x).Value)
        Next
        MessageBox.Show("Se guardó con éxito.")
    End Sub

    Private Sub tsbEliminar_Click(sender As System.Object, e As System.EventArgs) Handles tsbEliminar.Click
        BORComisiones(cbGrupoVentas.SelectedValue, cbCatalogoDeRangos.SelectedValue)
        MessageBox.Show("Se eliminó con éxito.")
        CONComisiones(cbGrupoVentas.SelectedValue, cbCatalogoDeRangos.SelectedValue)
    End Sub

    Private Sub bnSalir_Click(sender As System.Object, e As System.EventArgs) Handles bnSalir.Click
        Me.Close()
    End Sub

    Private Sub cbGrupoVentas_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles cbGrupoVentas.SelectedIndexChanged
        If refrescar = True Then
            CONComisiones(cbGrupoVentas.SelectedValue, cbCatalogoDeRangos.SelectedValue)
        End If
    End Sub

    Private Sub cbCatalogoDeRangos_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles cbCatalogoDeRangos.SelectedIndexChanged
        If refrescar = True Then
            CONComisiones(cbGrupoVentas.SelectedValue, cbCatalogoDeRangos.SelectedValue)
        End If
    End Sub
End Class
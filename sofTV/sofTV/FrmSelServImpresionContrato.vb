﻿Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports System.Reflection
Imports System.Data.SqlClient
Imports System.Net
Imports System.Net.Sockets
Imports System.IO
Imports System
Imports System.IO.StreamReader
Imports System.IO.File
Imports System.Collections.Generic

Public Class FrmSelServImpresionContrato


    Private Sub FrmSelServImpresionContrato_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Servicio()
    End Sub
    Private Sub Servicio()
        Dim conn As New SqlConnection(MiConexion)
        Try

            conn.Open()
            Dim comando As New SqlClient.SqlCommand("uspMuestraTiposServicios", conn)
            comando.CommandType = CommandType.StoredProcedure
            comando.CommandTimeout = 0

            Dim parametro As New SqlParameter("@clvTipSer", SqlDbType.Int)
            parametro.Direction = ParameterDirection.Input
            parametro.Value = 6
            comando.Parameters.Add(parametro)

            Dim Adaptador As New SqlDataAdapter()
            Adaptador.SelectCommand = comando

            'Agrego el parámetro
            'Adaptador.SelectCommand.Parameters.Add("@Clv_Txt", SqlDbType.VarChar, 150).Value = eCompania

            Dim Dataset As New DataSet
            'Creamos nuestros BidingSource
            Dim Bs As New BindingSource
            'Llenamos el Adaptador con el DataSet
            Adaptador.Fill(Dataset, "uspMuestraTiposServicios")
            Bs.DataSource = Dataset.Tables("uspMuestraTiposServicios")

            'dgvResultadosClasificacion.DataSource = Bs
            ComboBox1Servicios.DataSource = Bs
            ComboBox1Servicios.DisplayMember = "Concepto"
            ComboBox1Servicios.ValueMember = "Clv_TipSer"
        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            conn.Close()
        End Try

    End Sub
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        ServicioContrato = ComboBox1Servicios.SelectedValue
        If IMPRESORA_CONTRATOS.Length = 0 Then
            MessageBox.Show("No se ha asignado una impresora de contratos en esta sucursal/caja.")
            Exit Sub
        End If

        Dim DS As New DataSet
        DS.Clear()
        If ServicioContrato = 1 Then

            Dim listatablas As New List(Of String)
            listatablas.Add("Cliente")
            listatablas.Add("DatosFiscales")
            listatablas.Add("Servicios")
            listatablas.Add("SeImprimeContratgo")

            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@CONTRATO", SqlDbType.BigInt, Contrato)
            DS = BaseII.ConsultaDS("REPORTEContratoTV", listatablas)

            If DS.Tables("SeImprimeContratgo").Rows(0)(0).ToString = "0" Then
                MessageBox.Show("El contrato no cuenta con servicio de TV.", "Contrato ::..", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Else
                FrmImprimirContrato_TvInternet.Show()
            End If

        ElseIf ServicioContrato = 2 Then

            Dim listatablas As New List(Of String)
            listatablas.Add("Cliente")
            listatablas.Add("DatosFiscales")
            listatablas.Add("Servicios")
            listatablas.Add("SeImprimeContratgo")

            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@CONTRATO", SqlDbType.BigInt, Contrato)
            DS = BaseII.ConsultaDS("REPORTEContratoNET", listatablas)

            If DS.Tables("SeImprimeContratgo").Rows(0)(0).ToString = "0" Then
                MessageBox.Show("El contrato no cuenta con servicio de Internet.", "Contrato ::..", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Else
                FrmImprimirContrato_TvInternet.Show()
            End If

        End If


            'If ServicioContrato = 1 Then
            '  BrwClientes.REPORTEContratoTV(BrwClientes.DataGridView1.SelectedCells(0).Value)
            ' ElseIf ServicioContrato = 2 Then
            'BrwClientes.REPORTEContratoNET(BrwClientes.DataGridView1.SelectedCells(0).Value)
            ' End If
            Me.Close()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub

    'Private Sub ComboBox1Servicios_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox1Servicios.SelectedIndexChanged
    '    'If Me.ComboBox1Servicios.SelectedValue <> " " Then
    '    ServicioContrato = Me.ComboBox1Servicios.SelectedValue
    '    ' End If
    'End Sub
End Class
﻿Public Class FrmDesconexionSeleccionColonias



    Private Sub MUESTRATBLDESCONEXIONSELECCIONCOLONIAS(ByVal CLV_SESSION As Integer, ByVal OP As Integer)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@CLV_SESSION", SqlDbType.Int, CLV_SESSION)
        BaseII.CreateMyParameter("@OP", SqlDbType.Int, OP)
        If OP = 0 Then dgvIzq.DataSource = BaseII.ConsultaDT("MUESTRATBLDESCONEXIONSELECCIONCOLONIAS")
        If OP = 1 Then dgvDer.DataSource = BaseII.ConsultaDT("MUESTRATBLDESCONEXIONSELECCIONCOLONIAS")
    End Sub

    Private Sub INSERTATBLDESCONEXIONSELECCIONCOLONIAS(ByVal CLV_SESSION As Integer, ByVal OP As Integer, ByVal CLV_CIUDAD As Integer)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@CLV_SESSION", SqlDbType.Int, CLV_SESSION)
        BaseII.CreateMyParameter("@OP", SqlDbType.Int, OP)
        BaseII.CreateMyParameter("@CLV_COLONIA", SqlDbType.Int, CLV_CIUDAD)
        BaseII.Inserta("INSERTATBLDESCONEXIONSELECCIONCOLONIAS")
    End Sub

    Private Sub ELIMINATBLDESCONEXIONSELECCIONCOLONIAS(ByVal CLV_SESSION As Integer, ByVal OP As Integer, ByVal CLV_CIUDAD As Integer)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@CLV_SESSION", SqlDbType.Int, CLV_SESSION)
        BaseII.CreateMyParameter("@OP", SqlDbType.Int, OP)
        BaseII.CreateMyParameter("@CLV_COLONIA", SqlDbType.Int, CLV_CIUDAD)
        BaseII.Inserta("ELIMINATBLDESCONEXIONSELECCIONCOLONIAS")
    End Sub

    Private Sub DameClv_Session()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@CLV_SESSION", ParameterDirection.Output, SqlDbType.Int)
        BaseII.ProcedimientoOutPut("DameClv_Session")
        eClv_Session = 0
        eClv_Session = CInt(BaseII.dicoPar("@CLV_SESSION").ToString)
    End Sub

    Private Function DAMEClientesADesconectarTV(ByVal CLV_SESSION As Integer, ByVal CLV_USUARIO As String) As DataTable
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@CLV_SESSION", SqlDbType.Int, CLV_SESSION)
        BaseII.CreateMyParameter("@CLV_USUARIO", SqlDbType.VarChar, CLV_USUARIO, 10)
        Return BaseII.ConsultaDT("DAMEClientesADesconectarTV")
    End Function

    Private Sub GENERACarteraDesconexionTV()
        BaseII.limpiaParametros()
        BaseII.Inserta("GENERACarteraDesconexionTV")
    End Sub

    Private Sub GENERADesconexionTV(ByVal Contrato As Integer)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Contrato", SqlDbType.Int, Contrato)
        BaseII.Inserta("GENERADesconexionTV")
    End Sub

    Private Sub GENERAGeneralDesconexionTV(ByVal Clv_Session As Integer)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_Session", SqlDbType.Int, Clv_Session)
        BaseII.Inserta("GENERAGeneralDesconexionTV")
    End Sub

    Private Sub MUESTRAMENSAJEDESCONEXIONTV()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@MSJ", ParameterDirection.Output, SqlDbType.VarChar, 150)
        BaseII.ProcedimientoOutPut("MUESTRAMENSAJEDESCONEXIONTV")
        lbMensaje.Text = BaseII.dicoPar("@MSJ")
    End Sub

    Private Sub INSERTAtblSeleccionColonia(ByVal Clv_Session As Integer, ByVal Op As Integer, Clv_Colonia As Integer)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_Session", SqlDbType.Int, Clv_Session)
        BaseII.CreateMyParameter("@Op", SqlDbType.Int, Op)
        BaseII.CreateMyParameter("@Clv_Colonia", SqlDbType.Int, Clv_Colonia)
        BaseII.Inserta("INSERTAtblSeleccionColonia")
    End Sub
    Private Sub FrmDesconexionSeleccionCiudades_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
        MUESTRATBLDESCONEXIONSELECCIONCOLONIAS(eClv_Session, 0)
        MUESTRATBLDESCONEXIONSELECCIONCOLONIAS(eClv_Session, 1)
        MUESTRAMENSAJEDESCONEXIONTV()
    End Sub

    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles Button1.Click
        If dgvIzq.RowCount = 0 Then
            MessageBox.Show("Selecciona un registro.")
            Exit Sub
        End If
        If dgvIzq.SelectedCells(0).Value = 0 Then
            Exit Sub
        End If
        INSERTATBLDESCONEXIONSELECCIONCOLONIAS(eClv_Session, 0, dgvIzq.SelectedCells(0).Value)
        MUESTRATBLDESCONEXIONSELECCIONCOLONIAS(eClv_Session, 0)
        MUESTRATBLDESCONEXIONSELECCIONCOLONIAS(eClv_Session, 1)
    End Sub

    Private Sub Button2_Click(sender As System.Object, e As System.EventArgs) Handles Button2.Click
        If dgvIzq.RowCount = 0 Then
            MessageBox.Show("Selecciona un registro.")
            Exit Sub
        End If
        INSERTATBLDESCONEXIONSELECCIONCOLONIAS(eClv_Session, 1, 0)
        MUESTRATBLDESCONEXIONSELECCIONCOLONIAS(eClv_Session, 0)
        MUESTRATBLDESCONEXIONSELECCIONCOLONIAS(eClv_Session, 1)
    End Sub

    Private Sub Button3_Click(sender As System.Object, e As System.EventArgs) Handles Button3.Click
        If dgvDer.RowCount = 0 Then
            MessageBox.Show("Selecciona un registro.")
            Exit Sub
        End If
        ELIMINATBLDESCONEXIONSELECCIONCOLONIAS(eClv_Session, 0, dgvDer.SelectedCells(0).Value)
        MUESTRATBLDESCONEXIONSELECCIONCOLONIAS(eClv_Session, 0)
        MUESTRATBLDESCONEXIONSELECCIONCOLONIAS(eClv_Session, 1)
    End Sub

    Private Sub Button4_Click(sender As System.Object, e As System.EventArgs) Handles Button4.Click
        If dgvDer.RowCount = 0 Then
            MessageBox.Show("Selecciona un registro.")
            Exit Sub
        End If
        ELIMINATBLDESCONEXIONSELECCIONCOLONIAS(eClv_Session, 1, dgvDer.SelectedCells(0).Value)
        MUESTRATBLDESCONEXIONSELECCIONCOLONIAS(eClv_Session, 0)
        MUESTRATBLDESCONEXIONSELECCIONCOLONIAS(eClv_Session, 1)
    End Sub

    Private Sub bnAceptar_Click(sender As System.Object, e As System.EventArgs) Handles bnGenerar.Click
        Try
            If dgvDer.RowCount = 0 Then
                MessageBox.Show("Selecciona una Colonia.")
                Exit Sub
            End If

            Dim x As Integer
            Dim dTable As New DataTable

            dTable = DAMEClientesADesconectarTV(eClv_Session, GloUsuario)

            If dTable.Rows.Count = 0 Then
                MessageBox.Show("No se encontraron Clientes para generar su desconexión.")
                Me.Close()
            End If

            If dTable.Rows.Count > 0 Then
                MessageBox.Show("A " + dTable.Rows.Count.ToString() + " Clientes se les realizará su desconexión.")
            End If

            GENERACarteraDesconexionTV()

            For x = 0 To dTable.Rows.Count - 1
                lbStatus.Text = "Generando " + (x + 1).ToString + " de " + dTable.Rows.Count.ToString() + " desconexiones..."
                lbStatus.Refresh()
                GENERADesconexionTV(CInt(dTable.Rows(x)(1).ToString()))
            Next

            GENERAGeneralDesconexionTV(eClv_Session)
            GENERACarteraDesconexionTV()

            MessageBox.Show("Finalizó con éxito.")

            INSERTAtblSeleccionColonia(eClv_Session, 2, 0)
            eOpVentas = 101
            FrmImprimirComision.Show()

            Me.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub bnSalir_Click(sender As System.Object, e As System.EventArgs) Handles bnSalir.Click
        Me.Close()
    End Sub
End Class
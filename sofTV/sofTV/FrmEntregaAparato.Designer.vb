﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmEntregaAparato
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.CMBlblDig = New System.Windows.Forms.Label()
        Me.CMBlblNet = New System.Windows.Forms.Label()
        Me.btnAceptar = New System.Windows.Forms.Button()
        Me.bntCancelar = New System.Windows.Forms.Button()
        Me.txtDig = New System.Windows.Forms.TextBox()
        Me.txtNet = New System.Windows.Forms.TextBox()
        Me.CMBLabel1 = New System.Windows.Forms.Label()
        Me.Muestra_ServiciosDigitalesTableAdapter1 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter2 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.Muestra_ServiciosDigitalesTableAdapter3 = New sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter()
        Me.SuspendLayout()
        '
        'CMBlblDig
        '
        Me.CMBlblDig.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBlblDig.ForeColor = System.Drawing.Color.Black
        Me.CMBlblDig.Location = New System.Drawing.Point(92, 73)
        Me.CMBlblDig.Name = "CMBlblDig"
        Me.CMBlblDig.Size = New System.Drawing.Size(217, 23)
        Me.CMBlblDig.TabIndex = 0
        Me.CMBlblDig.Text = "Número de Aparatos Digitales:"
        Me.CMBlblDig.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'CMBlblNet
        '
        Me.CMBlblNet.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBlblNet.ForeColor = System.Drawing.Color.Black
        Me.CMBlblNet.Location = New System.Drawing.Point(117, 111)
        Me.CMBlblNet.Name = "CMBlblNet"
        Me.CMBlblNet.Size = New System.Drawing.Size(192, 23)
        Me.CMBlblNet.TabIndex = 1
        Me.CMBlblNet.Text = "Número de Cablemodems:"
        Me.CMBlblNet.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'btnAceptar
        '
        Me.btnAceptar.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAceptar.Location = New System.Drawing.Point(203, 180)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(144, 36)
        Me.btnAceptar.TabIndex = 2
        Me.btnAceptar.Text = "&ACEPTAR"
        Me.btnAceptar.UseVisualStyleBackColor = True
        '
        'bntCancelar
        '
        Me.bntCancelar.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bntCancelar.Location = New System.Drawing.Point(353, 180)
        Me.bntCancelar.Name = "bntCancelar"
        Me.bntCancelar.Size = New System.Drawing.Size(144, 36)
        Me.bntCancelar.TabIndex = 3
        Me.bntCancelar.Text = "&CANCELAR"
        Me.bntCancelar.UseVisualStyleBackColor = True
        '
        'txtDig
        '
        Me.txtDig.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDig.Location = New System.Drawing.Point(315, 73)
        Me.txtDig.Name = "txtDig"
        Me.txtDig.Size = New System.Drawing.Size(79, 21)
        Me.txtDig.TabIndex = 4
        Me.txtDig.Text = "0"
        '
        'txtNet
        '
        Me.txtNet.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNet.Location = New System.Drawing.Point(315, 111)
        Me.txtNet.Name = "txtNet"
        Me.txtNet.Size = New System.Drawing.Size(79, 21)
        Me.txtNet.TabIndex = 5
        Me.txtNet.Text = "0"
        '
        'CMBLabel1
        '
        Me.CMBLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel1.ForeColor = System.Drawing.Color.Black
        Me.CMBLabel1.Location = New System.Drawing.Point(21, 18)
        Me.CMBLabel1.Name = "CMBLabel1"
        Me.CMBLabel1.Size = New System.Drawing.Size(244, 38)
        Me.CMBLabel1.TabIndex = 6
        Me.CMBLabel1.Text = "Entrega de Aparatos"
        Me.CMBLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Muestra_ServiciosDigitalesTableAdapter1
        '
        Me.Muestra_ServiciosDigitalesTableAdapter1.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter2
        '
        Me.Muestra_ServiciosDigitalesTableAdapter2.ClearBeforeFill = True
        '
        'Muestra_ServiciosDigitalesTableAdapter3
        '
        Me.Muestra_ServiciosDigitalesTableAdapter3.ClearBeforeFill = True
        '
        'FrmEntregaAparato
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(509, 228)
        Me.Controls.Add(Me.CMBLabel1)
        Me.Controls.Add(Me.txtNet)
        Me.Controls.Add(Me.txtDig)
        Me.Controls.Add(Me.bntCancelar)
        Me.Controls.Add(Me.btnAceptar)
        Me.Controls.Add(Me.CMBlblNet)
        Me.Controls.Add(Me.CMBlblDig)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "FrmEntregaAparato"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Entrega de Aparatos"
        Me.TopMost = True
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents CMBlblDig As System.Windows.Forms.Label
    Friend WithEvents CMBlblNet As System.Windows.Forms.Label
    Friend WithEvents btnAceptar As System.Windows.Forms.Button
    Friend WithEvents bntCancelar As System.Windows.Forms.Button
    Friend WithEvents txtDig As System.Windows.Forms.TextBox
    Friend WithEvents txtNet As System.Windows.Forms.TextBox
    Friend WithEvents CMBLabel1 As System.Windows.Forms.Label
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter1 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter2 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
    Friend WithEvents Muestra_ServiciosDigitalesTableAdapter3 As sofTV.DataSetyahveTableAdapters.Muestra_ServiciosDigitalesTableAdapter
End Class

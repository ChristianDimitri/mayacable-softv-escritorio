Imports System.Data.SqlClient
Public Class BrwCablemodems

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub


    Private Sub BUSCA(ByVal OP As Integer)
        Me.Clv_calleLabel2.Text = "0"
        Me.CMBNombreTextBox.Text = ""
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Try
            If OP = 0 Then
                If IsNumeric(Me.TextBox1.Text) = True Then
                    Me.BUSCAAparatosTableAdapter.Connection = CON
                    Me.BUSCAAparatosTableAdapter.Fill(Me.NewSofTvDataSet.BUSCAAparatos, New System.Nullable(Of Integer)(CType(Me.TextBox1.Text, Integer)), "", New System.Nullable(Of Integer)(CType(0, Integer)), Me.ComboBox1.SelectedValue)
                Else
                    MsgBox(mensaje3)
                End If
            ElseIf OP = 1 Then
                If Len(Trim(Me.TextBox2.Text)) > 0 Then
                    Me.BUSCAAparatosTableAdapter.Connection = CON
                    Me.BUSCAAparatosTableAdapter.Fill(Me.NewSofTvDataSet.BUSCAAparatos, New System.Nullable(Of Integer)(CType(0, Integer)), Me.TextBox2.Text, New System.Nullable(Of Integer)(CType(1, Integer)), Me.ComboBox1.SelectedValue)
                Else
                    MsgBox(mensaje4)
                End If
            Else
                Me.BUSCAAparatosTableAdapter.Connection = CON
                Me.BUSCAAparatosTableAdapter.Fill(Me.NewSofTvDataSet.BUSCAAparatos, New System.Nullable(Of Integer)(CType(0, Integer)), "", New System.Nullable(Of Integer)(CType(2, Integer)), Me.ComboBox1.SelectedValue)
            End If
            Me.TextBox1.Clear()
            Me.TextBox2.Clear()
            CON.Close()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub TextBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox1.KeyPress
        If Asc(e.KeyChar) = 13 Then
            'BUSCA(0)
            If Me.TextBox1.Text.Length > 0 Then
                uspRastreoDeEquipos(String.Empty, Me.ComboBox1.SelectedValue, CInt(Me.TextBox1.Text))
            Else
                uspRastreoDeEquipos(String.Empty, 0, 0)
            End If
        End If
    End Sub


    Private Sub TextBox2_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox2.KeyPress
        If Asc(e.KeyChar) = 13 Then
            If Me.TextBox2.Text.Length > 0 Then
                uspRastreoDeEquipos(Me.TextBox2.Text, Me.ComboBox1.SelectedValue, 0)
            Else
                uspRastreoDeEquipos(String.Empty, 0, 0)
            End If
            'BUSCA(1)
        End If
    End Sub


    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        'BUSCA(0)
        If Me.TextBox1.Text.Length > 0 Then
            If IsNumeric(Me.TextBox1.Text) = True Then
                uspRastreoDeEquipos(String.Empty, Me.ComboBox1.SelectedValue, CInt(Me.TextBox1.Text))
            Else
                MsgBox("�Este campo s�lo permite valores num�ricos!", MsgBoxStyle.Information)
            End If
        Else
            uspRastreoDeEquipos(String.Empty, 0, 0)
        End If
    End Sub


    Private Sub BrwCablemodems_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        'If GloBnd = True Then
        '    GloBnd = False
        '    BUSCA(2)
        'End If
    End Sub

    Private Sub BrwCablemodems_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        Me.Button2.Visible = False
        Me.Button4.Visible = False
        Me.Button6.Visible = False

        uspRastreoDeEquipos(String.Empty, 0, 0)
        'Dim CON As New SqlConnection(MiConexion)
        'CON.Open()
        'Me.TiposAparatosTableAdapter.Connection = CON
        'Me.TiposAparatosTableAdapter.Fill(Me.NewSofTvDataSet.TiposAparatos)
        'CON.Close()
        TiposAparatos()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        opcion = "N"
        GloTipoAparato = Me.ComboBox1.SelectedValue
        FrmCablemodems.Show()
    End Sub

    Private Sub consultar()
        If GloGClv_Cablemodem > 0 Then
            GloTipoAparato = Me.ComboBox1.SelectedValue
            opcion = "C"
            GloGClv_Cablemodem = Me.Clv_calleLabel2.Text
            FrmCablemodems.Show()
        Else
            MsgBox(mensaje2)
        End If
    End Sub


    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        If Me.DataGridView1.RowCount > 0 Then
            If CInt(Me.DataGridView1.SelectedCells(0).Value) > 0 Then
                rastrearEquipos()
            End If
        Else
            MsgBox("Selecione al menos un registro a Consultar", MsgBoxStyle.Information)
        End If
    End Sub

    Private Sub modificar()
        If GloGClv_Cablemodem > 0 Then
            GloTipoAparato = Me.ComboBox1.SelectedValue
            opcion = "M"
            GloGClv_Cablemodem = Me.Clv_calleLabel2.Text
            FrmCablemodems.Show()
        Else
            MsgBox("Seleccione algun Tipo de Servicio")
        End If
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        If Me.DataGridView1.RowCount > 0 Then
            modificar()
        Else
            MsgBox(mensaje1)
        End If

    End Sub


    Private Sub Clv_calleLabel2_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Clv_calleLabel2.TextChanged

        'GloGClv_Cablemodem = Me.Clv_calleLabel2.Text
        

    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox1.SelectedIndexChanged
        'GloTipoAparato = Me.ComboBox1.SelectedValue
        'BUSCA(2)
        Try
            uspRastreoDeEquipos(String.Empty, Me.ComboBox1.SelectedValue, 0)
        Catch ex As Exception

        End Try
    End Sub

    Private Sub DataGridView1_CurrentCellChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DataGridView1.CurrentCellChanged
        Try
            Me.CMBNombreTextBox.Text = Me.DataGridView1.SelectedCells(1).Value.ToString
            Me.Clv_calleLabel2.Text = Me.DataGridView1.SelectedCells(0).Value.ToString
        Catch ex As Exception

        End Try
    End Sub
    Private Sub DataGridView1_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles DataGridView1.DoubleClick
        If Me.DataGridView1.RowCount > 0 Then
            If CInt(Me.DataGridView1.SelectedCells(0).Value) > 0 Then
                rastrearEquipos()
            End If
        Else
            MsgBox("Selecione al menos un registro a Consultar", MsgBoxStyle.Information)
        End If
    End Sub

    
    Private Sub DataGridView1_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellContentClick

    End Sub



    Private Sub CMBNombreTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CMBNombreTextBox.TextChanged
        Dim CON As New SqlConnection(MiConexion)
        If Len(Me.CMBNombreTextBox.Text) = 0 Then Me.CMBNombreTextBox.Text = ""
        CON.Open()
        Me.MuestraInfoAparatoTableAdapter.Connection = CON
        Me.MuestraInfoAparatoTableAdapter.Fill(Me.DataSetEric2.MuestraInfoAparato, Me.CMBNombreTextBox.Text)
        CON.Close()
    End Sub


    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        'BUSCA(1)
        If Me.TextBox2.Text.Length > 0 Then
            uspRastreoDeEquipos(Me.TextBox2.Text, Me.ComboBox1.SelectedValue, 0)
        Else
            uspRastreoDeEquipos(String.Empty, 0, 0)
        End If
    End Sub
    Private Sub rastrearEquipos()
        Dim FRM As New frmRastreoEquipos

        FRM.txtClave.Text = Me.DataGridView1.SelectedCells(0).Value.ToString
        FRM.txtSerieAparato.Text = Me.DataGridView1.SelectedCells(1).Value.ToString
        FRM.txtEstadoAparato.Text = Me.DataGridView1.SelectedCells(2).Value.ToString
        FRM.txtUbicacion.Text = Me.DataGridView1.SelectedCells(3).Value.ToString
        FRM.txtDescripcion.Text = Me.DataGridView1.SelectedCells(4).Value.ToString

        FRM.Show()
        'uspRastreoDeEquipos(String.Empty, 0, 0)
    End Sub

    Private Sub uspRastreoDeEquipos(ByVal prmMacCableModem As String, ByVal prmClvArticulo As Integer, ByVal prmClvCableModem As Integer)
        Dim CON As New SqlConnection(MiConexion)
        Dim CMD As New SqlCommand("uspRastreoDeEquipos", CON)
        CMD.CommandType = CommandType.StoredProcedure

        Dim prm1 As New SqlParameter("@macCablemodem", SqlDbType.VarChar, 50)
        prm1.Direction = ParameterDirection.Input
        prm1.Value = prmMacCableModem
        CMD.Parameters.Add(prm1)

        Dim prm2 As New SqlParameter("@clvArticulo", SqlDbType.Int)
        prm2.Direction = ParameterDirection.Input
        prm2.Value = prmClvArticulo
        CMD.Parameters.Add(prm2)

        Dim prm3 As New SqlParameter("@clvCableModem", SqlDbType.Int)
        prm3.Direction = ParameterDirection.Input
        prm3.Value = prmClvCableModem
        CMD.Parameters.Add(prm3)

        Try
            Dim DA As New SqlDataAdapter(CMD)
            Dim DT As New DataTable

            DA.Fill(DT)
            Me.DataGridView1.DataSource = DT
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        End Try
    End Sub

    Private Sub TiposAparatos()
        Try
            BaseII.limpiaParametros()
            Me.ComboBox1.DataSource = BaseII.ConsultaDT("TiposAparatos")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
End Class
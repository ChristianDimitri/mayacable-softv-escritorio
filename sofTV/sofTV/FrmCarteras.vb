Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports System.Data.SqlClient
Imports System.Collections.Generic




Public Class FrmCarteras
    Private customersByCityReport As ReportDocument
    Private op As String = Nothing
    Private Titulo As String = Nothing


    Private Sub ConfigureCrystalReports(ByVal op As String, ByVal Titulo As String)
        Try
            If GloClv_TipSer > 0 And GLOClv_Cartera > 0 Then
                customersByCityReport = New ReportDocument
                Dim connectionInfo As New ConnectionInfo
                '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
                '    "=True;User ID=DeSistema;Password=1975huli")

                'connectionInfo.ServerName = GloServerName
                'connectionInfo.DatabaseName = GloDatabaseName
                'connectionInfo.UserID = GloUserID
                'connectionInfo.Password = GloPassword

                Dim mySelectFormula As String = Titulo
                Dim ciudades1 As String = Nothing
                ciudades1 = " Ciudad(es): " + LocCiudades

                'Dim OpOrdenar As String = "0"
                'If Me.RadioButton1.Checked = True Then
                '    OpOrdenar = "0"
                'ElseIf Me.RadioButton2.Checked = True Then
                '    OpOrdenar = "1"
                'ElseIf Me.RadioButton3.Checked = True Then
                '    OpOrdenar = "2"
                'End If

                Dim reportPath As String = Nothing
                If Me.ComboPeriodo.SelectedValue = 0 Then
                    Select Case op
                        Case 4
                            reportPath = RutaReportes + "\ValorResumenCarterasrespaldo.rpt"
                        Case Else
                            If GloClv_TipSer <> 3 Then
                                reportPath = RutaReportes + "\Carteras.rpt"
                            Else
                                If IdSistema = "SA" Or IdSistema = "VA" Then
                                    reportPath = RutaReportes + "\Carteras.rpt"
                                Else
                                    reportPath = RutaReportes + "\CarterasDig.rpt"
                                End If

                            End If
                    End Select
                Else
                    Select Case op
                        Case 4
                            reportPath = RutaReportes + "\ValorResumenCarterasrespaldo.rpt"
                        Case Else
                            If GloClv_TipSer <> 3 Then
                                reportPath = RutaReportes + "\CarterasPorPeriodo_02.rpt"
                            Else
                                If IdSistema = "SA" Or IdSistema = "VA" Then
                                    reportPath = RutaReportes + "\CarterasPorPeriodo_02.rpt"
                                Else
                                    reportPath = RutaReportes + "\CarteraDig_PorPeriodo.rpt"
                                End If

                            End If
                    End Select
                End If

                Dim DS As New DataSet
                DS.Clear()

                BaseII.limpiaParametros()
                BaseII.CreateMyParameter("@CLV_CARTERA", SqlDbType.BigInt, GLOClv_Cartera)
                BaseII.CreateMyParameter("@CLV_TIPSER", SqlDbType.Int, GloClv_TipSer)

                Dim listatablas As New List(Of String)


                'DS = BaseII.ConsultaDS("ReporteCarteraPorPeriodo", listatablas)

                'customersByCityReport.Load(reportPath)
                'SetDBReport(DS, customersByCityReport)
                

                
                Dim loc_tipser As Integer = 1
                If IsNumeric(Me.ComboBox1.SelectedValue) = True Then loc_tipser = Me.ComboBox1.SelectedValue
                If Me.ComboPeriodo.SelectedValue = 0 Then
                    Select Case op
                        Case 4
                            Select Case loc_tipser
                                Case 1
                                    mySelectFormula = "Valor de Cartera de " & Me.ComboBox1.Text
                                Case 2
                                    mySelectFormula = "Valor de Cartera de " & Me.ComboBox1.Text
                                Case 3
                                    mySelectFormula = "Valor de Cartera de " & Me.ComboBox1.Text
                            End Select

                        Case Else

                            Select Case loc_tipser
                                Case 1
                                    mySelectFormula = "Estado de Cartera de " & Me.ComboBox1.Text
                                Case 2
                                    mySelectFormula = "Estado de Cartera de " & Me.ComboBox1.Text
                                Case 3
                                    mySelectFormula = "Estado de Cartera de " & Me.ComboBox1.Text
                            End Select

                    End Select
                Else
                    Select Case op
                        Case 4
                            Select Case loc_tipser
                                Case 1
                                    mySelectFormula = "Valor de Cartera de " & Me.ComboBox1.Text
                                Case 2
                                    mySelectFormula = "Valor de Cartera de " & Me.ComboBox1.Text
                                Case 3
                                    mySelectFormula = "Valor de Cartera de " & Me.ComboBox1.Text
                            End Select

                        Case Else

                            Select Case loc_tipser
                                Case 1
                                    mySelectFormula = "Estado de Cartera de " & Me.ComboBox1.Text & " del " & Me.ComboPeriodo.Text
                                Case 2
                                    mySelectFormula = "Estado de Cartera de " & Me.ComboBox1.Text & " del " & Me.ComboPeriodo.Text
                                Case 3
                                    mySelectFormula = "Estado de Cartera de " & Me.ComboBox1.Text & " del " & Me.ComboPeriodo.Text
                            End Select

                    End Select
                End If

                'Select Case op
                '    Case 4, 6
                'customersByCityReport.PrintOptions.PaperOrientation = PaperOrientation.Landscape
                '    Case Else
                'customersByCityReport.PrintOptions.PaperOrientation = PaperOrientation.Portrait
                'End Select
                If Me.ComboPeriodo.SelectedValue = 0 Then
                    Select Case op
                        Case 4
                            listatablas.Add("SeleccionaCartera")
                            listatablas.Add("CatalogoConceptosCartera")
                            listatablas.Add("DetCartera")
                            listatablas.Add("Servicios")
                            listatablas.Add("TipServ")
                            listatablas.Add("VistaCatalogoConceptosCartera")
                            DS = BaseII.ConsultaDS("SeleccionaCartera", listatablas)
                        Case Else
                            If GloClv_TipSer <> 3 Then
                                listatablas.Add("ReporteCartera")
                                listatablas.Add("Servicios")
                                DS = BaseII.ConsultaDS("ReporteCartera", listatablas)
                            Else
                                If IdSistema = "SA" Or IdSistema = "VA" Then
                                    listatablas.Add("ReporteCartera")
                                    listatablas.Add("Servicios")
                                    DS = BaseII.ConsultaDS("ReporteCartera", listatablas)
                                Else
                                    listatablas.Add("SeleccionaCartera")
                                    listatablas.Add("CatalogoConceptosCartera")
                                    listatablas.Add("DetCartera")
                                    listatablas.Add("Servicios")
                                    listatablas.Add("TipServ")
                                    listatablas.Add("VistaCatalogoConceptosCartera")
                                    DS = BaseII.ConsultaDS("SeleccionaCartera", listatablas)
                                End If

                            End If
                    End Select
                Else
                    Select Case op
                        Case 4
                            listatablas.Add("SeleccionaCartera")
                            listatablas.Add("CatalogoConceptosCartera")
                            listatablas.Add("DetCartera")
                            listatablas.Add("Servicios")
                            listatablas.Add("TipServ")
                            listatablas.Add("VistaCatalogoConceptosCartera")
                            DS = BaseII.ConsultaDS("SeleccionaCartera", listatablas)
                        Case Else
                            If GloClv_TipSer <> 3 Then
                                listatablas.Add("ReporteCarteraPorPeriodo")
                                listatablas.Add("Servicios")
                                DS = BaseII.ConsultaDS("ReporteCarteraPorPeriodo", listatablas)
                            Else
                                If IdSistema = "SA" Or IdSistema = "VA" Then
                                    listatablas.Add("ReporteCarteraPorPeriodo")
                                    listatablas.Add("Servicios")
                                    DS = BaseII.ConsultaDS("ReporteCarteraPorPeriodo", listatablas)
                                Else
                                    listatablas.Add("CatalogoConceptosCartera_PorPeriodo")
                                    listatablas.Add("DetCartera_PorPeriodo")
                                    listatablas.Add("PorPeriodo_SeleccionaCartera")
                                    listatablas.Add("Servicios")
                                    DS = BaseII.ConsultaDS("PorPeriodo_SeleccionaCartera", listatablas)

                                End If

                            End If
                    End Select
                End If
                customersByCityReport.Load(reportPath)
                SetDBReport(DS, customersByCityReport)


                customersByCityReport.DataDefinition.FormulaFields("Titulo").Text = "'" & mySelectFormula & "'"
                customersByCityReport.DataDefinition.FormulaFields("SubTitulo").Text = "'" & GloSucursal & "'"
                customersByCityReport.DataDefinition.FormulaFields("Sucursal").Text = "'" & ciudades1 & "'"
                'customersByCityReport.DataDefinition.FormulaFields(3).Text = "'" & op & "'"

                '--SetDBLogonForReport(connectionInfo)

                CrystalReportViewer1.ReportSource = customersByCityReport
                SetDBLogonForReport2(connectionInfo)
                customersByCityReport = Nothing
            End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub FrmCarteras_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        Dim CON As New SqlConnection(MiConexion)

        Dim resp As MsgBoxResult = MsgBoxResult.Yes

        'GloBndGenCartera = False
        'If IsNumeric(Me.Clv_CarteraTextBox.Text) = True Then
        '    GLOClv_Cartera = Me.Clv_CarteraTextBox.Text
        '    GloBndGenCartera = True
        '    execar = True
        '    Me.Close()
        'Else
        '    MsgBox("Seleccione la Cartera ", MsgBoxStyle.Information)
        'End If

        If execar = True Then
            execar = False
            If op = 0 Then
                If GloClv_TipSer = 1 Then
                    Genera_Cartera(glosessioncar, GloClv_TipSer, 0, GloUsuario)
                    'ReporteUsp_Genera_Reporte_Cartera()
                    ReporteCartera()
                ElseIf GloClv_TipSer = 2 Then
                    Genera_Cartera(glosessioncar, GloClv_TipSer, 0, GloUsuario)
                    ReporteCartera()
                    'ReporteUsp_Genera_Reporte_Cartera()
                ElseIf GloClv_TipSer = 3 Then
                    Genera_CarteraDIG(glosessioncar, GloClv_TipSer, 0, GloUsuario)
                    ReporteCarteraDigital()
                ElseIf GloClv_TipSer = 5 Then
                    Genera_Cartera(glosessioncar, GloClv_TipSer, 0, GloUsuario)
                    'Genera_CarteraTEL(glosessioncar, GloClv_TipSer, 0, GloUsuario)
                    ReporteCartera()
                    'ReporteUsp_Genera_Reporte_Cartera()
                End If
            ElseIf op = 1 Then
                VerGenerar()
            Else
                GENERAMICARTERA()
            End If



                'If execar = True Then
                '    execar = False
                '    If op = 0 Then
                '        Genera_Cartera(glosessioncar, GloClv_TipSer, 0, GloUsuario)
                '        ReporteCartera()
                '    ElseIf op = 1 Then
                '        ReporteCartera()
                '    ElseIf op = 3 Then
                '        Genera_CarteraDIG(glosessioncar, GloClv_TipSer, 0, GloUsuario)
                '        ReporteCartera()
                '    ElseIf op = 5 Then
                '        Genera_CarteraTEL(glosessioncar, GloClv_TipSer, 0, GloUsuario)
                '        ReporteCartera()
                '    Else
                '        GENERAMICARTERA()
                '    End If



                '    execar = False
                '    If Me.ComboPeriodo.SelectedValue = 0 Then

                '        If op <> 4 Then
                '            ReporteCartera()
                '        Else
                '            GENERAMICARTERA()
                '        End If
                '    Else
                '        GENERAMICARTERA_PorPeriodo()
                '    End If
                'ElseIf execar = False Then
                '    'borra la session de las tablas
                '    'If IdSistema = "AG" Then
                '    '    If glosessioncar > 0 Then
                '    '        glosessioncar = 0
                '    '        Me.Borra_sessionCarteraTableAdapter.Connection = CON
                '    '        Me.Borra_sessionCarteraTableAdapter.Fill(Me.Procedimientosarnoldo4.Borra_sessionCartera, glosessioncar)
                '    '    End If
                '    'End If
            End If

        'If GloBndGenCartera = True Then
        '    GloBndGenCartera = False
        '    ConfigureCrystalReports(op, "")



        If GloBndGenCartera_Pregunta = True Then
            GloBndGenCartera_Pregunta = False
            resp = MsgBox("�Deseas guardar la Cartera?", MsgBoxStyle.YesNo)
            If resp = MsgBoxResult.No Then
                CON.Open()
                Me.BORRACARTERASTableAdapter.Connection = CON
                Me.BORRACARTERASTableAdapter.Fill(Me.DataSetEDGAR.BORRACARTERAS, GLOClv_Cartera, GloClv_TipSer)
                CON.Close()
            End If
        End If





        'End If

    End Sub

    Private Sub FrmCarteras_CausesValidationChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.CausesValidationChanged

    End Sub

    Private Sub FrmCarteras_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        GloClv_TipSer = 0
    End Sub


    Private Sub FrmCarteras_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: esta l�nea de c�digo carga datos en la tabla 'DataSetEdgarRev2.PorPerido_Seleccione' Puede moverla o quitarla seg�n sea necesario.

        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        '    GLOClv_Cartera = Me.Clv_CarteraTextBox.Text
        '    GloBndGenCartera = True
        '    execar = True
        colorea(Me, Me.Name)
        Me.Text = "Estados De Cartera"
        If IsNumeric(ComboBox1.SelectedValue) = True Then
            GloClv_TipSer = Me.ComboBox1.SelectedValue
        Else
            GloClv_TipSer = glotiposervicioppal
        End If
        'TODO: esta l�nea de c�digo carga datos en la tabla 'DataSetEDGAR.MuestraTipSerPrincipal' Puede moverla o quitarla seg�n sea necesario.
        Try
            'GloClv_TipSer = 1
            Me.PorPerido_SeleccioneTableAdapter.Connection = CON
            Me.PorPerido_SeleccioneTableAdapter.Fill(Me.DataSetEdgarRev2.PorPerido_Seleccione)
            Me.ComboPeriodo.SelectedIndex = 0
            gloPorClv_Periodo = 0
            Me.MuestraTipSerPrincipalTableAdapter.Connection = CON
            Me.MuestraTipSerPrincipalTableAdapter.Fill(Me.DataSetEDGAR.MuestraTipSerPrincipal)
            If IsNumeric(ComboBox1.SelectedValue) = True Then
                Me.Rep_CarterasTableAdapter.Connection = CON
                Me.Rep_CarterasTableAdapter.Fill(Me.DataSetEDGAR.Rep_Carteras, Me.ComboBox1.SelectedValue, 0)
                'ConfigureCrystalReports(op, "")
            End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
        CON.Close()
        'If IdSistema = "AG" Or IdSistema = "VA" Then
        '    Me.Label1.Visible = True
        '    Me.ComboPeriodo.Visible = True
        'End If
    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox1.SelectedIndexChanged
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Try

            If IsNumeric(ComboBox1.SelectedValue) = True Then
                GloClv_TipSer = Me.ComboBox1.SelectedValue
                Me.Rep_CarterasTableAdapter.Connection = CON
                Me.Rep_CarterasTableAdapter.Fill(Me.DataSetEDGAR.Rep_Carteras, Me.ComboBox1.SelectedValue, 0)
            End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
        CON.Close()

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Me.Close()
    End Sub

    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        Try
            Dim myTables As Tables = myReportDocument.Database.Tables
            Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
            For Each myTable In myTables
                Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
                myTableLogonInfo.ConnectionInfo = myConnectionInfo
                myTable.ApplyLogOnInfo(myTableLogonInfo)
                myTable.Location = GloDatabaseName & ".dbo." & myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)
            Next
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub SetDBLogonForReport2(ByVal myConnectionInfo As ConnectionInfo)
        Dim myTableLogOnInfos As TableLogOnInfos = Me.CrystalReportViewer1.LogOnInfo
        For Each myTableLogOnInfo As TableLogOnInfo In myTableLogOnInfos
            myTableLogOnInfo.ConnectionInfo = myConnectionInfo
        Next
    End Sub

    Private Sub Rep_CarterasDataGridView_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles Rep_CarterasDataGridView.CellClick
        If Me.Rep_CarterasDataGridView.SelectedCells(0).Value = 0 Then
            op = 0
            'FrmGeneraCarteras.Show()
            FrmSelCd_Cartera.Show()

        ElseIf Me.Rep_CarterasDataGridView.SelectedCells(0).Value = 1 Then
            op = 1
            FrmSelCuidadesCartera.Show()
        ElseIf Me.Rep_CarterasDataGridView.SelectedCells(0).Value = 4 Then
            op = 4
            FrmSelCuidadesCartera.Show()
            'BrwCarteras.Show()
        End If
    End Sub

    Private Sub Rep_CarterasDataGridView_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles Rep_CarterasDataGridView.CellContentClick

    End Sub

    Private Sub GENERAMICARTERA()
        Dim comando As SqlClient.SqlCommand
        Dim resp As MsgBoxResult = MsgBoxResult.Yes
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            If GloClv_TipSer = 1 Or GloClv_TipSer = 2 Then 'If GloClv_TipSer <> 3 Then ::antes::
                comando = New SqlClient.SqlCommand
                With comando
                    .Connection = CON
                    .CommandText = "Genera_Cartera"
                    .CommandType = CommandType.StoredProcedure
                    .CommandTimeout = 0
                    ' Create a SqlParameter for each parameter in the stored procedure.
                    Dim prm As New SqlParameter("@Clv_Cartera", SqlDbType.BigInt)
                    Dim prm1 As New SqlParameter("@clv_session", SqlDbType.BigInt)
                    Dim prm2 As New SqlParameter("@Clv_TipSer", SqlDbType.Int)
                    Dim prm3 As New SqlParameter("@TipoCartera", SqlDbType.Int)
                    Dim prm4 As New SqlParameter("@Clv_Usuario", SqlDbType.VarChar)
                    prm.Direction = ParameterDirection.Output
                    prm1.Direction = ParameterDirection.Input
                    prm2.Direction = ParameterDirection.Input
                    prm3.Direction = ParameterDirection.Input
                    prm4.Direction = ParameterDirection.Input
                    prm.Value = 0
                    prm1.Value = glosessioncar
                    prm2.Value = GloClv_TipSer
                    prm3.Value = 0
                    prm4.Value = GloUsuario

                    .Parameters.Add(prm1)
                    .Parameters.Add(prm2)
                    .Parameters.Add(prm3)
                    .Parameters.Add(prm4)
                    .Parameters.Add(prm)
                    Dim i As Integer = comando.ExecuteNonQuery()
                    GLOClv_Cartera = prm.Value
                End With
            End If
            If GloClv_TipSer = 3 Then
                'Else
                If IdSistema = "SA" Or IdSistema = "VA" Then
                    comando = New SqlClient.SqlCommand
                    With comando
                        .Connection = CON
                        .CommandText = "Genera_Cartera"
                        .CommandType = CommandType.StoredProcedure
                        .CommandTimeout = 0
                        ' Create a SqlParameter for each parameter in the stored procedure.
                        Dim prm As New SqlParameter("@Clv_Cartera", SqlDbType.BigInt)
                        Dim prm1 As New SqlParameter("@clv_session", SqlDbType.BigInt)
                        Dim prm2 As New SqlParameter("@Clv_TipSer", SqlDbType.Int)
                        Dim prm3 As New SqlParameter("@TipoCartera", SqlDbType.Int)
                        Dim prm4 As New SqlParameter("@Clv_Usuario", SqlDbType.VarChar)
                        prm.Direction = ParameterDirection.Output
                        prm1.Direction = ParameterDirection.Input
                        prm2.Direction = ParameterDirection.Input
                        prm3.Direction = ParameterDirection.Input
                        prm4.Direction = ParameterDirection.Input
                        prm.Value = 0
                        prm1.Value = glosessioncar
                        prm2.Value = GloClv_TipSer
                        prm3.Value = 0
                        prm4.Value = GloUsuario

                        .Parameters.Add(prm1)
                        .Parameters.Add(prm2)
                        .Parameters.Add(prm3)
                        .Parameters.Add(prm4)
                        .Parameters.Add(prm)
                        Dim i As Integer = comando.ExecuteNonQuery()
                        GLOClv_Cartera = prm.Value
                    End With
                Else
                    comando = New SqlClient.SqlCommand
                    With comando
                        .Connection = CON
                        .CommandText = "Genera_CarteraDIG"
                        .CommandType = CommandType.StoredProcedure
                        .CommandTimeout = 0

                        ' Create a SqlParameter for each parameter in the stored procedure.
                        Dim prm As New SqlParameter("@Clv_Cartera", SqlDbType.BigInt)
                        Dim prm1 As New SqlParameter("@clv_session2", SqlDbType.BigInt)
                        Dim prm2 As New SqlParameter("@Clv_TipSer", SqlDbType.Int)
                        Dim prm3 As New SqlParameter("@TipoCartera", SqlDbType.Int)
                        Dim prm4 As New SqlParameter("@Clv_Usuario", SqlDbType.VarChar)
                        prm.Direction = ParameterDirection.Output
                        prm1.Direction = ParameterDirection.Input
                        prm2.Direction = ParameterDirection.Input
                        prm3.Direction = ParameterDirection.Input
                        prm4.Direction = ParameterDirection.Input
                        prm.Value = 0
                        prm1.Value = glosessioncar
                        prm2.Value = GloClv_TipSer
                        prm3.Value = 0
                        prm4.Value = GloUsuario
                        .Parameters.Add(prm1)
                        .Parameters.Add(prm2)
                        .Parameters.Add(prm3)
                        .Parameters.Add(prm4)
                        .Parameters.Add(prm)
                        Dim i As Integer = comando.ExecuteNonQuery()
                        GLOClv_Cartera = prm.Value
                    End With
                End If
            End If
            If GloClv_TipSer = 5 Then
                If IdSistema = "SA" Or IdSistema = "VA" Then
                    comando = New SqlClient.SqlCommand
                    With comando
                        .Connection = CON
                        .CommandText = "Genera_Cartera"
                        .CommandType = CommandType.StoredProcedure
                        .CommandTimeout = 0
                        ' Create a SqlParameter for each parameter in the stored procedure.
                        Dim prm As New SqlParameter("@Clv_Cartera", SqlDbType.BigInt)
                        Dim prm1 As New SqlParameter("@clv_session", SqlDbType.BigInt)
                        Dim prm2 As New SqlParameter("@Clv_TipSer", SqlDbType.Int)
                        Dim prm3 As New SqlParameter("@TipoCartera", SqlDbType.Int)
                        Dim prm4 As New SqlParameter("@Clv_Usuario", SqlDbType.VarChar)
                        prm.Direction = ParameterDirection.Output
                        prm1.Direction = ParameterDirection.Input
                        prm2.Direction = ParameterDirection.Input
                        prm3.Direction = ParameterDirection.Input
                        prm4.Direction = ParameterDirection.Input
                        prm.Value = 0
                        prm1.Value = glosessioncar
                        prm2.Value = GloClv_TipSer
                        prm3.Value = 0
                        prm4.Value = GloUsuario

                        .Parameters.Add(prm1)
                        .Parameters.Add(prm2)
                        .Parameters.Add(prm3)
                        .Parameters.Add(prm4)
                        .Parameters.Add(prm)
                        Dim i As Integer = comando.ExecuteNonQuery()
                        GLOClv_Cartera = prm.Value
                    End With
                Else
                    comando = New SqlClient.SqlCommand
                    With comando
                        .Connection = CON
                        .CommandText = "Genera_Cartera_TEL"
                        .CommandType = CommandType.StoredProcedure
                        .CommandTimeout = 0

                        ' Create a SqlParameter for each parameter in the stored procedure.
                        Dim prm As New SqlParameter("@Clv_Cartera", SqlDbType.BigInt)
                        Dim prm1 As New SqlParameter("@clv_session2", SqlDbType.BigInt)
                        Dim prm2 As New SqlParameter("@Clv_TipSer", SqlDbType.Int)
                        Dim prm3 As New SqlParameter("@TipoCartera", SqlDbType.Int)
                        Dim prm4 As New SqlParameter("@Clv_Usuario", SqlDbType.VarChar)
                        prm.Direction = ParameterDirection.Output
                        prm1.Direction = ParameterDirection.Input
                        prm2.Direction = ParameterDirection.Input
                        prm3.Direction = ParameterDirection.Input
                        prm4.Direction = ParameterDirection.Input
                        prm.Value = 0
                        prm1.Value = glosessioncar
                        prm2.Value = GloClv_TipSer
                        prm3.Value = 0
                        prm4.Value = GloUsuario
                        .Parameters.Add(prm1)
                        .Parameters.Add(prm2)
                        .Parameters.Add(prm3)
                        .Parameters.Add(prm4)
                        .Parameters.Add(prm)
                        Dim i As Integer = comando.ExecuteNonQuery()
                        GLOClv_Cartera = prm.Value
                    End With
                End If
            End If

            Me.Dame_ciudad_carteraTableAdapter.Connection = CON
            Me.Dame_ciudad_carteraTableAdapter.Fill(Me.ProcedimientosArnoldo2.Dame_ciudad_cartera, glosessioncar, LocCiudades)
            ConfigureCrystalReports(op, "")
            'If IdSistema = "AG" Then
            '    Me.Borra_sessionCarteraTableAdapter.Connection = CON
            '    Me.Borra_sessionCarteraTableAdapter.Fill(Me.Procedimientosarnoldo4.Borra_sessionCartera, glosessioncar)
            'End If
            resp = MsgBox("Deseas Guardar la Cartera ", MsgBoxStyle.YesNo)
            If resp = MsgBoxResult.No Then
                Me.BORRACARTERASTableAdapter.Connection = CON
                Me.BORRACARTERASTableAdapter.Fill(Me.DataSetEDGAR.BORRACARTERAS, GLOClv_Cartera, GloClv_TipSer)
            End If
            CON.Close()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub GENERAMICARTERA_PorPeriodo()
        Dim comando As SqlClient.SqlCommand
        Dim resp As MsgBoxResult = MsgBoxResult.Yes
        Try

            If Me.ComboPeriodo.SelectedValue >= 1 Then
                Dim CON2 As New SqlConnection(MiConexion)
                CON2.Open()
                Dim comando2 As SqlClient.SqlCommand
                comando2 = New SqlClient.SqlCommand
                With comando2
                    .Connection = CON2
                    .CommandText = "PorPeriodo_GrabaPeriodoTmp "
                    .CommandType = CommandType.StoredProcedure
                    .CommandTimeout = 0
                    ' Create a SqlParameter for each parameter in the stored procedure.
                    Dim prm1 As New SqlParameter("@clv_session", SqlDbType.BigInt)
                    Dim prm2 As New SqlParameter("@Clv_Periodo", SqlDbType.Int)


                    prm1.Direction = ParameterDirection.Input
                    prm2.Direction = ParameterDirection.Input

                    prm1.Value = glosessioncar
                    prm2.Value = Me.ComboPeriodo.SelectedValue


                    .Parameters.Add(prm1)
                    .Parameters.Add(prm2)
                    Dim i As Integer = comando2.ExecuteNonQuery()
                End With
                CON2.Close()
            End If
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()

            If GloClv_TipSer <> 3 Then
                comando = New SqlClient.SqlCommand
                With comando
                    .Connection = CON
                    .CommandText = "Genera_Cartera_Por_Periodo "
                    .CommandType = CommandType.StoredProcedure
                    .CommandTimeout = 0
                    ' Create a SqlParameter for each parameter in the stored procedure.
                    Dim prm As New SqlParameter("@Clv_Cartera", SqlDbType.BigInt)
                    Dim prm1 As New SqlParameter("@clv_session", SqlDbType.BigInt)
                    Dim prm2 As New SqlParameter("@Clv_TipSer", SqlDbType.Int)
                    Dim prm3 As New SqlParameter("@TipoCartera", SqlDbType.Int)
                    Dim prm4 As New SqlParameter("@Clv_Usuario", SqlDbType.VarChar)
                    prm.Direction = ParameterDirection.Output
                    prm1.Direction = ParameterDirection.Input
                    prm2.Direction = ParameterDirection.Input
                    prm3.Direction = ParameterDirection.Input
                    prm4.Direction = ParameterDirection.Input
                    prm.Value = 0
                    prm1.Value = glosessioncar
                    prm2.Value = GloClv_TipSer
                    prm3.Value = 0
                    prm4.Value = GloUsuario

                    .Parameters.Add(prm1)
                    .Parameters.Add(prm2)
                    .Parameters.Add(prm3)
                    .Parameters.Add(prm4)
                    .Parameters.Add(prm)
                    Dim i As Integer = comando.ExecuteNonQuery()
                    GLOClv_Cartera = prm.Value
                End With
            Else
                If IdSistema = "SA" Then
                    comando = New SqlClient.SqlCommand
                    With comando
                        .Connection = CON
                        .CommandText = "Genera_Cartera_Por_Periodo "
                        .CommandType = CommandType.StoredProcedure
                        .CommandTimeout = 0
                        ' Create a SqlParameter for each parameter in the stored procedure.
                        Dim prm As New SqlParameter("@Clv_Cartera", SqlDbType.BigInt)
                        Dim prm1 As New SqlParameter("@clv_session", SqlDbType.BigInt)
                        Dim prm2 As New SqlParameter("@Clv_TipSer", SqlDbType.Int)
                        Dim prm3 As New SqlParameter("@TipoCartera", SqlDbType.Int)
                        Dim prm4 As New SqlParameter("@Clv_Usuario", SqlDbType.VarChar)
                        prm.Direction = ParameterDirection.Output
                        prm1.Direction = ParameterDirection.Input
                        prm2.Direction = ParameterDirection.Input
                        prm3.Direction = ParameterDirection.Input
                        prm4.Direction = ParameterDirection.Input
                        prm.Value = 0
                        prm1.Value = glosessioncar
                        prm2.Value = GloClv_TipSer
                        prm3.Value = 0
                        prm4.Value = GloUsuario

                        .Parameters.Add(prm1)
                        .Parameters.Add(prm2)
                        .Parameters.Add(prm3)
                        .Parameters.Add(prm4)
                        .Parameters.Add(prm)
                        Dim i As Integer = comando.ExecuteNonQuery()
                        GLOClv_Cartera = prm.Value
                    End With
                Else
                    comando = New SqlClient.SqlCommand
                    With comando
                        .Connection = CON
                        .CommandText = "Genera_CarteraDIG_PorPeriodo "
                        .CommandType = CommandType.StoredProcedure
                        .CommandTimeout = 0

                        ' Create a SqlParameter for each parameter in the stored procedure.
                        Dim prm As New SqlParameter("@Clv_Cartera", SqlDbType.BigInt)
                        Dim prm1 As New SqlParameter("@clv_session2", SqlDbType.BigInt)
                        Dim prm2 As New SqlParameter("@Clv_TipSer", SqlDbType.Int)
                        Dim prm3 As New SqlParameter("@TipoCartera", SqlDbType.Int)
                        Dim prm4 As New SqlParameter("@Clv_Usuario", SqlDbType.VarChar)
                        prm.Direction = ParameterDirection.Output
                        prm1.Direction = ParameterDirection.Input
                        prm2.Direction = ParameterDirection.Input
                        prm3.Direction = ParameterDirection.Input
                        prm4.Direction = ParameterDirection.Input
                        prm.Value = 0
                        prm1.Value = glosessioncar
                        prm2.Value = GloClv_TipSer
                        prm3.Value = 0
                        prm4.Value = GloUsuario
                        .Parameters.Add(prm1)
                        .Parameters.Add(prm2)
                        .Parameters.Add(prm3)
                        .Parameters.Add(prm4)
                        .Parameters.Add(prm)
                        Dim i As Integer = comando.ExecuteNonQuery()
                        GLOClv_Cartera = prm.Value
                    End With
                End If
            End If
            Me.Dame_ciudad_carteraTableAdapter.Connection = CON
            Me.Dame_ciudad_carteraTableAdapter.Fill(Me.ProcedimientosArnoldo2.Dame_ciudad_cartera, glosessioncar, LocCiudades)
            ConfigureCrystalReports(op, "")
            'If IdSistema = "AG" Then
            '    Me.Borra_sessionCarteraTableAdapter.Connection = CON
            '    Me.Borra_sessionCarteraTableAdapter.Fill(Me.Procedimientosarnoldo4.Borra_sessionCartera, glosessioncar)
            'End If
            CON.Close()
            resp = MsgBox("Deseas Guardar la Cartera ", MsgBoxStyle.YesNo)
            If resp = MsgBoxResult.No Then
                'Me.BORRACARTERASTableAdapter.Connection = CON
                'Me.BORRACARTERASTableAdapter.Fill(Me.DataSetEDGAR.BORRACARTERAS, GLOClv_Cartera, GloClv_TipSer)
                Dim CON3 As New SqlConnection(MiConexion)
                CON3.Open()
                Dim comando3 As SqlClient.SqlCommand
                comando3 = New SqlClient.SqlCommand
                With comando3
                    .Connection = CON3
                    .CommandText = "PorPeriodo_BORRACARTERAS"
                    .CommandType = CommandType.StoredProcedure
                    .CommandTimeout = 0
                    ' Create a SqlParameter for each parameter in the stored procedure.
                    Dim prm1 As New SqlParameter("@Clv_Cartera", SqlDbType.BigInt)
                    Dim prm2 As New SqlParameter("@Clv_Tipser", SqlDbType.Int)


                    prm1.Direction = ParameterDirection.Input
                    prm2.Direction = ParameterDirection.Input

                    prm1.Value = GLOClv_Cartera
                    prm2.Value = GloClv_TipSer


                    .Parameters.Add(prm1)
                    .Parameters.Add(prm2)
                    Dim i As Integer = comando3.ExecuteNonQuery()
                End With
                CON3.Close()
            End If

        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub



    Private Sub ComboPeriodo_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboPeriodo.SelectedIndexChanged
        If IsNumeric(Me.ComboPeriodo.SelectedValue) = True Then
            gloPorClv_Periodo = Me.ComboPeriodo.SelectedValue
        Else
            gloPorClv_Periodo = 0
        End If
    End Sub

    Private Sub Genera_Cartera(ByVal CLV_SESSION As Integer, ByVal CLV_TIPSER As Integer, ByVal TIPOCARTERA As Integer, ByVal CLV_USUARIO As String)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@CLV_SESSION", SqlDbType.Int, CInt(CLV_SESSION))
        BaseII.CreateMyParameter("@CLV_TIPSER", SqlDbType.Int, CInt(CLV_TIPSER))
        BaseII.CreateMyParameter("@TIPOCARTERA", SqlDbType.Int, CInt(TIPOCARTERA))
        BaseII.CreateMyParameter("@CLV_USUARIO", SqlDbType.VarChar, CLV_USUARIO, 150)
        BaseII.CreateMyParameter("@CLV_CARTERA", System.Data.ParameterDirection.Output, SqlDbType.Int)
        BaseII.ProcedimientoOutPut("Genera_Cartera")
        GLOClv_Cartera = CInt(BaseII.dicoPar("@CLV_CARTERA").ToString)
        GloBndGenCartera_Pregunta = True
    End Sub
    Private Sub Genera_CarteraDIG(ByVal CLV_SESSION As Integer, ByVal CLV_TIPSER As Integer, ByVal TIPOCARTERA As Integer, ByVal CLV_USUARIO As String) ', ByVal CLV_CARTERA As Integer)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@clv_session2", SqlDbType.BigInt, CInt(CLV_SESSION))
        BaseII.CreateMyParameter("@Clv_TipSer", SqlDbType.Int, CInt(CLV_TIPSER))
        BaseII.CreateMyParameter("@TipoCartera", SqlDbType.Int, CInt(TIPOCARTERA))
        BaseII.CreateMyParameter("@Clv_Usuario", SqlDbType.VarChar, CLV_USUARIO, 150)
        'BaseII.CreateMyParameter("@Clv_Cartera", SqlDbType.BigInt, CInt(CLV_CARTERA))
        BaseII.CreateMyParameter("@Clv_Cartera", System.Data.ParameterDirection.Output, SqlDbType.Int)
        BaseII.ProcedimientoOutPut("Genera_CarteraDIG")
        GLOClv_Cartera = CInt(BaseII.dicoPar("@Clv_Cartera").ToString)
        GloBndGenCartera_Pregunta = True
    End Sub
    Private Sub Genera_CarteraTEL(ByVal CLV_SESSION As Integer, ByVal CLV_TIPSER As Integer, ByVal TIPOCARTERA As Integer, ByVal CLV_USUARIO As String)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@CLV_session", SqlDbType.Int, CInt(CLV_SESSION))
        BaseII.CreateMyParameter("@Clv_TipSer", SqlDbType.Int, CInt(CLV_TIPSER))
        BaseII.CreateMyParameter("@TipoCartera", SqlDbType.Int, CInt(TIPOCARTERA))
        BaseII.CreateMyParameter("@Clv_Usuario", SqlDbType.VarChar, CLV_USUARIO, 150)
        BaseII.CreateMyParameter("@Clv_Cartera", System.Data.ParameterDirection.Output, SqlDbType.Int)
        BaseII.ProcedimientoOutPut("Genera_Cartera_TEL")
        GLOClv_Cartera = CInt(BaseII.dicoPar("@Clv_Cartera").ToString)
        GloBndGenCartera_Pregunta = True
    End Sub

    Private Function SELECCION_Usp_Genera_Reporte_Cartera(ByVal CLV_CARTERA As Integer, ByVal CLV_TIPSER As Integer, ByVal SUCURSAL As String) As DataSet
        Dim tableNameList As New List(Of String)
        tableNameList.Add("catalogoconceptoscartera")
        tableNameList.Add("DetCartera")
        tableNameList.Add("SeleccionaCartera;1")
        tableNameList.Add("Servicios")
        tableNameList.Add("Encabezados")
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_Cartera", SqlDbType.BigInt, CInt(CLV_CARTERA))
        BaseII.CreateMyParameter("@CLV_TIPSER", SqlDbType.Int, CInt(CLV_TIPSER))
        BaseII.CreateMyParameter("@SUCURSAL", SqlDbType.VarChar, CChar(SUCURSAL))
        Return BaseII.ConsultaDS("SELECCIONACARTERA", tableNameList)
    End Function

    Private Function SELECCION_Usp_Genera_Reporte_CarteraDig(ByVal CLV_CARTERA As Integer, ByVal CLV_TIPSER As Integer, ByVal SUCURSAL As String) As DataSet
        Dim tableNameList As New List(Of String)
        tableNameList.Add("Usp_Genera_Reporte_Cartera")
        'tableNameList.Add("DetCartera")
        'tableNameList.Add("SeleccionaCartera;1")
        tableNameList.Add("Servicios")
        tableNameList.Add("Encabezados")
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_Cartera", SqlDbType.BigInt, CInt(CLV_CARTERA))
        'BaseII.CreateMyParameter("@CLV_TIPSER", SqlDbType.Int, CInt(CLV_TIPSER))
        'BaseII.CreateMyParameter("@SUCURSAL", SqlDbType.VarChar, CChar(SUCURSAL))
        Return BaseII.ConsultaDS("Usp_Genera_Reporte_CarteraDig", tableNameList)
    End Function

    Private Function SELECCIONACARTERA(ByVal CLV_CARTERA As Integer, ByVal CLV_TIPSER As Integer, ByVal SUCURSAL As String) As DataSet
        Dim tableNameList As New List(Of String)
        tableNameList.Add("CatalogoConceptosCartera")
        tableNameList.Add("DetCartera")
        tableNameList.Add("SeleccionaCartera;1")
        tableNameList.Add("Servicios")
        tableNameList.Add("Encabezados")
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@CLV_CARTERA", SqlDbType.Int, CInt(CLV_CARTERA))
        BaseII.CreateMyParameter("@CLV_TIPSER", SqlDbType.Int, CInt(CLV_TIPSER))
        BaseII.CreateMyParameter("@SUCURSAL", SqlDbType.VarChar, CChar(SUCURSAL))
        Return BaseII.ConsultaDS("SELECCIONACARTERA", tableNameList)
    End Function

    Private Function SELECCIONACARTERADIG(ByVal CLV_CARTERA As Integer, ByVal CLV_TIPSER As Integer, ByVal SUCURSAL As String) As DataSet
        Dim tableNameList As New List(Of String)
        tableNameList.Add("CatalogoConceptosCartera")
        tableNameList.Add("DetCartera")
        tableNameList.Add("SeleccionaCartera;1")
        tableNameList.Add("Servicios")
        tableNameList.Add("DATOS")
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@CLV_CARTERA", SqlDbType.Int, CInt(CLV_CARTERA))
        BaseII.CreateMyParameter("@CLV_TIPSER", SqlDbType.Int, CInt(CLV_TIPSER))
        BaseII.CreateMyParameter("@SUCURSAL", SqlDbType.VarChar, CChar(SUCURSAL))
        Return BaseII.ConsultaDS("SELECCIONACARTERADIGITAL", tableNameList)
    End Function

    Private Sub ReporteCartera()
        Dim rDocument As New CrystalDecisions.CrystalReports.Engine.ReportDocument
        Dim dSet As New DataSet
        dSet = SELECCIONACARTERA(GLOClv_Cartera, GloClv_TipSer, GloSucursal)
        rDocument.Load(RutaReportes + "\Carteras.rpt")
        rDocument.SetDataSource(dSet)
        CrystalReportViewer1.ReportSource = rDocument
        rDocument = Nothing
    End Sub


    Private Sub ReporteUsp_Genera_Reporte_Cartera()
        Dim rDocument As New CrystalDecisions.CrystalReports.Engine.ReportDocument
        Dim dSet As New DataSet
        dSet = SELECCION_Usp_Genera_Reporte_Cartera(GLOClv_Cartera, GloClv_TipSer, GloSucursal)
        rDocument.Load(RutaReportes + "\Report_Usp_Genera_Reporte_Cartera.rpt")
        rDocument.SetDataSource(dSet)
        CrystalReportViewer1.ReportSource = rDocument
        rDocument = Nothing
    End Sub

    Private Sub ReporteCarteraDigital()
        Dim rDocument As New CrystalDecisions.CrystalReports.Engine.ReportDocument
        Dim dSet As New DataSet
        dSet = SELECCION_Usp_Genera_Reporte_CarteraDig(GLOClv_Cartera, GloClv_TipSer, GloSucursal)
        rDocument.Load(RutaReportes + "\Report_Usp_Genera_Reporte_CarteraDig.rpt")
        rDocument.SetDataSource(dSet)
        CrystalReportViewer1.ReportSource = rDocument
        rDocument = Nothing
    End Sub

    Private Sub VerGenerar()
        If GloClv_TipSer = 3 Then
            ReporteCarteraDigital()
        End If
        If GloClv_TipSer <> 3 Then
            ReporteCartera()
            'ReporteUsp_Genera_Reporte_Cartera()
        End If
    End Sub

End Class
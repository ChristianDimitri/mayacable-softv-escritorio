﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmEntreCalles
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmEntreCalles))
        Me.bnSalir = New System.Windows.Forms.Button()
        Me.tbEntreCalles = New System.Windows.Forms.TextBox()
        Me.bnGuardar = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.tsbGuardar = New System.Windows.Forms.ToolStripButton()
        CType(Me.bnGuardar, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.bnGuardar.SuspendLayout()
        Me.SuspendLayout()
        '
        'bnSalir
        '
        Me.bnSalir.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnSalir.Location = New System.Drawing.Point(448, 276)
        Me.bnSalir.Name = "bnSalir"
        Me.bnSalir.Size = New System.Drawing.Size(136, 36)
        Me.bnSalir.TabIndex = 0
        Me.bnSalir.Text = "&SALIR"
        Me.bnSalir.UseVisualStyleBackColor = True
        '
        'tbEntreCalles
        '
        Me.tbEntreCalles.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbEntreCalles.Location = New System.Drawing.Point(29, 41)
        Me.tbEntreCalles.MaxLength = 250
        Me.tbEntreCalles.Multiline = True
        Me.tbEntreCalles.Name = "tbEntreCalles"
        Me.tbEntreCalles.Size = New System.Drawing.Size(524, 208)
        Me.tbEntreCalles.TabIndex = 1
        '
        'bnGuardar
        '
        Me.bnGuardar.AddNewItem = Nothing
        Me.bnGuardar.CountItem = Nothing
        Me.bnGuardar.DeleteItem = Nothing
        Me.bnGuardar.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnGuardar.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsbGuardar})
        Me.bnGuardar.Location = New System.Drawing.Point(0, 0)
        Me.bnGuardar.MoveFirstItem = Nothing
        Me.bnGuardar.MoveLastItem = Nothing
        Me.bnGuardar.MoveNextItem = Nothing
        Me.bnGuardar.MovePreviousItem = Nothing
        Me.bnGuardar.Name = "bnGuardar"
        Me.bnGuardar.PositionItem = Nothing
        Me.bnGuardar.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.bnGuardar.Size = New System.Drawing.Size(596, 25)
        Me.bnGuardar.TabIndex = 4
        Me.bnGuardar.Text = "BindingNavigator1"
        '
        'tsbGuardar
        '
        Me.tsbGuardar.Image = CType(resources.GetObject("tsbGuardar.Image"), System.Drawing.Image)
        Me.tsbGuardar.Name = "tsbGuardar"
        Me.tsbGuardar.Size = New System.Drawing.Size(88, 22)
        Me.tsbGuardar.Text = "&GUARDAR"
        '
        'FrmEntreCalles
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(596, 324)
        Me.Controls.Add(Me.bnGuardar)
        Me.Controls.Add(Me.tbEntreCalles)
        Me.Controls.Add(Me.bnSalir)
        Me.Name = "FrmEntreCalles"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Entre Calles"
        Me.TopMost = True
        CType(Me.bnGuardar, System.ComponentModel.ISupportInitialize).EndInit()
        Me.bnGuardar.ResumeLayout(False)
        Me.bnGuardar.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents bnSalir As System.Windows.Forms.Button
    Friend WithEvents tbEntreCalles As System.Windows.Forms.TextBox
    Friend WithEvents bnGuardar As System.Windows.Forms.BindingNavigator
    Friend WithEvents tsbGuardar As System.Windows.Forms.ToolStripButton
End Class

﻿Public Class BrwCambioDeTipoDeServicio
    Private Sub MUESTRACambioDeTipoDeServicio(ByVal Op As Integer, ByVal Contrato As Integer)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Op", SqlDbType.Int, Op)
        BaseII.CreateMyParameter("@Contrato", SqlDbType.Int, Contrato)
        dgvClientes.DataSource = BaseII.ConsultaDT("MUESTRACambioDeTipoDeServicio")
    End Sub

    Private Sub BrwCambioDeTipoDeServicio_Activated(sender As Object, e As System.EventArgs) Handles Me.Activated
        If eRefrescar = True Then
            eRefrescar = False
            MUESTRACambioDeTipoDeServicio(0, 0)
        End If
    End Sub

    Private Sub BrwCambioDeTipoDeServicio_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
        MUESTRACambioDeTipoDeServicio(0, 0)
    End Sub

    Private Sub tbContrato_KeyDown(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles tbContrato.KeyDown
        If e.KeyValue <> 13 Then Exit Sub
        If tbContrato.Text.Length = 0 Then Exit Sub
        If IsNumeric(tbContrato.Text) = False Then Exit Sub
        MUESTRACambioDeTipoDeServicio(1, tbContrato.Text)
    End Sub

    Private Sub bnBuscarContrato_Click(sender As System.Object, e As System.EventArgs) Handles bnBuscarContrato.Click
        If tbContrato.Text.Length = 0 Then Exit Sub
        If IsNumeric(tbContrato.Text) = False Then Exit Sub
        MUESTRACambioDeTipoDeServicio(1, tbContrato.Text)
    End Sub

    Private Sub bnNuevo_Click(sender As System.Object, e As System.EventArgs) Handles bnNuevo.Click
        FrmCambioDeTipoDeServicio.Show()
    End Sub

    Private Sub bnSalir_Click(sender As System.Object, e As System.EventArgs) Handles bnSalir.Click
        Me.Close()
    End Sub
End Class
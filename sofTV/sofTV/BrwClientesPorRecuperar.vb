Imports System.Data.SqlClient
Public Class BrwClientesPorRecuperar

    Private Sub MuestraClientePorRecuperar(ByVal Op As Integer)
        Dim CON As New SqlConnection(MiConexion)

        If Op = 0 Then
            CON.Open()
            Me.MuestraClientePorRecuperarTableAdapter.Connection = CON
            Me.MuestraClientePorRecuperarTableAdapter.Fill(Me.DataSetEric2.MuestraClientePorRecuperar, 0, 0, 0, 0, Today, 0, 0, Today, Op)
            CON.Close()
        ElseIf Op = 2 Then
            If IsNumeric(Me.TextBox1.Text) = False Then
                Exit Sub
            End If
            CON.Open()
            Me.MuestraClientePorRecuperarTableAdapter.Connection = CON
            Me.MuestraClientePorRecuperarTableAdapter.Fill(Me.DataSetEric2.MuestraClientePorRecuperar, 0, CLng(Me.TextBox1.Text), 0, 0, Today, 0, 0, Today, Op)
            CON.Close()
        ElseIf Op = 3 Then
            If IsNumeric(Me.NOMBREComboBox.SelectedValue) = False Then
                Exit Sub
            End If
            CON.Open()
            Me.MuestraClientePorRecuperarTableAdapter.Connection = CON
            Me.MuestraClientePorRecuperarTableAdapter.Fill(Me.DataSetEric2.MuestraClientePorRecuperar, 0, 0, 0, CInt(Me.NOMBREComboBox.SelectedValue), Today, 0, 0, Today, Op)
            CON.Close()
        ElseIf Op = 4 Then
            CON.Open()
            Me.MuestraClientePorRecuperarTableAdapter.Connection = CON
            Me.MuestraClientePorRecuperarTableAdapter.Fill(Me.DataSetEric2.MuestraClientePorRecuperar, 0, 0, 0, 0, Me.DateTimePicker1.Value, 0, 0, Today, Op)
            CON.Close()
        ElseIf Op = 5 Then
            If IsNumeric(Me.DESCRIPCIONComboBox.SelectedValue) = False Then
                Exit Sub
            End If
            CON.Open()
            Me.MuestraClientePorRecuperarTableAdapter.Connection = CON
            Me.MuestraClientePorRecuperarTableAdapter.Fill(Me.DataSetEric2.MuestraClientePorRecuperar, 0, 0, 0, 0, Today, CInt(DESCRIPCIONComboBox.SelectedValue), 0, Today, Op)
            CON.Close()
        ElseIf Op = 6 Then
            If IsNumeric(Me.NOMBREComboBox1.SelectedValue) = False Then
                Exit Sub
            End If
            CON.Open()
            Me.MuestraClientePorRecuperarTableAdapter.Connection = CON
            Me.MuestraClientePorRecuperarTableAdapter.Fill(Me.DataSetEric2.MuestraClientePorRecuperar, 0, 0, 0, 0, Today, CInt(Me.NOMBREComboBox1.SelectedValue), 0, Today, Op)
            CON.Close()
        ElseIf Op = 7 Then
            CON.Open()
            Me.MuestraClientePorRecuperarTableAdapter.Connection = CON
            Me.MuestraClientePorRecuperarTableAdapter.Fill(Me.DataSetEric2.MuestraClientePorRecuperar, 0, 0, 0, 0, Today, 0, 0, Me.DateTimePicker2.Value, Op)
            CON.Close()
        End If
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        MuestraClientePorRecuperar(2)
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        MuestraClientePorRecuperar(3)
    End Sub

    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        MuestraClientePorRecuperar(4)
    End Sub

    Private Sub Button10_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button10.Click
        MuestraClientePorRecuperar(5)
    End Sub

    Private Sub Button8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button8.Click
        MuestraClientePorRecuperar(6)
    End Sub

    Private Sub Button9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button9.Click
        MuestraClientePorRecuperar(7)
    End Sub

    Private Sub BrwClientesPorRecuperar_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        If eBndPorRecuperar = True Then
            eBndPorRecuperar = False
            MuestraClientePorRecuperar(0)
        End If
    End Sub

    Private Sub BrwClientesPorRecuperar_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'TODO: esta l�nea de c�digo carga datos en la tabla 'DataSetEric2.MuestraStatusPorRecuperar' Puede moverla o quitarla seg�n sea necesario.
        Me.MuestraStatusPorRecuperarTableAdapter.Fill(Me.DataSetEric2.MuestraStatusPorRecuperar)
        colorea(Me, Me.Name)
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.MUESTRAUSUARIOSTableAdapter.Connection = CON
        Me.MUESTRAUSUARIOSTableAdapter.Fill(Me.NewSofTvDataSet.MUESTRAUSUARIOS, 2)
        Me.MuestraStatusPorRecuperarTableAdapter.Connection = CON
        Me.MuestraStatusPorRecuperarTableAdapter.Fill(Me.DataSetEric2.MuestraStatusPorRecuperar)
        CON.Close()

        MuestraClientePorRecuperar(0)
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Me.Close()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        eOpcion = "N"
        eBndPorRecuperar = False
        FrmClientesPorRecuperar.Show()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        If Me.MuestraClientePorRecuperarDataGridView.RowCount = 0 Then
            MsgBox("Selecciona un registro para Consultaro.", MsgBoxStyle.Information)
            Exit Sub
        End If
        eOpcion = "C"
        eClavePorRecuperar = CLng(Me.MuestraClientePorRecuperarDataGridView.SelectedCells.Item(0).Value)
        eBndPorRecuperar = False
        FrmClientesPorRecuperar.Show()
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        If Me.MuestraClientePorRecuperarDataGridView.RowCount = 0 Then
            MsgBox("Selecciona un registro para Ejecutar.", MsgBoxStyle.Information)
            Exit Sub
        End If
        eOpcion = "E"
        eClavePorRecuperar = CLng(Me.MuestraClientePorRecuperarDataGridView.SelectedCells.Item(0).Value)
        eBndPorRecuperar = False
        FrmClientesPorRecuperar.Show()
    End Sub
End Class
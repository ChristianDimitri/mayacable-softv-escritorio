﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class BrwCambioDeTipoDeServicio
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.bnSalir = New System.Windows.Forms.Button()
        Me.bnNuevo = New System.Windows.Forms.Button()
        Me.CMBLabel3 = New System.Windows.Forms.Label()
        Me.dgvClientes = New System.Windows.Forms.DataGridView()
        Me.Id = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Contrato = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Cambio = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FechaSolicitud = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FechaEjecución = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.tbContrato = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.bnBuscarContrato = New System.Windows.Forms.Button()
        CType(Me.dgvClientes, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'bnSalir
        '
        Me.bnSalir.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnSalir.Location = New System.Drawing.Point(862, 682)
        Me.bnSalir.Name = "bnSalir"
        Me.bnSalir.Size = New System.Drawing.Size(136, 36)
        Me.bnSalir.TabIndex = 3
        Me.bnSalir.Text = "&SALIR"
        Me.bnSalir.UseVisualStyleBackColor = True
        '
        'bnNuevo
        '
        Me.bnNuevo.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnNuevo.Location = New System.Drawing.Point(862, 12)
        Me.bnNuevo.Name = "bnNuevo"
        Me.bnNuevo.Size = New System.Drawing.Size(136, 36)
        Me.bnNuevo.TabIndex = 2
        Me.bnNuevo.Text = "&NUEVO"
        Me.bnNuevo.UseVisualStyleBackColor = True
        '
        'CMBLabel3
        '
        Me.CMBLabel3.AutoSize = True
        Me.CMBLabel3.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel3.Location = New System.Drawing.Point(10, 12)
        Me.CMBLabel3.Name = "CMBLabel3"
        Me.CMBLabel3.Size = New System.Drawing.Size(162, 20)
        Me.CMBLabel3.TabIndex = 14
        Me.CMBLabel3.Text = "Buscar Cliente por:"
        '
        'dgvClientes
        '
        Me.dgvClientes.AllowUserToAddRows = False
        Me.dgvClientes.AllowUserToDeleteRows = False
        Me.dgvClientes.BackgroundColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvClientes.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvClientes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvClientes.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Id, Me.Contrato, Me.Cambio, Me.FechaSolicitud, Me.FechaEjecución})
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvClientes.DefaultCellStyle = DataGridViewCellStyle4
        Me.dgvClientes.Location = New System.Drawing.Point(225, 12)
        Me.dgvClientes.Name = "dgvClientes"
        Me.dgvClientes.ReadOnly = True
        Me.dgvClientes.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvClientes.Size = New System.Drawing.Size(621, 706)
        Me.dgvClientes.TabIndex = 11
        Me.dgvClientes.TabStop = False
        '
        'Id
        '
        Me.Id.DataPropertyName = "Id"
        Me.Id.HeaderText = "Id"
        Me.Id.Name = "Id"
        Me.Id.ReadOnly = True
        Me.Id.Visible = False
        '
        'Contrato
        '
        Me.Contrato.DataPropertyName = "Contrato"
        Me.Contrato.HeaderText = "Contrato"
        Me.Contrato.Name = "Contrato"
        Me.Contrato.ReadOnly = True
        '
        'Cambio
        '
        Me.Cambio.DataPropertyName = "Cambio"
        Me.Cambio.HeaderText = "Cambio"
        Me.Cambio.Name = "Cambio"
        Me.Cambio.ReadOnly = True
        Me.Cambio.Width = 150
        '
        'FechaSolicitud
        '
        Me.FechaSolicitud.DataPropertyName = "FechaSolicitud"
        DataGridViewCellStyle2.Format = "d"
        DataGridViewCellStyle2.NullValue = Nothing
        Me.FechaSolicitud.DefaultCellStyle = DataGridViewCellStyle2
        Me.FechaSolicitud.HeaderText = "Fecha Solicitud"
        Me.FechaSolicitud.Name = "FechaSolicitud"
        Me.FechaSolicitud.ReadOnly = True
        Me.FechaSolicitud.Width = 150
        '
        'FechaEjecución
        '
        Me.FechaEjecución.DataPropertyName = "FechaEjecución"
        DataGridViewCellStyle3.Format = "d"
        DataGridViewCellStyle3.NullValue = Nothing
        Me.FechaEjecución.DefaultCellStyle = DataGridViewCellStyle3
        Me.FechaEjecución.HeaderText = "Fecha Ejecución"
        Me.FechaEjecución.Name = "FechaEjecución"
        Me.FechaEjecución.ReadOnly = True
        Me.FechaEjecución.Width = 150
        '
        'tbContrato
        '
        Me.tbContrato.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbContrato.Location = New System.Drawing.Point(14, 70)
        Me.tbContrato.MaxLength = 10
        Me.tbContrato.Name = "tbContrato"
        Me.tbContrato.Size = New System.Drawing.Size(100, 21)
        Me.tbContrato.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(11, 52)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(61, 15)
        Me.Label1.TabIndex = 10
        Me.Label1.Text = "Contrato"
        '
        'bnBuscarContrato
        '
        Me.bnBuscarContrato.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnBuscarContrato.Location = New System.Drawing.Point(14, 97)
        Me.bnBuscarContrato.Name = "bnBuscarContrato"
        Me.bnBuscarContrato.Size = New System.Drawing.Size(75, 23)
        Me.bnBuscarContrato.TabIndex = 1
        Me.bnBuscarContrato.Text = "&Buscar"
        Me.bnBuscarContrato.UseVisualStyleBackColor = True
        '
        'BrwCambioDeTipoDeServicio
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1008, 730)
        Me.Controls.Add(Me.bnSalir)
        Me.Controls.Add(Me.bnNuevo)
        Me.Controls.Add(Me.CMBLabel3)
        Me.Controls.Add(Me.dgvClientes)
        Me.Controls.Add(Me.tbContrato)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.bnBuscarContrato)
        Me.Name = "BrwCambioDeTipoDeServicio"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Cambios de Tipo de Servicio"
        CType(Me.dgvClientes, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents bnSalir As System.Windows.Forms.Button
    Friend WithEvents bnNuevo As System.Windows.Forms.Button
    Friend WithEvents CMBLabel3 As System.Windows.Forms.Label
    Friend WithEvents dgvClientes As System.Windows.Forms.DataGridView
    Friend WithEvents Id As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Contrato As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Cambio As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FechaSolicitud As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FechaEjecución As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents tbContrato As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents bnBuscarContrato As System.Windows.Forms.Button
End Class

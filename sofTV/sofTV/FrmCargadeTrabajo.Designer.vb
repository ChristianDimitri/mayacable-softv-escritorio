﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmCargadeTrabajo
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim Carga_trab_insLabel As System.Windows.Forms.Label
        Dim CargaDeTrabajoLabel As System.Windows.Forms.Label
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.Carga_trab_insTextBox = New System.Windows.Forms.TextBox()
        Me.CargaDeTrabajoTextBox = New System.Windows.Forms.TextBox()
        Me.Button3 = New System.Windows.Forms.Button()
        Carga_trab_insLabel = New System.Windows.Forms.Label()
        CargaDeTrabajoLabel = New System.Windows.Forms.Label()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'Carga_trab_insLabel
        '
        Carga_trab_insLabel.AutoSize = True
        Carga_trab_insLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Carga_trab_insLabel.Location = New System.Drawing.Point(57, 50)
        Carga_trab_insLabel.Name = "Carga_trab_insLabel"
        Carga_trab_insLabel.Size = New System.Drawing.Size(85, 15)
        Carga_trab_insLabel.TabIndex = 2
        Carga_trab_insLabel.Text = "Instalación :"
        '
        'CargaDeTrabajoLabel
        '
        CargaDeTrabajoLabel.AutoSize = True
        CargaDeTrabajoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CargaDeTrabajoLabel.Location = New System.Drawing.Point(76, 23)
        CargaDeTrabajoLabel.Name = "CargaDeTrabajoLabel"
        CargaDeTrabajoLabel.Size = New System.Drawing.Size(66, 15)
        CargaDeTrabajoLabel.TabIndex = 0
        CargaDeTrabajoLabel.Text = "General :"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Carga_trab_insLabel)
        Me.GroupBox2.Controls.Add(Me.Carga_trab_insTextBox)
        Me.GroupBox2.Controls.Add(CargaDeTrabajoLabel)
        Me.GroupBox2.Controls.Add(Me.CargaDeTrabajoTextBox)
        Me.GroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(12, 12)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(302, 84)
        Me.GroupBox2.TabIndex = 14
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Carga de trabajo"
        '
        'Carga_trab_insTextBox
        '
        Me.Carga_trab_insTextBox.Location = New System.Drawing.Point(148, 47)
        Me.Carga_trab_insTextBox.MaxLength = 2
        Me.Carga_trab_insTextBox.Name = "Carga_trab_insTextBox"
        Me.Carga_trab_insTextBox.Size = New System.Drawing.Size(100, 21)
        Me.Carga_trab_insTextBox.TabIndex = 14
        '
        'CargaDeTrabajoTextBox
        '
        Me.CargaDeTrabajoTextBox.Location = New System.Drawing.Point(148, 20)
        Me.CargaDeTrabajoTextBox.MaxLength = 2
        Me.CargaDeTrabajoTextBox.Name = "CargaDeTrabajoTextBox"
        Me.CargaDeTrabajoTextBox.Size = New System.Drawing.Size(100, 21)
        Me.CargaDeTrabajoTextBox.TabIndex = 13
        '
        'Button3
        '
        Me.Button3.BackColor = System.Drawing.SystemColors.Control
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.ForeColor = System.Drawing.Color.Black
        Me.Button3.Location = New System.Drawing.Point(177, 102)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(136, 36)
        Me.Button3.TabIndex = 21
        Me.Button3.Text = "&GUARDAR"
        Me.Button3.UseVisualStyleBackColor = False
        '
        'FrmCargadeTrabajo
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(325, 148)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.GroupBox2)
        Me.Name = "FrmCargadeTrabajo"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Carga de Trabajo"
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Carga_trab_insTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CargaDeTrabajoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Button3 As System.Windows.Forms.Button
End Class

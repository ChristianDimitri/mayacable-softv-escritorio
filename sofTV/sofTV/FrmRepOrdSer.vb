﻿Public Class FrmRepOrdSer

    Private Sub bnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bnSalir.Click
        eOpVentas = 0
        Me.Close()
    End Sub

    Private Sub bnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bnAceptar.Click

        eMesIni = dtpFechaIni.Value.Month
        eAnioIni = dtpFechaIni.Value.Year
        eMesFin = dtpFechaFin.Value.Month
        eAnioFin = dtpFechaFin.Value.Year
        eTrabajo = ""
        eTitulo = ""

        If rbRAPAR.Checked = True Then
            eTrabajo = "RAPAR"
            eTitulo = rbRAPAR.Text
        End If

        If rbRETCA.Checked = True Then
            eTrabajo = "RETCA"
            eTitulo = rbRETCA.Text
        End If

        eOpVentas = 81
        FrmImprimirComision.Show()
    End Sub
End Class
Imports System.Data.SqlClient
Public Class FrmCiudades
    Private nomciudad As String = Nothing
    Private Sub damedatosbitacora()
        Try
            If opcion = "M" Then
                nomciudad = Me.NombreTextBox.Text
            End If
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub guardabitacora(ByVal op As Integer)
        Select Case op
            Case 0
                If opcion = "N" Then
                    bitsist(GloUsuario, 0, LocGloSistema, Me.Name, "Se guardo un nueva ciudad", "", "Se guardo un nueva ciudad: " + Me.NombreTextBox.Text, LocClv_Ciudad)
                ElseIf opcion = "M" Then
                    bitsist(GloUsuario, 0, LocGloSistema, Me.Name, Me.NombreTextBox.Name, nomciudad, Me.NombreTextBox.Text, LocClv_Ciudad)
                End If
            Case 1
                bitsist(GloUsuario, 0, LocGloSistema, Me.Name, "Se Elimino Una Ciudad", "", "Se Elimino Una Ciudad: " + Me.NombreTextBox.Text, LocClv_Ciudad)
        End Select
    End Sub

    Private Sub FrmCiudades_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        If opcion = "N" Then
            Me.CONCIUDADESBindingSource.AddNew()
            Panel1.Enabled = True
        ElseIf opcion = "C" Then
            Panel1.Enabled = False
            BUSCA(gloClave)
        ElseIf opcion = "M" Then
            Panel1.Enabled = True
            BUSCA(gloClave)
        End If
    End Sub



    Private Sub BUSCA(ByVal CLAVE As Integer)
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Try
            Me.CONCIUDADESTableAdapter.Connection = CON
            Me.CONCIUDADESTableAdapter.Fill(Me.NewSofTvDataSet.CONCIUDADES, New System.Nullable(Of Integer)(CType(CLAVE, Integer)))
            damedatosbitacora()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
        CON.Close()

    End Sub

    Private Sub Panel1_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles Panel1.Paint

    End Sub

    Private Sub CONCIUDADESBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CONCIUDADESBindingNavigatorSaveItem.Click
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.Validate()
        Me.CONCIUDADESBindingSource.EndEdit()
        Me.CONCIUDADESTableAdapter.Connection = CON
        Me.CONCIUDADESTableAdapter.Update(Me.NewSofTvDataSet.CONCIUDADES)
        guardabitacora(0)
        MsgBox("Se ha Guardado con Ex�to")
        GloBnd = True
        CON.Close()
        Me.Close()
    End Sub


    Private Sub Clv_CiudadTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Clv_CiudadTextBox.TextChanged
        gloClave = Me.Clv_CiudadTextBox.Text
    End Sub

    Private Sub NombreTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NombreTextBox.TextChanged

    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub ToolStripButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton1.Click
        Me.CONCIUDADESBindingSource.CancelEdit()
        GloBnd = True
        Me.Close()
    End Sub

    Private Sub BindingNavigatorDeleteItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BindingNavigatorDeleteItem.Click
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.CONCIUDADESTableAdapter.Connection = CON
        Me.CONCIUDADESTableAdapter.Delete(gloClave)
        guardabitacora(1)
        GloBnd = True
        Me.Close()
        CON.Close()
    End Sub
End Class
Imports System.Data.SqlClient
Public Class FrmReprocesamiento

    Private eClv_Periodo_Cobro As Long = 0
    Private eFactura As String = String.Empty
    Private eRes As Integer = 0
    Private eMsg As String = String.Empty

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub

    Private Sub FrmReprocesamiento_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        If Bnprocesa = True Then
            Bnprocesa = False
            Me.TextBox1.Text = GLOCONTRATOSEL
            GLOCONTRATOSEL = 0
        End If
    End Sub

    Private Sub FrmReprocesamiento_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        GLOCONTRATOSEL = 0
        Bnprocesa = True
        FrmSelCliente.Show()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If IsNumeric(Me.TextBox1.Text) = False Then
            MsgBox("Teclea un contrato v�lido.", MsgBoxStyle.Information)
            Limpiar()
            Exit Sub
        End If
        If CLng(Me.TextBox1.Text) = 0 Then
            MsgBox("Teclea un contrato v�lido.", MsgBoxStyle.Information)
            Limpiar()
            Exit Sub
        End If
        If IsNumeric(Me.TextBox3.Text) = False Then
            MsgBox("No se puede realizar la operaci�n.", MsgBoxStyle.Information)
            Limpiar()
            Exit Sub
        End If

        Procesa(CLng(Me.TextBox1.Text), CLng(Me.TextBox3.Text))


    End Sub

    Private Sub Procesa(ByVal Contrato As Long, ByVal Clv_Periodo_Cobro As Long)

        Dim con As New SqlConnection(MiConexion)
        Dim cmd As New SqlCommand("Procesa_Cobranza_Tel_Credito", con)
        cmd.CommandType = CommandType.StoredProcedure

        Dim prm As New SqlParameter( _
                  "@Contrato", SqlDbType.BigInt)
        prm.Direction = ParameterDirection.Input
        prm.Value = Contrato
        cmd.Parameters.Add(prm)

        prm = New SqlParameter( _
                  "@Clv_Periodo_Cobro", SqlDbType.BigInt)
        prm.Direction = ParameterDirection.Input
        prm.Value = Clv_Periodo_Cobro
        cmd.Parameters.Add(prm)

        Try
            con.Open()
            Dim i As Integer = cmd.ExecuteNonQuery()
            DameFacturaYPeriodoNuevo(CLng(Me.TextBox1.Text), 1)

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        con.Close()

    End Sub

    Private Sub DameFacturaYPeriodo(ByVal Contrato As Long, ByVal Op As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("DameFacturaYPeriodo", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Contrato", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Contrato
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@Op", SqlDbType.Int)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = Op
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@Clv_Periodo_Cobro", SqlDbType.BigInt)
        parametro3.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro3)

        Dim parametro4 As New SqlParameter("@Factura", SqlDbType.VarChar, 50)
        parametro4.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro4)

        Dim parametro5 As New SqlParameter("@Res", SqlDbType.Int)
        parametro5.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro5)

        Dim parametro6 As New SqlParameter("@Msg", SqlDbType.VarChar, 150)
        parametro6.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro6)

        Try

            conexion.Open()
            comando.ExecuteNonQuery()
            eClv_Periodo_Cobro = CLng(parametro3.Value.ToString)
            eFactura = parametro4.Value.ToString
            eRes = CInt(parametro5.Value.ToString)
            eMsg = parametro6.Value.ToString
            conexion.Close()

            Me.Label4.Text = eMsg

            If eRes = 1 Then
                Me.Button1.Enabled = False
                Limpiar()
            Else
                Me.Button1.Enabled = True
                Asigar()
            End If

        Catch ex As Exception
            conexion.Close()
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try

    End Sub

    Private Sub DameFacturaYPeriodoNuevo(ByVal Contrato As Long, ByVal Op As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("DameFacturaYPeriodo", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Contrato", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Contrato
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@Op", SqlDbType.Int)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = Op
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@Clv_Periodo_Cobro", SqlDbType.BigInt)
        parametro3.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro3)

        Dim parametro4 As New SqlParameter("@Factura", SqlDbType.VarChar, 50)
        parametro4.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro4)

        Dim parametro5 As New SqlParameter("@Res", SqlDbType.Int)
        parametro5.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro5)

        Dim parametro6 As New SqlParameter("@Msg", SqlDbType.VarChar, 150)
        parametro6.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro6)

        Try

            conexion.Open()
            comando.ExecuteNonQuery()
            eClv_Periodo_Cobro = CLng(parametro3.Value.ToString)
            eFactura = parametro4.Value.ToString
            eRes = CInt(parametro5.Value.ToString)
            eMsg = parametro6.Value.ToString
            conexion.Close()

            If eRes = 1 Then
                
                MsgBox(eMsg, MsgBoxStyle.Information)
            Else
                
                MsgBox(eMsg, MsgBoxStyle.Information)
                Limpiar2()
            End If





        Catch ex As Exception
            conexion.Close()
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub


    Private Sub TextBox1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox1.TextChanged
        If Me.TextBox1.Text.Length = 0 Then
            Limpiar2()
            Exit Sub
        End If
        If IsNumeric(Me.TextBox1.Text) = False Then
            MsgBox("Teclea un contrato v�lido.", MsgBoxStyle.Information)
            Limpiar2()
            Exit Sub
        End If
        If CLng(Me.TextBox1.Text) = 0 Then
            MsgBox("Teclea un contrato v�lido.", MsgBoxStyle.Information)
            Limpiar2()
            Exit Sub
        End If
        DameFacturaYPeriodo(CLng(Me.TextBox1.Text), 0)
    End Sub

    Private Sub Limpiar()
        Me.TextBox2.Clear()
        Me.TextBox3.Clear()
        eClv_Periodo_Cobro = 0
        eFactura = String.Empty
    End Sub

    Private Sub Limpiar2()
        Me.TextBox1.Clear()
        Me.TextBox2.Clear()
        Me.TextBox3.Clear()
        eClv_Periodo_Cobro = 0
        eFactura = String.Empty
        eRes = 0
        eMsg = String.Empty
        Me.Label4.Text = ""
    End Sub

    Private Sub Asigar()
        Me.TextBox2.Text = eFactura
        Me.TextBox3.Text = eClv_Periodo_Cobro
    End Sub
End Class
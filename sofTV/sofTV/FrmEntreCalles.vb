﻿Public Class FrmEntreCalles

    Private Sub MUESTRAEntreCalles(ByVal CONTRATO As Integer)
        Dim dTable As New DataTable
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@CONTRATO", SqlDbType.Int, CONTRATO)
        dTable = BaseII.ConsultaDT("MUESTRAEntreCalles")
        If dTable.Rows.Count = 0 Then
            Exit Sub
        End If
        tbEntreCalles.Text = dTable.Rows(0)(0).ToString
    End Sub

    Private Sub MODIFICAEntreCalles(ByVal CONTRATO As Integer, ByVal ENTRECALLES As String)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@CONTRATO", SqlDbType.Int, CONTRATO)
        BaseII.CreateMyParameter("@ENTRECALLES", SqlDbType.VarChar, ENTRECALLES, 250)
        BaseII.Inserta("MODIFICAEntreCalles")
    End Sub

    Private Sub tsbGuardar_Click(sender As System.Object, e As System.EventArgs) Handles tsbGuardar.Click
        If tbEntreCalles.Text.Length = 0 Then
            MessageBox.Show("Captura una entre calles.")
            Exit Sub
        End If
        MODIFICAEntreCalles(eContrato, tbEntreCalles.Text)
        MessageBox.Show("Se guardó con éxito.")
        Me.Close()
    End Sub

    Private Sub bnSalir_Click(sender As System.Object, e As System.EventArgs) Handles bnSalir.Click
        Me.Close()
    End Sub

    Private Sub FrmEntreCalles_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
        MUESTRAEntreCalles(eContrato)
    End Sub
End Class
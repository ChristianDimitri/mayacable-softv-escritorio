﻿Public Class FrmRepCobrosADomicilio

    Private Sub DAMEFECHADELSERVIDOR()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Fecha", ParameterDirection.Output, SqlDbType.DateTime)
        BaseII.ProcedimientoOutPut("DAMEFECHADELSERVIDOR")
        dtpFechaIni.Value = DateTime.Parse(BaseII.dicoPar("@Fecha").ToString())
        dtpFechaFin.Value = DateTime.Parse(BaseII.dicoPar("@Fecha").ToString())
    End Sub

    Private Sub dtpFechaIni_ValueChanged(sender As System.Object, e As System.EventArgs) Handles dtpFechaIni.ValueChanged
        dtpFechaFin.MinDate = dtpFechaIni.Value
    End Sub

    Private Sub dtpFechaFin_ValueChanged(sender As System.Object, e As System.EventArgs) Handles dtpFechaFin.ValueChanged
        dtpFechaIni.MaxDate = dtpFechaFin.Value
    End Sub

    Private Sub FrmRepCobrosADomicilio_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
        DAMEFECHADELSERVIDOR()
    End Sub

    Private Sub bnAceptar_Click(sender As System.Object, e As System.EventArgs) Handles bnAceptar.Click
        If rbCobros.Checked = True Then eOpVentas = 97
        If rbComisiones.Checked = True Then eOpVentas = 98
        eFechaIni = dtpFechaIni.Value
        eFechaFin = dtpFechaFin.Value
        FrmImprimirComision.Show()
    End Sub

    Private Sub bnSalir_Click(sender As System.Object, e As System.EventArgs) Handles bnSalir.Click
        Me.Close()
    End Sub
End Class
﻿Public Class FrmProblema

    Private Sub FrmProblema_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        llenaComboClasifProblemas()
    End Sub

#Region "Clasificación Problemas"
    Private Sub llenaComboClasifProblemas()
        Dim Problema As New ClassClasificacionProblemas
        Problema.clvProblema = 0
        Problema.Descripcion = String.Empty
        Problema.OpBusqueda = 4
        Me.cmbClasifProblemas.DataSource = Problema.uspConsultaTblClasificacionProblemas
    End Sub
#End Region

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        If Me.cmbClasifProblemas.SelectedValue = 0 Then
            MsgBox("Seleccione un Problema", MsgBoxStyle.Information)
            Exit Sub
        End If

        FrmQuejas.lblProblemaSeleccionado.Text = Me.cmbClasifProblemas.Text
        FrmQuejas.LocClvClasifProblema = Me.cmbClasifProblemas.SelectedValue
        Me.Close()
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Me.Close()
    End Sub
End Class
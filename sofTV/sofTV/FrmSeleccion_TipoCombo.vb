Imports System.Data.SqlClient
Public Class FrmSeleccion_TipoCombo

    Private Sub FrmSeleccion_TipoCombo_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        llenalisttmp(LocClv_session)
        recargalist(LocClv_session)
    End Sub
    Private Sub llenalisttmp(ByVal clv_session As Long)
        Dim con As New SqlClient.SqlConnection(MiConexion)
        Dim cmd1 As New SqlClient.SqlCommand()
        Try
            con.Open()
            cmd1 = New SqlClient.SqlCommand()
            With cmd1
                .CommandText = "Muestra_Seleccion_tipocombo_tmp_nuevo"
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure
                .Connection = con

                Dim prm As New SqlParameter("@clv_session", SqlDbType.BigInt)
                prm.Direction = ParameterDirection.Input
                prm.Value = clv_session
                .Parameters.Add(prm)

                Dim x As Integer = cmd1.ExecuteNonQuery()

            End With
            con.Close()

        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub insertauno(ByVal clv_session As Long, ByVal clv_descuento As Long, ByVal op As Integer)
        Dim con As New SqlClient.SqlConnection(MiConexion)
        Dim cmd1 As New SqlClient.SqlCommand()
        Select Case op
            Case 1
                Try
                    con.Open()
                    cmd1 = New SqlClient.SqlCommand()
                    With cmd1
                        .CommandText = "InsertaUno_Seleccion_tipocombo"
                        .CommandTimeout = 0
                        .CommandType = CommandType.StoredProcedure
                        .Connection = con

                        Dim prm As New SqlParameter("@clv_session", SqlDbType.BigInt)
                        prm.Direction = ParameterDirection.Input
                        prm.Value = clv_session
                        .Parameters.Add(prm)

                        Dim prm1 As New SqlParameter("@clv_descuento", SqlDbType.BigInt)
                        prm1.Direction = ParameterDirection.Input
                        prm1.Value = clv_descuento
                        .Parameters.Add(prm1)

                        Dim y As Integer = cmd1.ExecuteNonQuery()

                    End With
                    con.Close()
                Catch ex As Exception
                    System.Windows.Forms.MessageBox.Show(ex.Message)
                End Try
                'InsertaUno_Seleccion_tipocombo()
                'InsertaUno_Seleccion_tipocombo_tmp()
            Case 2
                Try
                    con.Open()
                    cmd1 = New SqlClient.SqlCommand()
                    With cmd1
                        .CommandText = "InsertaUno_Seleccion_tipocombo_tmp"
                        .CommandTimeout = 0
                        .CommandType = CommandType.StoredProcedure
                        .Connection = con

                        Dim prm As New SqlParameter("@clv_session", SqlDbType.BigInt)
                        prm.Direction = ParameterDirection.Input
                        prm.Value = clv_session
                        .Parameters.Add(prm)

                        Dim prm1 As New SqlParameter("@clv_descuento", SqlDbType.BigInt)
                        prm1.Direction = ParameterDirection.Input
                        prm1.Value = clv_descuento
                        .Parameters.Add(prm1)

                        Dim y As Integer = cmd1.ExecuteNonQuery()

                    End With
                    con.Close()
                Catch ex As Exception
                    System.Windows.Forms.MessageBox.Show(ex.Message)
                End Try
        End Select
    End Sub

    Private Sub insertatodo(ByVal clv_session As Long, ByVal op As Integer)
        Dim con As New SqlClient.SqlConnection(MiConexion)
        Dim cmd1 As New SqlClient.SqlCommand()
        Select Case op
            Case 1
                Try
                    con.Open()
                    cmd1 = New SqlClient.SqlCommand()
                    With cmd1
                        .CommandText = "InsertaTodo_Seleccion_tipocombo"
                        .CommandTimeout = 0
                        .CommandType = CommandType.StoredProcedure
                        .Connection = con

                        Dim prm As New SqlParameter("@clv_session", SqlDbType.BigInt)
                        prm.Direction = ParameterDirection.Input
                        prm.Value = clv_session
                        .Parameters.Add(prm)



                        Dim y As Integer = cmd1.ExecuteNonQuery()

                    End With
                    con.Close()
                Catch ex As Exception
                    System.Windows.Forms.MessageBox.Show(ex.Message)
                End Try
            Case 2
                Try
                    con.Open()
                    cmd1 = New SqlClient.SqlCommand()
                    With cmd1
                        .CommandText = "InsertaTodo_Seleccion_tipocombo_tmp"

                        .CommandTimeout = 0
                        .CommandType = CommandType.StoredProcedure
                        .Connection = con

                        Dim prm As New SqlParameter("@clv_session", SqlDbType.BigInt)
                        prm.Direction = ParameterDirection.Input
                        prm.Value = clv_session
                        .Parameters.Add(prm)


                        Dim y As Integer = cmd1.ExecuteNonQuery()
                    End With
                    con.Close()
                Catch ex As Exception
                    System.Windows.Forms.MessageBox.Show(ex.Message)
                End Try
        End Select
    End Sub


    Private Sub recargalist(ByVal clv_session As Long)
        Dim con As New SqlClient.SqlConnection(MiConexion)
        Try
            con.Open()
            Me.MuestraSeleccion_tipocombo_tmp_consultaTableAdapter.Connection = con
            Me.MuestraSeleccion_tipocombo_tmp_consultaTableAdapter.Fill(Me.Procedimientosarnoldo4.MuestraSeleccion_tipocombo_tmp_consulta, clv_session)

            Me.MuestraSeleccion_tipocombo_consultaTableAdapter.Connection = con
            Me.MuestraSeleccion_tipocombo_consultaTableAdapter.Fill(Me.Procedimientosarnoldo4.MuestraSeleccion_tipocombo_consulta, clv_session)
            con.Close()
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

        'Dim cmd As New SqlClient.SqlCommand()


        'Try
        '    con.Open()
        '    cmd = New SqlClient.SqlCommand()
        '    With cmd
        '        .CommandText = "MuestraSeleccion_tipocombo_tmp_consulta"
        '        .CommandTimeout = 0
        '        .CommandType = CommandType.StoredProcedure
        '        .Connection = con

        '        Dim prm As New SqlParameter("@clv_Session", SqlDbType.BigInt)
        '        prm.Direction = ParameterDirection.Input
        '        prm.Value = clv_session
        '        .Parameters.Add(prm)

        '        Dim i As Integer = cmd.ExecuteNonQuery()
        '    End With
        '    con.Close()

        '    con.Open()
        '    cmd = New SqlClient.SqlCommand()
        '    With cmd
        '        .CommandText = "MuestraSeleccion_tipocombo_consulta"
        '        .CommandTimeout = 0
        '        .CommandType = CommandType.StoredProcedure
        '        .Connection = con

        '        Dim prm As New SqlParameter("@clv_Session", SqlDbType.BigInt)
        '        prm.Direction = ParameterDirection.Input
        '        prm.Value = clv_session
        '        .Parameters.Add(prm)

        '        Dim i As Integer = cmd.ExecuteNonQuery()
        '    End With
        '    con.Close()

        'Catch ex As Exception
        '    System.Windows.Forms.MessageBox.Show(ex.Message)
        'End Try
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Me.insertauno(LocClv_session, CLng(Me.ListBox1.SelectedValue), 1)
        recargalist(LocClv_session)
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Me.insertauno(LocClv_session, CLng(Me.ListBox2.SelectedValue), 2)
        recargalist(LocClv_session)
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.insertatodo(LocClv_session, 1)
        recargalist(LocClv_session)
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Me.insertatodo(LocClv_session, 2)
        recargalist(LocClv_session)
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        If (Me.ListBox2.Items.Count()) > 0 Then
            FrmSelCiudad.Show()
        Else
            MsgBox("Selecciona Al Menos Un Combo Por Favor", MsgBoxStyle.Information)
        End If
        Me.Close()
    End Sub
End Class
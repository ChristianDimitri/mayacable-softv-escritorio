﻿Imports sofTV.BAL
Public Class BrwMTAsAsignacionNumTel

    Private Sub btnBucar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBucar.Click
        If (Buscar()) Then
            ValidaMTAs()
        End If
    End Sub

    Private Sub btnModificar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModificar.Click
        Dim frmMTAsAsignacion As New FrmMTAsAsignacionNumTel()
        frmMTAsAsignacion._idAparato = Long.Parse(dgvMTAs.CurrentRow.Cells(0).Value.ToString())
        frmMTAsAsignacion._MAC = dgvMTAs.CurrentRow.Cells(1).Value.ToString()
        frmMTAsAsignacion._MACMTA = dgvMTAs.CurrentRow.Cells(2).Value.ToString()
        frmMTAsAsignacion.ShowDialog()
    End Sub
    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub
    Private Sub BrwMTAsAsignacionNumTel_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        dgvMTAs.DataSource = RelMTATelefono.GetMTAsDisponibles()
        ValidaMTAs()
    End Sub
    Public Sub ValidaMTAs()
        Try
            If (dgvMTAs.RowCount <= 0) Then
                btnModificar.Enabled = False
            Else
                btnModificar.Enabled = True
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub
    Public Function Buscar() As Boolean
        Dim encontrados = False, idTelefono As Integer = 0, MAC As String = "", MACMTA As String = ""
        Dim dt As New DataTable()

        If IsNumeric(txtIDAparato.Text) And txtIDAparato.TextLength > 0 Then
            idTelefono = txtIDAparato.Text
        End If
        If txtMAC.TextLength > 0 Then
            MAC = txtMAC.Text
        End If
        If txtMACMTA.TextLength > 0 Then
            MACMTA = txtMACMTA.Text
        End If

        Try
            dt = RelMTATelefono.GetMTAsDisponiblesByFiltros(idTelefono, MAC, MACMTA)
            dgvMTAs.DataSource = dt
            If dt.Rows.Count > 0 Then
                encontrados = True
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try

        Return encontrados
    End Function
End Class
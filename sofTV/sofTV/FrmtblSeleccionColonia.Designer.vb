﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmtblSeleccionColonia
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle21 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle22 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle23 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle24 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.bnSalir = New System.Windows.Forms.Button()
        Me.bnAceptar = New System.Windows.Forms.Button()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.dgvDer = New System.Windows.Forms.DataGridView()
        Me.clv_coloniaDER = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NombreDER = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgvIzq = New System.Windows.Forms.DataGridView()
        Me.clv_coloniaIZQ = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NombreIZQ = New System.Windows.Forms.DataGridViewTextBoxColumn()
        CType(Me.dgvDer, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvIzq, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(431, 29)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(159, 15)
        Me.Label2.TabIndex = 19
        Me.Label2.Text = "Colonias seleccionadas"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(52, 29)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(164, 15)
        Me.Label1.TabIndex = 18
        Me.Label1.Text = "Colonias sin seleccionar"
        '
        'bnSalir
        '
        Me.bnSalir.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnSalir.Location = New System.Drawing.Point(626, 398)
        Me.bnSalir.Name = "bnSalir"
        Me.bnSalir.Size = New System.Drawing.Size(136, 36)
        Me.bnSalir.TabIndex = 17
        Me.bnSalir.Text = "&SALIR"
        Me.bnSalir.UseVisualStyleBackColor = True
        '
        'bnAceptar
        '
        Me.bnAceptar.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnAceptar.Location = New System.Drawing.Point(484, 398)
        Me.bnAceptar.Name = "bnAceptar"
        Me.bnAceptar.Size = New System.Drawing.Size(136, 36)
        Me.bnAceptar.TabIndex = 16
        Me.bnAceptar.Text = "&ACEPTAR"
        Me.bnAceptar.UseVisualStyleBackColor = True
        '
        'Button4
        '
        Me.Button4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button4.Location = New System.Drawing.Point(344, 277)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(75, 23)
        Me.Button4.TabIndex = 15
        Me.Button4.Text = "<<"
        Me.Button4.UseVisualStyleBackColor = True
        '
        'Button3
        '
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.Location = New System.Drawing.Point(344, 248)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(75, 23)
        Me.Button3.TabIndex = 14
        Me.Button3.Text = "<"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.Location = New System.Drawing.Point(344, 149)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(75, 23)
        Me.Button2.TabIndex = 13
        Me.Button2.Text = ">>"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Button1
        '
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(344, 120)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 23)
        Me.Button1.TabIndex = 12
        Me.Button1.Text = ">"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'dgvDer
        '
        Me.dgvDer.AllowUserToAddRows = False
        Me.dgvDer.AllowUserToDeleteRows = False
        Me.dgvDer.BackgroundColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle21.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle21.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle21.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle21.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle21.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle21.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle21.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvDer.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle21
        Me.dgvDer.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvDer.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.clv_coloniaDER, Me.NombreDER})
        DataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle22.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle22.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle22.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle22.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle22.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle22.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvDer.DefaultCellStyle = DataGridViewCellStyle22
        Me.dgvDer.Location = New System.Drawing.Point(434, 47)
        Me.dgvDer.Name = "dgvDer"
        Me.dgvDer.ReadOnly = True
        Me.dgvDer.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvDer.Size = New System.Drawing.Size(273, 313)
        Me.dgvDer.TabIndex = 11
        '
        'clv_coloniaDER
        '
        Me.clv_coloniaDER.DataPropertyName = "clv_colonia"
        Me.clv_coloniaDER.HeaderText = "clv_colonia"
        Me.clv_coloniaDER.Name = "clv_coloniaDER"
        Me.clv_coloniaDER.ReadOnly = True
        Me.clv_coloniaDER.Visible = False
        '
        'NombreDER
        '
        Me.NombreDER.DataPropertyName = "Nombre"
        Me.NombreDER.HeaderText = "Nombre"
        Me.NombreDER.Name = "NombreDER"
        Me.NombreDER.ReadOnly = True
        Me.NombreDER.Width = 200
        '
        'dgvIzq
        '
        Me.dgvIzq.AllowUserToAddRows = False
        Me.dgvIzq.AllowUserToDeleteRows = False
        Me.dgvIzq.BackgroundColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle23.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle23.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle23.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle23.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle23.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle23.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle23.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvIzq.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle23
        Me.dgvIzq.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvIzq.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.clv_coloniaIZQ, Me.NombreIZQ})
        DataGridViewCellStyle24.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle24.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle24.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle24.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle24.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle24.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle24.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvIzq.DefaultCellStyle = DataGridViewCellStyle24
        Me.dgvIzq.Location = New System.Drawing.Point(55, 47)
        Me.dgvIzq.Name = "dgvIzq"
        Me.dgvIzq.ReadOnly = True
        Me.dgvIzq.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvIzq.Size = New System.Drawing.Size(273, 313)
        Me.dgvIzq.TabIndex = 10
        '
        'clv_coloniaIZQ
        '
        Me.clv_coloniaIZQ.DataPropertyName = "clv_colonia"
        Me.clv_coloniaIZQ.HeaderText = "clv_colonia"
        Me.clv_coloniaIZQ.Name = "clv_coloniaIZQ"
        Me.clv_coloniaIZQ.ReadOnly = True
        Me.clv_coloniaIZQ.Visible = False
        '
        'NombreIZQ
        '
        Me.NombreIZQ.DataPropertyName = "Nombre"
        Me.NombreIZQ.HeaderText = "Nombre"
        Me.NombreIZQ.Name = "NombreIZQ"
        Me.NombreIZQ.ReadOnly = True
        Me.NombreIZQ.Width = 200
        '
        'FrmtblSeleccionColonia
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(774, 446)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.bnSalir)
        Me.Controls.Add(Me.bnAceptar)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.dgvDer)
        Me.Controls.Add(Me.dgvIzq)
        Me.Name = "FrmtblSeleccionColonia"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Selecciona Colonia"
        CType(Me.dgvDer, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvIzq, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents bnSalir As System.Windows.Forms.Button
    Friend WithEvents bnAceptar As System.Windows.Forms.Button
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents dgvDer As System.Windows.Forms.DataGridView
    Friend WithEvents dgvIzq As System.Windows.Forms.DataGridView
    Friend WithEvents clv_coloniaDER As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NombreDER As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents clv_coloniaIZQ As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NombreIZQ As System.Windows.Forms.DataGridViewTextBoxColumn
End Class

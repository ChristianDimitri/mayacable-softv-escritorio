Imports System.Data.Sql
Imports System.Data.SqlClient

Public Class FrmRelClienteInfoProspectos

    Private actualiza As Boolean

    Private Sub FrmRelClienteInfo_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.llenaComboPrefijos()
    End Sub

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        Dim contrato As Int64 = Convert.ToInt64(Me.lblContrato.Text.ToString)
        Dim fecha As DateTime = Convert.ToDateTime(Me.dtpFechaNac.Value.ToShortDateString)
        Dim clv As Integer = Convert.ToInt32(Me.cbPrefijos.SelectedValue.ToString)
        Dim nombre As String = Me.lbl_Nombre.Text
        If actualiza = True Then
            Me.guarda(contrato, fecha, clv, 2, nombre)
        Else
            Me.guarda(contrato, fecha, clv, 1, nombre)
            actualiza = True
        End If
        MsgBox("Se han guardado correctamente los cambios", MsgBoxStyle.Information)
        Me.Close()
    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub llenaComboPrefijos()
        Dim conexion As New SqlConnection(MiConexion)
        Dim tabla As DataTable = New DataTable
        Dim com As SqlCommand = New SqlCommand
        Dim da As SqlDataAdapter = New SqlDataAdapter(com)
        Dim bs As BindingSource = New BindingSource
        Try
            com.CommandType = CommandType.Text
            com.CommandTimeout = 0
            com.CommandText = "Select * from Prefijos"
            com.Connection = conexion
            com.Connection.Open()
            da.Fill(tabla)
            bs.DataSource = tabla
            Me.cbPrefijos.DataSource = bs
            Me.cbPrefijos.DisplayMember = "Prefijo"
            Me.cbPrefijos.ValueMember = "Clv_Prefijo"
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        Finally
            com.Connection.Close()
            com.Dispose()
            tabla.Dispose()
        End Try
    End Sub

    Public Sub inicia(ByVal contrato As String)
        Dim checa As Boolean = Me.ChecaSiExiste(contrato)
        If checa = True Then
            If MsgBox("El Cliente ya tiene informaci�n capturada, �Desea Actualizarla?", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
                actualiza = True
            Else
                Me.Close()
                Return
            End If
        Else
            actualiza = False
            Dim conexion As New SqlConnection(MiConexionProspectos)
            Dim tabla As DataTable = New DataTable
            Dim com As SqlCommand = New SqlCommand
            Dim da As SqlDataAdapter = New SqlDataAdapter(com)
            Try
                com.CommandType = CommandType.Text
                com.CommandTimeout = 0
                com.CommandText = "Select Nombre From Clientes Where Contrato = " + contrato
                com.Connection = conexion
                com.Connection.Open()
                da.Fill(tabla)
                If tabla.Rows.Count = 1 Then
                    Me.lbl_Nombre.Text = tabla.Rows(0)(0).ToString
                    Me.lblContrato.Text = contrato
                Else
                    MsgBox("No se encontro el numero de contrato", MsgBoxStyle.Exclamation)
                    Me.btnAceptar.Enabled = False
                    Return
                End If
            Catch ex As Exception
                MsgBox(ex.Message, MsgBoxStyle.Critical)
            Finally
                com.Connection.Close()
                com.Dispose()
                tabla.Dispose()
            End Try
        End If
    End Sub

    Private Sub guarda(ByVal contrato As Int64, ByVal fecha As DateTime, ByVal clv As Integer, ByVal op As Integer, ByVal nombre As String)
        Dim con As SqlConnection = New SqlConnection(MiConexionProspectos)
        Dim com As SqlCommand = New SqlCommand("LlenaRelClieInfo", con)
        Try
            com.CommandType = CommandType.StoredProcedure
            com.Connection.Open()
            com.Parameters.Add(New SqlParameter("@Contrato", contrato))
            com.Parameters.Add(New SqlParameter("@Fecha_Nacimiento", fecha))
            com.Parameters.Add(New SqlParameter("@Clv_Prefijo", clv))
            com.Parameters.Add(New SqlParameter("@Opcion", op))
            com.Parameters.Add(New SqlParameter("@Nombre", nombre))
            com.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        Finally
            com.Connection.Close()
            com.Dispose()
        End Try
    End Sub

    Private Function ChecaSiExiste(ByVal contrato As String) As Boolean
        Dim conexion As New SqlConnection(MiConexionProspectos)
        Dim tabla As DataTable = New DataTable
        Dim com As SqlCommand = New SqlCommand
        Dim da As SqlDataAdapter = New SqlDataAdapter(com)
        Dim valor As Boolean
        Try
            com.CommandType = CommandType.Text
            com.CommandTimeout = 0
            com.CommandText = "Exec ConsultaInfoCliente " + contrato
            com.Connection = conexion
            com.Connection.Open()
            da.Fill(tabla)
            If tabla.Rows.Count = 1 Then
                valor = True
            Else
                valor = False
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        Finally
            com.Connection.Close()
            com.Dispose()
            tabla.Dispose()
        End Try
        If valor = True Then
            Me.lblContrato.Text = tabla.Rows(0)(0).ToString
            Me.lbl_Nombre.Text = tabla.Rows(0)(1).ToString
            Me.dtpFechaNac.Value = Convert.ToDateTime(tabla.Rows(0)(2).ToString)
            Me.cbPrefijos.SelectedValue = Convert.ToInt32(tabla.Rows(0)(3).ToString)
            Return valor
        Else
            Return valor
        End If
    End Function

End Class
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports System.Data.SqlClient
Imports System.Text
Imports System.Collections.Generic

Public Class FrmImprimirContrato_TvInternet
    Private customersByCityReport As ReportDocument

    Private Sub ConfigureCrystalReports()

        customersByCityReport = New ReportDocument

        Dim connectionInfo As New ConnectionInfo
        'Dim eFecha As String = Nothing
        'eFecha = "Del " & eFechaIniPPE & " al " & eFechaFinPPE

        connectionInfo.ServerName = GloServerName
        connectionInfo.DatabaseName = GloDatabaseName
        connectionInfo.UserID = GloUserID
        connectionInfo.Password = GloPassword
        Dim reportPath As String = Nothing


        If ServicioContrato = 1 Then
            reportPath = RutaReportes + "\ReporteContratoTV_MayaEscritorio.rpt"
            Dim DS As New DataSet
            DS.Clear()

            Dim listatablas As New List(Of String)
            listatablas.Add("Cliente")
            listatablas.Add("DatosFiscales")
            listatablas.Add("Servicios")
            listatablas.Add("SeImprimeContratgo")

            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@CONTRATO", SqlDbType.BigInt, Contrato)
            DS = BaseII.ConsultaDS("REPORTEContratoTV", listatablas)

            If DS.Tables("SeImprimeContratgo").Rows(0)(0).ToString = "0" Then
                MessageBox.Show("El contrato no cuenta con servicio de TV.", "Contrato ::..", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Exit Sub
            End If

            customersByCityReport.Load(reportPath)
            SetDBReport(DS, customersByCityReport)

        Else

            reportPath = RutaReportes + "\ReporteContratoNET_MayaEscritorio.rpt"
            Dim DS As New DataSet
            DS.Clear()

            Dim listatablas As New List(Of String)
            listatablas.Add("Cliente")
            listatablas.Add("DatosFiscales")
            listatablas.Add("Servicios")
            listatablas.Add("SeImprimeContratgo")

            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@CONTRATO", SqlDbType.BigInt, Contrato)
            DS = BaseII.ConsultaDS("REPORTEContratoNET", listatablas)

            If DS.Tables("SeImprimeContratgo").Rows(0)(0).ToString = "0" Then
                MessageBox.Show("El contrato no cuenta con servicio de Internet.", "Contrato ::..", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Exit Sub
            End If

            customersByCityReport.Load(reportPath)
            SetDBReport(DS, customersByCityReport)

        End If

      

        'customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & LocGloNomEmpresa & "'"
        'customersByCityReport.DataDefinition.FormulaFields("Fecha").Text = "'" & eFecha & "'"
        'customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudad & "'"
        '  End If

        Me.CrystalReportViewer1.ReportSource = customersByCityReport
        customersByCityReport = Nothing
    End Sub

  

    Private Sub FrmImprimirContrato_TvInternet_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ConfigureCrystalReports()
    End Sub
End Class
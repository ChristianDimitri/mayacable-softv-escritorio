Imports System.Data.SqlClient
Public Class FrmDescargaMaterialTec

    Dim oMsj As String = Nothing


    Private Sub FrmDescargaMaterialTec_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        Dim CON7 As New SqlConnection(MiConexion)
        CON7.Open()

        Me.DimeSiTieneunaBitacoraTableAdapter.Connection = CON7
        Me.DimeSiTieneunaBitacoraTableAdapter.Fill(Me.DataSetEdgarRev2.DimeSiTieneunaBitacora, gloClave)
        Me.Muestra_Detalle_BitacoraTableAdapter.Connection = CON7
        Me.Muestra_Detalle_BitacoraTableAdapter.Fill(Me.DataSetarnoldo.Muestra_Detalle_Bitacora, Locclv_tec)
        If IsNumeric(Locclv_tec) = True And IsNumeric(ComboBox1.SelectedValue) Then
            Me.Muestra_Descripcion_ArticuloTableAdapter.Connection = CON7
            Me.Muestra_Descripcion_ArticuloTableAdapter.Fill(Me.DataSetarnoldo.Muestra_Descripcion_Articulo, Locclv_tec, Me.ComboBox1.SelectedValue)
        End If
        CON7.Close()
        llena_Detalle()
        

        Me.OK.Visible = True

     
        If opcion <> "C" Then
            Me.Button4.Visible = False
            Me.TextBox1.Enabled = True
            Me.ComboBox1.Enabled = True
            Me.ComboBox2.Enabled = True
            Me.ComboBox3.Enabled = True
            If gLOVERgUARDA = 1 Then
                Me.OK.Visible = False
                Me.Button4.Visible = True
            End If
        End If
        If opcion = "C" Then
            Me.OK.Visible = False
            Me.Button4.Visible = False
            Me.Button1.Enabled = False
            Me.Button3.Enabled = False
            Me.TextBox1.Enabled = False
            Me.ComboBox1.Enabled = False
            Me.ComboBox2.Enabled = False
            Me.ComboBox3.Enabled = False
        End If

        ConRelOrdSerMetraje(gloClave)
        If Me.ComboBox1.Text = "Instalaciones" Then
            Me.LabelMetrajeIni.Visible = True
            Me.TextBoxMetrajeIni.Visible = True
            Me.LabelMetrajeFin.Visible = True
            Me.TextBoxMetrajeFin.Visible = True
        Else
            Me.LabelMetrajeIni.Visible = False
            Me.TextBoxMetrajeIni.Visible = False
            Me.LabelMetrajeFin.Visible = False
            Me.TextBoxMetrajeFin.Visible = False
        End If

        If ValidarBitacoraCancelada(gloClave, clv_sessionTecnico, Me.TextBox2.Text, "O") = 1 Then
            Me.OK.Visible = False
            Me.Button4.Visible = False
        End If

    End Sub

    Private Sub ComboBox1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ComboBox1.Click

    End Sub

    Private Sub ComboBox1_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles ComboBox1.LostFocus
        If Me.ComboBox1.SelectedIndex = -1 Then
            Clv_Tipo.Text = 0
        End If
    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox1.SelectedIndexChanged
        If IsNumeric(ComboBox1.SelectedValue) = True Then
            Clv_Tipo.Text = Me.ComboBox1.SelectedValue
            If Me.ComboBox1.Text = "Instalaciones" Then
                Me.LabelMetrajeIni.Visible = True
                Me.TextBoxMetrajeIni.Visible = True
                Me.LabelMetrajeFin.Visible = True
                Me.TextBoxMetrajeFin.Visible = True
            Else
                Me.LabelMetrajeIni.Visible = False
                Me.TextBoxMetrajeIni.Visible = False
                Me.LabelMetrajeFin.Visible = False
                Me.TextBoxMetrajeFin.Visible = False
            End If
        End If
    End Sub

    Private Sub ComboBox3_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles ComboBox3.LostFocus
        If Me.ComboBox3.SelectedIndex = -1 Then
            Me.ComboBox2.ResetText()
        End If
    End Sub

    Private Sub ComboBox3_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox3.SelectedIndexChanged
        Me.ComboBox2.SelectedIndex = Me.ComboBox3.SelectedIndex
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click

        Dim CON As New SqlConnection(MiConexion)

        Dim num As Integer
        Dim RESP As String
        CON.Open()
        Me.Revisa_Tempo_OrdTableAdapter.Connection = CON
        Me.Revisa_Tempo_OrdTableAdapter.Fill(Me.DataSetLidia.Revisa_Tempo_Ord, clv_sessionTecnico, num)
        CON.Close()
        'CUANDO EL BOTON ACEPTAR ES VISIBLE=================================================
        If num > 0 And Me.Button4.Visible = True Then
            RESP = MsgBox("No se han Guardado los Datos Desea Guardar", MsgBoxStyle.YesNo).ToString
            If RESP = "Yes" Then
                'boton guardar
                CON.Open()
                Me.Quita_SEBORROART_SALTECTableAdapter.Connection = CON
                Me.Quita_SEBORROART_SALTECTableAdapter.Fill(Me.DataSetEdgarRev2.Quita_SEBORROART_SALTEC, clv_sessionTecnico)
                Me.Dame_FolioTableAdapter.Connection = CON
                Me.Dame_FolioTableAdapter.Fill(Me.DataSetarnoldo.Dame_Folio, gloClave, 1, Locclv_folio)
                Me.Inserta_Bitacora_tecTableAdapter.Connection = CON
                Me.Inserta_Bitacora_tecTableAdapter.Fill(Me.DataSetarnoldo.Inserta_Bitacora_tec, clv_sessionTecnico, gloClave, Locclv_folio, 1, Locclv_tec, GloUsuario, "P", "", LocNo_Bitacora)
                Me.Inserta_Rel_Bitacora_OrdenTableAdapter.Connection = CON
                Me.Inserta_Rel_Bitacora_OrdenTableAdapter.Fill(Me.DataSetarnoldo.Inserta_Rel_Bitacora_Orden, LocNo_Bitacora, CInt(gloClave))
                CON.Close()
                MsgBox("Se Guardo con �xito", MsgBoxStyle.Information)
                If Me.ConRel_Session_TecnicosDataGridView.RowCount > 0 Then
                    eActTecnico = False
                Else
                    eActTecnico = True
                End If
            ElseIf RESP = "No" Then
                CON.Open()
                Me.Borra_Inserta_TempoTableAdapter.Connection = CON
                Me.Borra_Inserta_TempoTableAdapter.Fill(Me.DataSetarnoldo.Borra_Inserta_Tempo, clv_sessionTecnico)
                CON.Close()
                'Eric lo hizo, sirve para habilitar o deshabilitar el combo e Vendedores; Lleno de nuevo el grid por
                'Puede que hayan agregado Art pero no quieren salvar.
                llena_Detalle()
                If Me.ConRel_Session_TecnicosDataGridView.RowCount > 0 Then
                    eActTecnico = False
                Else
                    eActTecnico = True
                End If
            End If
        End If
        'CUANDO EL BOTON GUARDAR ES VISIBLE==================================================
        If num > 0 And Me.OK.Visible = True Then
            RESP = MsgBox("No se han Guardado los Datos Desea Guardar", MsgBoxStyle.YesNo).ToString()
            If RESP = "Yes" Then
                'boton Aceptar
                LocValida1 = True
                If Me.ConRel_Session_TecnicosDataGridView.RowCount > 0 Then
                    eActTecnico = False
                Else
                    eActTecnico = True
                End If
            ElseIf RESP = "No" Then
                CON.Open()
                Me.Borra_Inserta_TempoTableAdapter.Connection = CON
                Me.Borra_Inserta_TempoTableAdapter.Fill(Me.DataSetarnoldo.Borra_Inserta_Tempo, clv_sessionTecnico)
                CON.Close()
                'Eric lo hizo, sirve para habilitar o deshabilitar el combo e Vendedores; Lleno de nuevo el grid por
                'Puede que hayan agregado Art pero no quieren salvar.
                llena_Detalle()
                If Me.ConRel_Session_TecnicosDataGridView.RowCount > 0 Then
                    eActTecnico = False
                Else
                    eActTecnico = True
                End If
            End If
        End If

        ' Me.Borra_Inserta_TempoTableAdapter.Fill(Me.DataSetarnoldo.Borra_Inserta_Tempo, clv_sessionTecnico)
        Me.Close()

    End Sub

    Public Function ValidarDuplicados_Y_Cancelados_Borrrar(ByVal oclv_Orden As Long, ByVal oClv_Session As Long, ByVal oNoArticulo As Integer, ByVal oTIPO As String) As Integer
        '@Clv_Orden bigint,@clv_session as bigint,@NoArticulo int,@Tipo varchar(2),@bnd int output,@Msj varchar(250) output
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("ValidaEliminarTecnicos_New ", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Clv_Orden", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = oclv_Orden
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@clv_session", SqlDbType.BigInt)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = oClv_Session
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@NoArticulo", SqlDbType.Int)
        parametro3.Direction = ParameterDirection.Input
        parametro3.Value = oNoArticulo
        comando.Parameters.Add(parametro3)

        Dim parametro4 As New SqlParameter("@Tipo", SqlDbType.VarChar, 2)
        parametro4.Direction = ParameterDirection.Input
        parametro4.Value = "O"
        comando.Parameters.Add(parametro4)

        Dim parametro5 As New SqlParameter("@bnd", SqlDbType.Int)
        parametro5.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro5)

        Dim parametro6 As New SqlParameter("@Msj", SqlDbType.VarChar, 250)
        parametro6.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro6)

        Try
            ValidarDuplicados_Y_Cancelados_Borrrar = 0
            conexion.Open()
            comando.ExecuteNonQuery()
            If parametro5.Value = 1 Then
                ValidarDuplicados_Y_Cancelados_Borrrar = 1
                oMsj = parametro6.Value
                MsgBox(oMsj)
            End If

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Function

    Public Function ValidarDuplicados_Y_Cancelados(ByVal oclv_Orden As Long, ByVal oClv_Session As Long, ByVal oNoArticulo As Integer, ByVal oTIPO As String) As Integer
        '@Clv_Orden bigint,@clv_session as bigint,@NoArticulo int,@Tipo varchar(2),@bnd int output,@Msj varchar(250) output
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("ValidaDuplicadoRel_Session_Tecnicos_New ", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Clv_Orden", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = oclv_Orden
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@clv_session", SqlDbType.BigInt)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = oClv_Session
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@NoArticulo", SqlDbType.Int)
        parametro3.Direction = ParameterDirection.Input
        parametro3.Value = oNoArticulo
        comando.Parameters.Add(parametro3)

        Dim parametro4 As New SqlParameter("@Tipo", SqlDbType.VarChar, 2)
        parametro4.Direction = ParameterDirection.Input
        parametro4.Value = "O"
        comando.Parameters.Add(parametro4)

        Dim parametro5 As New SqlParameter("@bnd", SqlDbType.Int)
        parametro5.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro5)

        Dim parametro6 As New SqlParameter("@Msj", SqlDbType.VarChar, 250)
        parametro6.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro6)

        Try
            ValidarDuplicados_Y_Cancelados = 0
            conexion.Open()
            comando.ExecuteNonQuery()
            If parametro5.Value = 1 Then
                ValidarDuplicados_Y_Cancelados = 1
                oMsj = parametro6.Value
                MsgBox(oMsj)
            End If

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Function

    Public Function ValidarBitacoraCancelada(ByVal oclv_Orden As Long, ByVal oClv_Session As Long, ByVal oNoArticulo As Integer, ByVal oTIPO As String) As Integer
        '@Clv_Orden bigint,@clv_session as bigint,@NoArticulo int,@Tipo varchar(2),@bnd int output,@Msj varchar(250) output
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("ValidaEliminarTecnicos_New ", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Clv_Orden", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = oclv_Orden
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@clv_session", SqlDbType.BigInt)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = oClv_Session
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@NoArticulo", SqlDbType.Int)
        parametro3.Direction = ParameterDirection.Input
        parametro3.Value = oNoArticulo
        comando.Parameters.Add(parametro3)

        Dim parametro4 As New SqlParameter("@Tipo", SqlDbType.VarChar, 2)
        parametro4.Direction = ParameterDirection.Input
        parametro4.Value = "O"
        comando.Parameters.Add(parametro4)

        Dim parametro5 As New SqlParameter("@bnd", SqlDbType.Int)
        parametro5.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro5)

        Dim parametro6 As New SqlParameter("@Msj", SqlDbType.VarChar, 250)
        parametro6.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro6)

        Try
            ValidarBitacoraCancelada = 0
            conexion.Open()
            comando.ExecuteNonQuery()
            If parametro5.Value = 1 Then
                ValidarBitacoraCancelada = 1
                oMsj = parametro6.Value
            End If

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Function

    Private Sub AGREGAR()

        Dim CON As New SqlConnection(MiConexion)
        Dim bndduplicados As Integer = 0
        Try
            If IsNumeric(TextBox1.Text) = True Then
                'CON.Open()
                'Me.ValidaDuplicadoRel_Session_TecnicosTableAdapter.Connection = CON
                'Me.ValidaDuplicadoRel_Session_TecnicosTableAdapter.Fill(Me.DataSetEDGAR.ValidaDuplicadoRel_Session_Tecnicos, gloClave, New System.Nullable(Of Long)(CType(clv_sessionTecnico, Long)), New System.Nullable(Of Integer)(CType(Me.TextBox2.Text, Integer)), bndduplicados)
                'CON.Close()

                bndduplicados = ValidarDuplicados_Y_Cancelados(gloClave, clv_sessionTecnico, Me.TextBox2.Text, "O")
                If bndduplicados = 0 Then
                    CON.Open()
                    Me.ValidaExistenciasTecnicosTableAdapter.Connection = CON
                    Me.ValidaExistenciasTecnicosTableAdapter.Fill(Me.DataSetEDGAR.ValidaExistenciasTecnicos, New System.Nullable(Of Long)(CType(Locclv_tec, Long)), New System.Nullable(Of Long)(CType(Me.TextBox2.Text, Long)), New System.Nullable(Of Long)(CType(Me.TextBox1.Text, Long)))
                    CON.Close()
                    If Me.RespuestaTextBox.Text = "4" Then
                        If Me.ComboBox1.Text = "Instalaciones" Then
                            If IsNumeric(Me.TextBoxMetrajeIni.Text) = False Or IsNumeric(Me.TextBoxMetrajeFin.Text) = False Then
                                MsgBox("Metrajes incorrectos. Ej. 7.5", MsgBoxStyle.Information)
                                Exit Sub
                            End If
                            NueRelOrdSerMetraje(gloClave, CInt(Me.TextBoxMetrajeIni.Text), CInt(Me.TextBoxMetrajeFin.Text))
                        End If
                        CON.Open()
                        Me.Inserta_Valores_TempoTableAdapter.Connection = CON
                        Me.Inserta_Valores_TempoTableAdapter.Fill(Me.DataSetarnoldo.Inserta_Valores_Tempo, clv_sessionTecnico, CInt(Me.TextBox2.Text), CInt(Me.TextBox3.Text), 1, 1, Me.ComboBox3.Text, CInt(Me.TextBox1.Text), Locclv_tec)
                        CON.Close()
                        Me.TextBox1.Clear()
                        llena_Detalle()
                    Else
                        Me.TextBox1.Text = ""
                        MsgBox("No tiene existencias suficientes de este material", MsgBoxStyle.Information)
                    End If
                Else
                    Me.TextBox1.Text = ""
                    'MsgBox("El Articulo ya esta en la lista ", MsgBoxStyle.Information)
                End If
            Else
                Me.TextBox1.Text = ""
                MsgBox("Capture datos validos ", MsgBoxStyle.Information)
            End If

        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub


    Private Sub TextBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox1.KeyPress
        Dim bndduplicados As Integer = 0
        Try
            e.KeyChar = Chr(ValidaKey(Me.TextBox1, Asc(LCase(e.KeyChar)), "N"))
            If e.KeyChar = Chr(13) Then
                AGREGAR()
            End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub TextBox1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox1.TextChanged

    End Sub

    Private Sub OK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK.Click
        LocValida1 = False
        If Me.ConRel_Session_TecnicosDataGridView.RowCount > 0 Then
            LocValida1 = True
            'Eric lo hizo, sirve para habilitar o deshabilitar el combo e Vendedores
            If Me.ConRel_Session_TecnicosDataGridView.RowCount > 0 Then
                eActTecnico = False
            Else
                eActTecnico = True
            End If
            'Me.Dame_FolioTableAdapter.Fill(Me.DataSetarnoldo.Dame_Folio, gloClave, 1, Locclv_folio)
            'Me.Inserta_Bitacora_tecTableAdapter.Fill(Me.DataSetarnoldo.Inserta_Bitacora_tec, gloClave, Locclv_folio, 1, Locclv_tec, GloUsuario, "P", "", LocNo_Bitacora)
            'Me.Inserta_Rel_Bitacora_OrdenTableAdapter.Fill(Me.DataSetarnoldo.Inserta_Rel_Bitacora_Orden, LocNo_Bitacora, CInt(gloClave))
            Me.Close()
        Else
            MsgBox("La Lista esta Vacia por lo cual no se puede Guardar ", MsgBoxStyle.Information)
        End If
    End Sub

    Private Sub llena_Detalle()
        Dim CON As New SqlConnection(MiConexion)
        Try
            CON.Open()
            Me.ConRel_Session_TecnicosTableAdapter.Connection = CON
            Me.ConRel_Session_TecnicosTableAdapter.Fill(Me.DataSetEDGAR.ConRel_Session_Tecnicos, gloClave, New System.Nullable(Of Integer)(CType(Locclv_tec, Integer)), clv_sessionTecnico)
            CON.Close()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try


    End Sub

    Private Sub validadupl()


    End Sub

    'Private Sub FillToolStripButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles FillToolStripButton.Click
    '    Try
    'Dim bnd As Object = BndToolStripTextBox.Text
    '        Me.ValidaDuplicadoRel_Session_TecnicosTableAdapter.Fill(Me.DataSetEDGAR.ValidaDuplicadoRel_Session_Tecnicos, New System.Nullable(Of Long)(CType(Clv_sessionToolStripTextBox.Text, Long)), New System.Nullable(Of Integer)(CType(NoArticuloToolStripTextBox.Text, Integer)), bnd)
    '    Catch ex As System.Exception
    '        System.Windows.Forms.MessageBox.Show(ex.Message)
    '    End Try

    'End Sub








    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        AGREGAR()


    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.Quita_SEBORROART_SALTECTableAdapter.Connection = CON
        Me.Quita_SEBORROART_SALTECTableAdapter.Fill(Me.DataSetEdgarRev2.Quita_SEBORROART_SALTEC, clv_sessionTecnico)
        Me.Dame_FolioTableAdapter.Connection = CON
        Me.Dame_FolioTableAdapter.Fill(Me.DataSetarnoldo.Dame_Folio, gloClave, 1, Locclv_folio)
        Me.Inserta_Bitacora_tecTableAdapter.Connection = CON
        Me.Inserta_Bitacora_tecTableAdapter.Fill(Me.DataSetarnoldo.Inserta_Bitacora_tec, clv_sessionTecnico, gloClave, Locclv_folio, 1, Locclv_tec, GloUsuario, "P", "", LocNo_Bitacora)
        Me.Inserta_Rel_Bitacora_OrdenTableAdapter.Connection = CON
        Me.Inserta_Rel_Bitacora_OrdenTableAdapter.Fill(Me.DataSetarnoldo.Inserta_Rel_Bitacora_Orden, LocNo_Bitacora, CInt(gloClave))
        Me.Inserta_RelCobraDescTableAdapter.Connection = CON
        Me.Inserta_RelCobraDescTableAdapter.Fill(Me.ProcedimientosArnoldo2.Inserta_RelCobraDesc, LocNo_Bitacora, "O")
        MsgBox("Se Guardo con �xito", MsgBoxStyle.Information)
        CON.Close()
        Me.Close()
    End Sub

    Private Sub ConRel_Session_TecnicosDataGridView_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles ConRel_Session_TecnicosDataGridView.CellClick

    End Sub

    Private Sub ConRel_Session_TecnicosDataGridView_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles ConRel_Session_TecnicosDataGridView.CellContentClick

    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        If IsNumeric(Me.NoArticuloTextBox.Text) = True Then
            If Me.MovimientoTextBox.Text = "N" Then
                BorraConcepto()
            ElseIf IsNumeric(Me.NoBitacoraTextBox.Text) = True Then
                If Me.NoBitacoraTextBox.Text > 0 Then
                    BorraExistente()
                End If
            End If
            llena_Detalle()
        End If
        'Eric lo hizo, sirve para habilitar o deshabilitar el combo e Vendedores
        If Me.ConRel_Session_TecnicosDataGridView.RowCount > 0 Then
            eActTecnico = False
        Else
            eActTecnico = True
        End If
    End Sub

    Private Sub BorraConcepto()
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.Borra_Inserta_Tempo_Por_ServicioTableAdapter.Connection = CON
            Me.Borra_Inserta_Tempo_Por_ServicioTableAdapter.Fill(Me.DataSetEdgarRev2.Borra_Inserta_Tempo_Por_Servicio, New System.Nullable(Of Integer)(CType(clv_sessionTecnico, Integer)), New System.Nullable(Of Long)(CType(Me.NoArticuloTextBox.Text, Long)))
            CON.Close()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub BorraExistente()
        Try
            Dim bndduplicados As Integer = 0
            bndduplicados = ValidarDuplicados_Y_Cancelados_Borrrar(gloClave, clv_sessionTecnico, Me.TextBox2.Text, "O")
            If bndduplicados = 0 Then
                Dim CON As New SqlConnection(MiConexion)
                CON.Open()
                Me.INSERTA_Borra_Articulo_BitacoraTableAdapter.Connection = CON
                Me.INSERTA_Borra_Articulo_BitacoraTableAdapter.Fill(Me.DataSetEdgarRev2.INSERTA_Borra_Articulo_Bitacora, clv_sessionTecnico, New System.Nullable(Of Long)(CType(gloClave, Long)), New System.Nullable(Of Long)(CType(Me.NoArticuloTextBox.Text, Long)), New System.Nullable(Of Long)(CType(Locclv_tec, Long)), New System.Nullable(Of Long)(CType(Me.NoBitacoraTextBox.Text, Long)))
                CON.Close()
           
            End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub


    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.ConRel_Session_TecnicosDataGridView.Enabled = False
        Me.Panel1.Visible = True
    End Sub


    Private Sub Clv_Tipo_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Clv_Tipo.TextChanged
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        If IsNumeric(Locclv_tec) = True And IsNumeric(Clv_Tipo.Text) Then
            Me.Muestra_Descripcion_ArticuloTableAdapter.Connection = CON
            Me.Muestra_Descripcion_ArticuloTableAdapter.Fill(Me.DataSetarnoldo.Muestra_Descripcion_Articulo, Locclv_tec, Clv_Tipo.Text)
        End If
        CON.Close()
    End Sub

    Private Sub ComboBox2_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles ComboBox2.LostFocus
        If Me.ComboBox2.SelectedIndex = -1 Then
            Me.ComboBox3.ResetText()
        End If
    End Sub

    Private Sub ComboBox2_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox2.SelectedIndexChanged
        Me.ComboBox3.SelectedIndex = Me.ComboBox2.SelectedIndex
    End Sub

    Private Sub ConRelOrdSerMetraje(ByVal Clv_Orden As Long)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("ConRelOrdSerMetraje", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Clv_Orden", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Clv_Orden
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@MetrajeIni", SqlDbType.Decimal)
        parametro2.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@MetrajeFin", SqlDbType.Decimal)
        parametro3.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro3)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            Me.TextBoxMetrajeIni.Text = parametro2.Value
            Me.TextBoxMetrajeFin.Text = parametro3.Value
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

    Private Sub NueRelOrdSerMetraje(ByVal Clv_Orden As Long, ByVal MetrajeIni As Single, ByVal MetrajeFin As Single)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("NueRelOrdSerMetraje", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Clv_Orden", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Clv_Orden
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@MetrajeIni", SqlDbType.Decimal)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = MetrajeIni
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@MetrajeFin", SqlDbType.Decimal)
        parametro3.Direction = ParameterDirection.Input
        parametro3.Value = MetrajeFin
        comando.Parameters.Add(parametro3)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Sub


End Class
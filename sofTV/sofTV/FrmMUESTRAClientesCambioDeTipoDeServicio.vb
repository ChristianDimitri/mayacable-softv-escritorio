﻿Public Class FrmMUESTRAClientesCambioDeTipoDeServicio
    Private Sub MUESTRAClientesCambioDeTipoDeServicio(ByVal Op As Integer, ByVal Clv_TipSer As Integer, ByVal Contrato As Integer, ByVal Nombre As String)

        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Op", SqlDbType.Int, Op)
        BaseII.CreateMyParameter("@Clv_TipSer", SqlDbType.Int, Clv_TipSer)
        BaseII.CreateMyParameter("@Contrato", SqlDbType.Int, Contrato)
        BaseII.CreateMyParameter("@Nombre", SqlDbType.VarChar, Nombre, 250)
        dgvClientes.DataSource = BaseII.ConsultaDT("MUESTRAClientesCambioDeTipoDeServicio")

    End Sub

    Private Sub FrmMUESTRAClientesCambioDeTipoDeServicio_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
        eContrato = 0
        MUESTRAClientesCambioDeTipoDeServicio(0, eClv_TipSer, 0, "")
    End Sub

    Private Sub tbContrato_KeyDown(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles tbContrato.KeyDown
        If e.KeyValue <> 13 Then Exit Sub
        If tbContrato.Text.Length = 0 Then Exit Sub
        If IsNumeric(tbContrato.Text) = False Then Exit Sub
        MUESTRAClientesCambioDeTipoDeServicio(1, eClv_TipSer, tbContrato.Text, "")
    End Sub

    Private Sub bnBuscarContrato_Click(sender As System.Object, e As System.EventArgs) Handles bnBuscarContrato.Click
        If tbContrato.Text.Length = 0 Then Exit Sub
        If IsNumeric(tbContrato.Text) = False Then Exit Sub
        MUESTRAClientesCambioDeTipoDeServicio(1, eClv_TipSer, tbContrato.Text, "")
    End Sub

    Private Sub tbNombre_KeyDown(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles tbNombre.KeyDown
        If e.KeyValue <> 13 Then Exit Sub
        If tbNombre.Text.Length = 0 Then Exit Sub
        MUESTRAClientesCambioDeTipoDeServicio(2, eClv_TipSer, 0, tbNombre.Text)
    End Sub

    Private Sub bnBuscarNombre_Click(sender As System.Object, e As System.EventArgs) Handles bnBuscarNombre.Click
        If tbNombre.Text.Length = 0 Then Exit Sub
        MUESTRAClientesCambioDeTipoDeServicio(2, eClv_TipSer, 0, tbNombre.Text)
    End Sub

    Private Sub bnAceptar_Click(sender As System.Object, e As System.EventArgs) Handles bnAceptar.Click
        If dgvClientes.RowCount = 0 Then Exit Sub
        eContrato = dgvClientes.SelectedCells(0).Value
        Me.Close()
    End Sub

    Private Sub bnSalir_Click(sender As System.Object, e As System.EventArgs) Handles bnSalir.Click
        eContrato = 0
        Me.Close()
    End Sub
End Class
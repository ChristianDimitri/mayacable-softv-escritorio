﻿Imports System.Text
Imports System.Data.SqlClient

Public Class FrmSucursalesCFD


    Private Sub ConDatosFiscalesCFDSucursales(ByVal Id As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("ConDatosFiscalesCFDSucursales", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0
        Dim reader As SqlDataReader

        Dim par1 As New SqlParameter("@Id", SqlDbType.Int)
        par1.Direction = ParameterDirection.Input
        par1.Value = Id
        comando.Parameters.Add(par1)

        Try
            conexion.Open()
            reader = comando.ExecuteReader

            While (reader.Read())
                tbSerie.Text = reader(0).ToString
                tbFolio.Text = reader(1).ToString
                tbSerieNotas.Text = reader(2).ToString
                tbFolioNotas.Text = reader(3).ToString
                tbCalle.Text = reader(4).ToString
                tbNumeroExt.Text = reader(5).ToString
                tbNumeroInterior.Text = reader(6).ToString
                tbColonia.Text = reader(7).ToString
                tbLocalidad.Text = reader(8).ToString
                tbReferencia.Text = reader(9).ToString
                tbMunicipio.Text = reader(10).ToString
                tbEstado.Text = reader(11).ToString
                tbPais.Text = reader(12).ToString
                tbCp.Text = reader(13).ToString
            End While

        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

    Private Sub NueDatosFiscalesCFDSucursales(ByVal Id As Integer, ByVal Serie As String, ByVal Folio As Integer, ByVal SerieNC As String, ByVal FolioNC As Integer, ByVal Calle As String, ByVal NoExterior As String, ByVal NoInterior As String, ByVal Colonia As String, ByVal Localidad As String, ByVal Referencia As String, ByVal Municipio As String, ByVal Estado As String, ByVal Pais As String, ByVal CodigoPostal As String)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("NueDatosFiscalesCFDSucursales", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Dim par1 As New SqlParameter("@Id", SqlDbType.Int)
        par1.Direction = ParameterDirection.InputOutput
        par1.Value = Id
        comando.Parameters.Add(par1)

        Dim par13 As New SqlParameter("@Serie", SqlDbType.VarChar, 50)
        par13.Direction = ParameterDirection.Input
        par13.Value = Serie
        comando.Parameters.Add(par13)

        Dim par14 As New SqlParameter("@Folio", SqlDbType.Int)
        par14.Direction = ParameterDirection.Input
        par14.Value = Folio
        comando.Parameters.Add(par14)

        Dim par131 As New SqlParameter("@SerieNC", SqlDbType.VarChar, 50)
        par131.Direction = ParameterDirection.Input
        par131.Value = SerieNC
        comando.Parameters.Add(par131)

        Dim par141 As New SqlParameter("@FolioNC", SqlDbType.Int)
        par141.Direction = ParameterDirection.Input
        par141.Value = FolioNC
        comando.Parameters.Add(par141)

        Dim par3 As New SqlParameter("@Calle", SqlDbType.VarChar, 250)
        par3.Direction = ParameterDirection.Input
        par3.Value = Calle
        comando.Parameters.Add(par3)

        Dim par4 As New SqlParameter("@NoExterior", SqlDbType.VarChar, 50)
        par4.Direction = ParameterDirection.Input
        par4.Value = NoExterior
        comando.Parameters.Add(par4)

        Dim par5 As New SqlParameter("@NoInterior", SqlDbType.VarChar, 50)
        par5.Direction = ParameterDirection.Input
        par5.Value = NoInterior
        comando.Parameters.Add(par5)

        Dim par6 As New SqlParameter("@Colonia", SqlDbType.VarChar, 150)
        par6.Direction = ParameterDirection.Input
        par6.Value = Colonia
        comando.Parameters.Add(par6)

        Dim par7 As New SqlParameter("@Localidad", SqlDbType.VarChar, 150)
        par7.Direction = ParameterDirection.Input
        par7.Value = Localidad
        comando.Parameters.Add(par7)

        Dim par8 As New SqlParameter("@Referencia", SqlDbType.VarChar, 150)
        par8.Direction = ParameterDirection.Input
        par8.Value = Referencia
        comando.Parameters.Add(par8)

        Dim par9 As New SqlParameter("@Municipio", SqlDbType.VarChar, 150)
        par9.Direction = ParameterDirection.Input
        par9.Value = Municipio
        comando.Parameters.Add(par9)

        Dim par10 As New SqlParameter("@Estado", SqlDbType.VarChar, 150)
        par10.Direction = ParameterDirection.Input
        par10.Value = Estado
        comando.Parameters.Add(par10)

        Dim par11 As New SqlParameter("@Pais", SqlDbType.VarChar, 150)
        par11.Direction = ParameterDirection.Input
        par11.Value = Pais
        comando.Parameters.Add(par11)

        Dim par12 As New SqlParameter("@CodigoPostal", SqlDbType.VarChar, 50)
        par12.Direction = ParameterDirection.Input
        par12.Value = CodigoPostal
        comando.Parameters.Add(par12)



        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            eId = par1.Value

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

    Private Sub BorDatosFiscalesCFDSucursales(ByVal Id As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("BorDatosFiscalesCFDSucursales", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Dim par1 As New SqlParameter("@Id", SqlDbType.Int)
        par1.Direction = ParameterDirection.InputOutput
        par1.Value = Id
        comando.Parameters.Add(par1)

        Dim par3 As New SqlParameter("@Msj", SqlDbType.VarChar, 150)
        par3.Direction = ParameterDirection.Output
        comando.Parameters.Add(par3)


        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            eMsj = ""
            eMsj = par3.Value.ToString()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub

    Function ConDetDatosFiscalesCFDSucursales(ByVal Op As Integer, ByVal Id As Integer) As BindingSource
        Dim conexion As New SqlConnection(MiConexion)
        Dim sBuilder As New StringBuilder("EXEC ConDetDatosFiscalesCFDSucursales " + Op.ToString() + ", " + Id.ToString())
        Dim dAdapter As New SqlDataAdapter(sBuilder.ToString(), conexion)
        Dim dTable As New DataTable
        Dim bSource As New BindingSource

        Try
            dAdapter.Fill(dTable)
            bSource.DataSource = dTable
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try

        Return bSource

    End Function

    Private Sub NueDetDatosFiscalesCFDSucursales(ByVal Op As Integer, ByVal Id As Integer, ByVal Clv_Sucursal As Integer)

        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("NueDetDatosFiscalesCFDSucursales", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Dim par1 As New SqlParameter("@Op", SqlDbType.Int)
        par1.Direction = ParameterDirection.Input
        par1.Value = Op
        comando.Parameters.Add(par1)

        Dim par3 As New SqlParameter("@Id", SqlDbType.Int)
        par3.Direction = ParameterDirection.Input
        par3.Value = Id
        comando.Parameters.Add(par3)

        Dim par4 As New SqlParameter("@Clv_Sucursal", SqlDbType.Int)
        par4.Direction = ParameterDirection.Input
        par4.Value = Clv_Sucursal
        comando.Parameters.Add(par4)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Sub

    Private Sub BorDetDatosFiscalesCFDSucursales(ByVal Op As Integer, ByVal Id As Integer, ByVal Clv_Sucursal As Integer)

        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("BorDetDatosFiscalesCFDSucursales", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Dim par1 As New SqlParameter("@Op", SqlDbType.Int)
        par1.Direction = ParameterDirection.Input
        par1.Value = Op
        comando.Parameters.Add(par1)

        Dim par3 As New SqlParameter("@Id", SqlDbType.Int)
        par3.Direction = ParameterDirection.Input
        par3.Value = Id
        comando.Parameters.Add(par3)

        Dim par4 As New SqlParameter("@Clv_Sucursal", SqlDbType.Int)
        par4.Direction = ParameterDirection.Input
        par4.Value = Clv_Sucursal
        comando.Parameters.Add(par4)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Sub

    Private Sub FrmSucursalesCFD_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If eOpcion = "N" Then
            eId = 0
            lbIzq.DataSource = ConDetDatosFiscalesCFDSucursales(0, 0)
            lbDer.DataSource = ConDetDatosFiscalesCFDSucursales(1, 0)
            gbSucursal.Enabled = False
            tsbEliminar.Enabled = False
        ElseIf eOpcion = "C" Then
            ConDatosFiscalesCFDSucursales(eId)
            lbIzq.DataSource = ConDetDatosFiscalesCFDSucursales(0, eId)
            lbDer.DataSource = ConDetDatosFiscalesCFDSucursales(1, eId)
            bnSucursalesSAT.Enabled = False
            gbSerie.Enabled = False
            gbSucursal.Enabled = False
        ElseIf eOpcion = "M" Then
            ConDatosFiscalesCFDSucursales(eId)
            lbIzq.DataSource = ConDetDatosFiscalesCFDSucursales(0, eId)
            lbDer.DataSource = ConDetDatosFiscalesCFDSucursales(1, eId)
        End If

    End Sub

    Private Sub tsbGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsbGuardar.Click
        Try

            If tbSerie.Text.Length = 0 Then
                MsgBox("Captura una serie de Facturas.", MsgBoxStyle.Exclamation)
                Exit Sub
            End If

            If tbFolio.Text.Length = 0 Then tbFolio.Text = "0"

            NueDatosFiscalesCFDSucursales(eId, tbSerie.Text, tbFolio.Text, tbSerieNotas.Text, tbFolioNotas.Text, tbCalle.Text, tbNumeroExt.Text, tbNumeroInterior.Text, tbColonia.Text, tbLocalidad.Text, tbReferencia.Text, tbMunicipio.Text, tbEstado.Text, tbPais.Text, tbCp.Text)

            If eOpcion = "N" Then
                tsbEliminar.Enabled = False
                gbSucursal.Enabled = True
            End If

            eRefrescar = True
            MsgBox(mensaje5, MsgBoxStyle.Information)

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        End Try

    End Sub

    Private Sub bnAgregar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bnAgregar.Click

        If lbIzq.Items.Count = 0 Then
            Exit Sub
        End If

        NueDetDatosFiscalesCFDSucursales(0, eId, lbIzq.SelectedValue)
        lbIzq.DataSource = ConDetDatosFiscalesCFDSucursales(0, eId)
        lbDer.DataSource = ConDetDatosFiscalesCFDSucursales(1, eId)
    End Sub

    Private Sub bnAgrearTodos_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bnAgrearTodos.Click

        If lbIzq.Items.Count = 0 Then
            Exit Sub
        End If

        NueDetDatosFiscalesCFDSucursales(1, eId, 0)
        lbIzq.DataSource = ConDetDatosFiscalesCFDSucursales(0, eId)
        lbDer.DataSource = ConDetDatosFiscalesCFDSucursales(1, eId)
    End Sub

    Private Sub bnEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bnEliminar.Click

        If lbDer.Items.Count = 0 Then
            Exit Sub
        End If

        BorDetDatosFiscalesCFDSucursales(0, eId, lbDer.SelectedValue)
        lbIzq.DataSource = ConDetDatosFiscalesCFDSucursales(0, eId)
        lbDer.DataSource = ConDetDatosFiscalesCFDSucursales(1, eId)
    End Sub

    Private Sub bnEliminarTodos_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bnEliminarTodos.Click

        If lbDer.Items.Count = 0 Then
            Exit Sub
        End If

        BorDetDatosFiscalesCFDSucursales(1, eId, 0)
        lbIzq.DataSource = ConDetDatosFiscalesCFDSucursales(0, eId)
        lbDer.DataSource = ConDetDatosFiscalesCFDSucursales(1, eId)
    End Sub

    Private Sub tsbEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsbEliminar.Click
        BorDatosFiscalesCFDSucursales(eId)

        If eMsj.Length = 0 Then
            MsgBox(eMsj, MsgBoxStyle.Exclamation)
        Else
            MsgBox(mensaje6, MsgBoxStyle.Information)
        End If

        eRefrescar = True

        Me.Close()
    End Sub

    Private Sub bnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bnSalir.Click
        Me.Close()
    End Sub
End Class
﻿Public Class FrmComisionesInstalaciones

    Dim refrescar As Boolean = False

    Private Sub MuestraTipServEric(ByVal Clv_TipServ As Integer, ByVal Op As Integer)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_TipServ", SqlDbType.Int, Clv_TipServ)
        BaseII.CreateMyParameter("@Op", SqlDbType.Int, Op)
        cbTipSer.DataSource = BaseII.ConsultaDT("MuestraTipServEric")
    End Sub

    Private Sub CONComisionesInstalaciones(ByVal CLV_TIPSER As Integer)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@CLV_TIPSER", SqlDbType.Int, CLV_TIPSER)
        dgvComisiones.DataSource = BaseII.ConsultaDT("CONComisionesInstalaciones")
    End Sub

    Private Sub NUEComisionesInstalaciones(ByVal RangoInicial As Integer, ByVal RangoFinal As Integer, ByVal Comision As Decimal, ByVal CLV_TIPSER As Integer)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@RangoInicial", SqlDbType.Int, RangoInicial)
        BaseII.CreateMyParameter("@RangoFinal", SqlDbType.Int, RangoFinal)
        BaseII.CreateMyParameter("@Comision", SqlDbType.Decimal, Comision)
        BaseII.CreateMyParameter("@CLV_TIPSER", SqlDbType.Int, CLV_TIPSER)
        BaseII.CreateMyParameter("@Msj", ParameterDirection.Output, SqlDbType.VarChar, 150)
        BaseII.ProcedimientoOutPut("NUEComisionesInstalaciones")
        eMsj = ""
        eMsj = BaseII.dicoPar("@Msj").ToString()
    End Sub

    Private Sub BORComisionesInstalaciones(ByVal IdComision As Integer)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@IdComision", SqlDbType.Int, IdComision)
        BaseII.Inserta("BORComisionesInstalaciones")
    End Sub

    Private Sub bnAgregar_Click(sender As System.Object, e As System.EventArgs) Handles bnAgregar.Click
        If tbRangoInicial.Text.Length = 0 Then
            MessageBox.Show("Captura rango inicial.")
            Exit Sub
        End If

        If IsNumeric(tbRangoInicial.Text) = False Then
            MessageBox.Show("Captura rango inicial válido.")
            Exit Sub
        End If

        If tbRangoFinal.Text.Length = 0 Then
            MessageBox.Show("Captura rango final.")
            Exit Sub
        End If

        If IsNumeric(tbRangoFinal.Text) = False Then
            MessageBox.Show("Captura rango final válido.")
            Exit Sub
        End If

        If tbComision.Text.Length = 0 Then
            MessageBox.Show("Captura comisión.")
            Exit Sub
        End If

        If IsNumeric(tbRangoFinal.Text) = False Then
            MessageBox.Show("Captura comisión válida.")
            Exit Sub
        End If

        NUEComisionesInstalaciones(tbRangoInicial.Text, tbRangoFinal.Text, tbComision.Text, cbTipSer.SelectedValue)
        If eMsj.Length > 0 Then
            MessageBox.Show(eMsj)
            Exit Sub
        End If

        MessageBox.Show("Se guardó con éxito.")
        tbRangoInicial.Clear()
        tbRangoFinal.Clear()
        tbComision.Clear()
        CONComisionesInstalaciones(cbTipSer.SelectedValue)

    End Sub

    Private Sub bnEliminar_Click(sender As System.Object, e As System.EventArgs) Handles bnEliminar.Click
        If dgvComisiones.Rows.Count = 0 Then
            MessageBox.Show("Selecciona un registro.")
            Exit Sub
        End If

        BORComisionesInstalaciones(dgvComisiones.SelectedCells(0).Value)
        MessageBox.Show("Se eliminó con éxito.")
        CONComisionesInstalaciones(cbTipSer.SelectedValue)

    End Sub

    Private Sub bnSalir_Click(sender As System.Object, e As System.EventArgs) Handles bnSalir.Click
        Me.Close()
    End Sub

    Private Sub FrmComisionesInstalaciones_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
        MuestraTipServEric(0, 0)
        CONComisionesInstalaciones(cbTipSer.SelectedValue)
        refrescar = True
    End Sub

    Private Sub cbTipSer_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles cbTipSer.SelectedIndexChanged
        If refrescar = True Then
            CONComisionesInstalaciones(cbTipSer.SelectedValue)
        End If
    End Sub
End Class
Imports System.Data.SqlClient
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Public Class FrmDatosContrato
    Private customersByCityReport As ReportDocument
    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        customersByCityReport.DataSourceConnections(0).SetConnection(GloServerName, GloDatabaseName, GloUserID, GloPassword)
        'customersByCityReport.SetDatabaseLogon(GloUserID, GloPassword, GloServerName, GloDatabaseName)

        Dim myTables As Tables = myReportDocument.Database.Tables
        Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
        For Each myTable In myTables
            Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            myTable.ApplyLogOnInfo(myTableLogonInfo)
            myTable.Location = GloDatabaseName & ".dbo." & myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)
        Next
    End Sub
    Private Sub ConfigureCrystalReportsContrato()
        Try
            Dim CON As New SqlConnection(MiConexion)
            Dim cmd As New SqlClient.SqlCommand
            Dim impresora As String = Nothing
            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo

            connectionInfo.ServerName = GloServerName
            connectionInfo.DatabaseName = GloDatabaseName
            connectionInfo.UserID = GloUserID
            connectionInfo.Password = GloPassword

            Dim reportPath As String = Nothing
            reportPath = RutaReportes + "\CONTRATOCOSMO.rpt"
            customersByCityReport.Load(reportPath)


            SetDBLogonForReport(connectionInfo, customersByCityReport)

            '@Contrato 
            customersByCityReport.SetParameterValue(0, eContrato)
            'CrystalReportViewer1.ReportSource = customersByCityReport
            'CrystalReportViewer1.ShowPrintButton = True

            'Me.Selecciona_ImpresoraTableAdapter.Fill(Me.DataSetarnoldo.Selecciona_Impresora, 1, impresora)

            CON.Open()
            With cmd
                .CommandText = "Hora_ins"
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure
                .Connection = CON
                Dim prm As New SqlParameter("@Hora_ini", SqlDbType.VarChar, 10)
                Dim prm1 As New SqlParameter("@Hora_fin", SqlDbType.VarChar, 10)
                Dim prm2 As New SqlParameter("@contrato", SqlDbType.BigInt)
                Dim prm3 As New SqlParameter("@observaciones", SqlDbType.VarChar)

                '@Hora_ini varchar(10),@Hora_fin varchar(10),@contrato bigint,@observaciones varchar(max)
                prm.Direction = ParameterDirection.Input
                prm1.Direction = ParameterDirection.Input
                prm2.Direction = ParameterDirection.Input
                prm3.Direction = ParameterDirection.Input

                .Parameters.Add(prm)
                .Parameters.Add(prm1)
                .Parameters.Add(prm2)
                .Parameters.Add(prm3)

                prm.Value = horaini
                prm1.Value = horafin
                prm2.Value = eContrato
                prm3.Value = Me.TextBox3.Text

                Dim i As Integer = .ExecuteNonQuery
            End With

            CON.Close()

            customersByCityReport.DataDefinition.FormulaFields("HRMA�ANA").Text = "'" & horaini & "'"
            customersByCityReport.DataDefinition.FormulaFields("HRTARDE").Text = "'" & horafin & "'"
            customersByCityReport.DataDefinition.FormulaFields("OBS").Text = "'" & Me.TextBox3.Text & "'"



            customersByCityReport.PrintOptions.PrinterName = ImpresoraContatos
            customersByCityReport.PrintToPrinter(1, True, 1, 1)

            MsgBox("Se Imprimio Con Exito", MsgBoxStyle.Information)

            'customersByCityReport = Nothing
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Me.Close()
    End Sub

    Private Sub FrmDatosContrato_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        'ImpresoraContatos
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        If Me.TextBox1.Text = "" Then
            MsgBox("Capture Por Favor Una Hora De Instalacion Por La Ma�ana", MsgBoxStyle.Information)
            Exit Sub
        End If
        If Me.TextBox2.Text = "" Then
            MsgBox("Capture Por Favor Una Hora De Instalacion Por La Tarde", MsgBoxStyle.Information)
            Exit Sub
        End If
        horaini = Me.TextBox1.Text
        horafin = Me.TextBox2.Text
        ConfigureCrystalReportsContrato()
        Me.Close()
    End Sub
End Class
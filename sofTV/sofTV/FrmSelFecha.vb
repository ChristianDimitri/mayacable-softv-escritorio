Imports System.Data.SqlClient

Public Class FrmSelFecha

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        eFechaIni = Me.DateTimePicker1.Value
        eBnd = cbBnd.Checked
        If rbResumen.Checked = True Then
            eOpVentas = 71

            FrmImprimirComision.Show()
        Else
            DameSession()
            LocOp = 0
            eOpVentas = 80
            FrmSelTipServRep.Show()
        End If

        Me.Close()
    End Sub

    Private Sub FrmSelFecha_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
        Me.DateTimePicker1.Value = Today
        eBnd = False
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub

    Private Sub rbResumen_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbResumen.CheckedChanged
        
    End Sub

    Private Sub DameSession()
        Dim con As New SqlConnection(MiConexion)
        Dim com As New SqlCommand("Dame_clv_session_Reportes", con)
        com.CommandType = CommandType.StoredProcedure
        com.CommandTimeout = 0

        Dim par As New SqlParameter("@Clv_Session", SqlDbType.BigInt)
        par.Direction = ParameterDirection.Output
        com.Parameters.Add(par)

        Try
            con.Open()
            com.ExecuteNonQuery()
            LocClv_session = par.Value

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            con.Close()
            con.Dispose()
        End Try
    End Sub
End Class
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmServiciosDigaAsignar
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim MACCABLEMODEMLabel As System.Windows.Forms.Label
        Dim CONTRATONETLabel As System.Windows.Forms.Label
        Dim RespuestaLabel As System.Windows.Forms.Label
        Me.Panel10 = New System.Windows.Forms.Panel
        Me.ComboBox14 = New System.Windows.Forms.ComboBox
        Me.MACCABLEMODEMTextBox = New System.Windows.Forms.TextBox
        Me.CONTRATONETTextBox2 = New System.Windows.Forms.TextBox
        Me.RespuestaTextBox = New System.Windows.Forms.TextBox
        Me.CMBLabel32 = New System.Windows.Forms.Label
        Me.Button14 = New System.Windows.Forms.Button
        Me.CMBLabel29 = New System.Windows.Forms.Label
        Me.ComboBox12 = New System.Windows.Forms.ComboBox
        Me.Button13 = New System.Windows.Forms.Button
        MACCABLEMODEMLabel = New System.Windows.Forms.Label
        CONTRATONETLabel = New System.Windows.Forms.Label
        RespuestaLabel = New System.Windows.Forms.Label
        Me.Panel10.SuspendLayout()
        Me.SuspendLayout()
        '
        'MACCABLEMODEMLabel
        '
        MACCABLEMODEMLabel.AutoSize = True
        MACCABLEMODEMLabel.Location = New System.Drawing.Point(182, 42)
        MACCABLEMODEMLabel.Name = "MACCABLEMODEMLabel"
        MACCABLEMODEMLabel.Size = New System.Drawing.Size(108, 13)
        MACCABLEMODEMLabel.TabIndex = 10
        MACCABLEMODEMLabel.Text = "MACCABLEMODEM:"
        '
        'CONTRATONETLabel
        '
        CONTRATONETLabel.AutoSize = True
        CONTRATONETLabel.Location = New System.Drawing.Point(33, 39)
        CONTRATONETLabel.Name = "CONTRATONETLabel"
        CONTRATONETLabel.Size = New System.Drawing.Size(92, 13)
        CONTRATONETLabel.TabIndex = 9
        CONTRATONETLabel.Text = "CONTRATONET:"
        '
        'RespuestaLabel
        '
        RespuestaLabel.AutoSize = True
        RespuestaLabel.ForeColor = System.Drawing.Color.WhiteSmoke
        RespuestaLabel.Location = New System.Drawing.Point(133, 157)
        RespuestaLabel.Name = "RespuestaLabel"
        RespuestaLabel.Size = New System.Drawing.Size(61, 13)
        RespuestaLabel.TabIndex = 8
        RespuestaLabel.Text = "Respuesta:"
        '
        'Panel10
        '
        Me.Panel10.Controls.Add(Me.ComboBox14)
        Me.Panel10.Controls.Add(MACCABLEMODEMLabel)
        Me.Panel10.Controls.Add(Me.MACCABLEMODEMTextBox)
        Me.Panel10.Controls.Add(CONTRATONETLabel)
        Me.Panel10.Controls.Add(Me.CONTRATONETTextBox2)
        Me.Panel10.Controls.Add(RespuestaLabel)
        Me.Panel10.Controls.Add(Me.RespuestaTextBox)
        Me.Panel10.Controls.Add(Me.CMBLabel32)
        Me.Panel10.Controls.Add(Me.Button14)
        Me.Panel10.Controls.Add(Me.CMBLabel29)
        Me.Panel10.Controls.Add(Me.ComboBox12)
        Me.Panel10.Controls.Add(Me.Button13)
        Me.Panel10.Enabled = False
        Me.Panel10.Location = New System.Drawing.Point(3, 12)
        Me.Panel10.Name = "Panel10"
        Me.Panel10.Size = New System.Drawing.Size(409, 263)
        Me.Panel10.TabIndex = 7
        Me.Panel10.Visible = False
        '
        'ComboBox14
        '
        Me.ComboBox14.DisplayMember = "MACCABLEMODEM"
        Me.ComboBox14.Enabled = False
        Me.ComboBox14.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ComboBox14.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox14.ForeColor = System.Drawing.Color.Red
        Me.ComboBox14.FormattingEnabled = True
        Me.ComboBox14.Location = New System.Drawing.Point(18, 37)
        Me.ComboBox14.Name = "ComboBox14"
        Me.ComboBox14.Size = New System.Drawing.Size(371, 24)
        Me.ComboBox14.TabIndex = 0
        Me.ComboBox14.ValueMember = "CONTRATONET"
        '
        'MACCABLEMODEMTextBox
        '
        Me.MACCABLEMODEMTextBox.Location = New System.Drawing.Point(239, 39)
        Me.MACCABLEMODEMTextBox.Name = "MACCABLEMODEMTextBox"
        Me.MACCABLEMODEMTextBox.Size = New System.Drawing.Size(100, 20)
        Me.MACCABLEMODEMTextBox.TabIndex = 11
        Me.MACCABLEMODEMTextBox.TabStop = False
        '
        'CONTRATONETTextBox2
        '
        Me.CONTRATONETTextBox2.Location = New System.Drawing.Point(114, 39)
        Me.CONTRATONETTextBox2.Name = "CONTRATONETTextBox2"
        Me.CONTRATONETTextBox2.Size = New System.Drawing.Size(100, 20)
        Me.CONTRATONETTextBox2.TabIndex = 10
        Me.CONTRATONETTextBox2.TabStop = False
        '
        'RespuestaTextBox
        '
        Me.RespuestaTextBox.BackColor = System.Drawing.Color.WhiteSmoke
        Me.RespuestaTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.RespuestaTextBox.ForeColor = System.Drawing.Color.WhiteSmoke
        Me.RespuestaTextBox.Location = New System.Drawing.Point(200, 154)
        Me.RespuestaTextBox.Name = "RespuestaTextBox"
        Me.RespuestaTextBox.Size = New System.Drawing.Size(100, 13)
        Me.RespuestaTextBox.TabIndex = 9
        Me.RespuestaTextBox.TabStop = False
        '
        'CMBLabel32
        '
        Me.CMBLabel32.AutoSize = True
        Me.CMBLabel32.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel32.ForeColor = System.Drawing.Color.DarkOrange
        Me.CMBLabel32.Location = New System.Drawing.Point(6, 16)
        Me.CMBLabel32.Name = "CMBLabel32"
        Me.CMBLabel32.Size = New System.Drawing.Size(385, 16)
        Me.CMBLabel32.TabIndex = 8
        Me.CMBLabel32.Text = "Seleccione el Equipo al que le va Asignar el Paquete :"
        '
        'Button14
        '
        Me.Button14.BackColor = System.Drawing.Color.DarkOrange
        Me.Button14.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button14.ForeColor = System.Drawing.Color.Black
        Me.Button14.Location = New System.Drawing.Point(213, 203)
        Me.Button14.Name = "Button14"
        Me.Button14.Size = New System.Drawing.Size(139, 33)
        Me.Button14.TabIndex = 3
        Me.Button14.Text = "&CANCELAR"
        Me.Button14.UseVisualStyleBackColor = False
        '
        'CMBLabel29
        '
        Me.CMBLabel29.AutoSize = True
        Me.CMBLabel29.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel29.ForeColor = System.Drawing.Color.LightSlateGray
        Me.CMBLabel29.Location = New System.Drawing.Point(4, 97)
        Me.CMBLabel29.Name = "CMBLabel29"
        Me.CMBLabel29.Size = New System.Drawing.Size(243, 16)
        Me.CMBLabel29.TabIndex = 4
        Me.CMBLabel29.Text = "Seleccione el Paquete a Asignar :"
        '
        'ComboBox12
        '
        Me.ComboBox12.DisplayMember = "Descripcion"
        Me.ComboBox12.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ComboBox12.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox12.FormattingEnabled = True
        Me.ComboBox12.Location = New System.Drawing.Point(18, 118)
        Me.ComboBox12.Name = "ComboBox12"
        Me.ComboBox12.Size = New System.Drawing.Size(363, 23)
        Me.ComboBox12.TabIndex = 1
        Me.ComboBox12.ValueMember = "Clv_Servicio"
        '
        'Button13
        '
        Me.Button13.BackColor = System.Drawing.Color.DarkOrange
        Me.Button13.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button13.ForeColor = System.Drawing.Color.Black
        Me.Button13.Location = New System.Drawing.Point(68, 203)
        Me.Button13.Name = "Button13"
        Me.Button13.Size = New System.Drawing.Size(139, 33)
        Me.Button13.TabIndex = 2
        Me.Button13.Text = "&GUARDAR"
        Me.Button13.UseVisualStyleBackColor = False
        '
        'FrmServiciosDigaAsignar
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(416, 275)
        Me.Controls.Add(Me.Panel10)
        Me.Name = "FrmServiciosDigaAsignar"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Servicios a Asignar"
        Me.Panel10.ResumeLayout(False)
        Me.Panel10.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel10 As System.Windows.Forms.Panel
    Friend WithEvents ComboBox14 As System.Windows.Forms.ComboBox
    Friend WithEvents MACCABLEMODEMTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CONTRATONETTextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents RespuestaTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CMBLabel32 As System.Windows.Forms.Label
    Friend WithEvents Button14 As System.Windows.Forms.Button
    Friend WithEvents CMBLabel29 As System.Windows.Forms.Label
    Friend WithEvents ComboBox12 As System.Windows.Forms.ComboBox
    Friend WithEvents Button13 As System.Windows.Forms.Button
End Class

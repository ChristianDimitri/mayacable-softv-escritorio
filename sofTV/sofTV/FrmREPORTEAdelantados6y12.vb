﻿Public Class FrmREPORTEAdelantados6y12

    Private Sub MuestraMeses()
        BaseII.limpiaParametros()
        cbMes.DataSource = BaseII.ConsultaDT("MuestraMeses")
    End Sub

    Private Sub MuestraAnios()
        BaseII.limpiaParametros()
        cbAnio.DataSource = BaseII.ConsultaDT("MuestraAnios")
    End Sub

    Private Sub FrmREPORTEAdelantados6y12_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
        MuestraMeses()
        MuestraAnios()
    End Sub

    Private Sub bnAceptar_Click(sender As System.Object, e As System.EventArgs) Handles bnAceptar.Click
        If cbMes.Text.Length = 0 Then
            MessageBox.Show("Selecciona mes.")
            Exit Sub
        End If
        If cbAnio.Text.Length = 0 Then
            MessageBox.Show("Selecciona año.")
            Exit Sub
        End If
        eMesIni = cbMes.SelectedValue
        eAnioIni = cbAnio.SelectedValue
        eOpVentas = 102
        FrmImprimirComision.Show()
        Me.Close()
    End Sub

    Private Sub bnSalir_Click(sender As System.Object, e As System.EventArgs) Handles bnSalir.Click
        Me.Close()
    End Sub
End Class
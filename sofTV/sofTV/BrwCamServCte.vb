Imports System.Data.SqlClient
Public Class BrwCamServCte

    Dim eRes As Integer = 0
    Dim eMsg As String = Nothing
    Dim contrato As Long = Nothing
    Private busca As Integer = 0

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        eOpcion = "N"
        eContrato = 0
        FrmCamServCte.Show()
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        If Len(Me.TextBox1.Text) > 0 And IsNumeric(Me.TextBox1.Text) Then
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.ConCambioServClienteTableAdapter.Connection = CON
            Me.ConCambioServClienteTableAdapter.Fill(Me.DataSetEric.ConCambioServCliente, 0, Me.TextBox1.Text, "", 0, 2)
            CON.Close()
            If Me.ConCambioServClienteDataGridView.RowCount > 0 Then
                contrato = Me.ConCambioServClienteDataGridView.SelectedCells(1).Value.ToString
                nuevoTipSer()
            End If
            busca = 1
        ElseIf Len(Me.TextBox1.Text) = 0 Then
            busca = 0
        End If
    End Sub

    Private Sub nuevoTipSer()
        Try
            Dim con As New SqlConnection(MiConexion)
            Dim cmd As New SqlCommand("TipSerNew", con)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandTimeout = 0

            Dim par As New SqlParameter("@Contrato", SqlDbType.BigInt)
            par.Value = contrato
            cmd.Parameters.Add(par)

            Dim par1 As New SqlParameter("@Desc", SqlDbType.VarChar, 150)
            par1.Direction = ParameterDirection.Output
            par1.Value = ""
            cmd.Parameters.Add(par1)

            con.Open()
            cmd.ExecuteNonQuery()
            Label7.Text = par1.Value.ToString
            con.Close()
            con.Dispose()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub TextBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox1.KeyPress
        Dim CON As New SqlConnection(MiConexion)
        If Asc(e.KeyChar) = 13 Then
            If Len(Me.TextBox1.Text) > 0 And IsNumeric(Me.TextBox1.Text) Then
                CON.Open()
                Me.ConCambioServClienteTableAdapter.Connection = CON
                Me.ConCambioServClienteTableAdapter.Fill(Me.DataSetEric.ConCambioServCliente, 0, Me.TextBox1.Text, "", 0, 2)
                CON.Close()
                If Me.ConCambioServClienteDataGridView.RowCount > 0 Then
                    contrato = Me.ConCambioServClienteDataGridView.SelectedCells(1).Value.ToString
                    nuevoTipSer()
                End If
                busca = 1
            ElseIf Len(Me.TextBox1.Text) = 0 Then
                busca = 0
            End If
        End If

    End Sub


    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.ConCambioServClienteTableAdapter.Connection = CON
        Me.ConCambioServClienteTableAdapter.Fill(Me.DataSetEric.ConCambioServCliente, 0, 0, Me.TextBox2.Text, 0, 3)
        CON.Close()
        If Me.ConCambioServClienteDataGridView.RowCount > 0 Then
            contrato = Me.ConCambioServClienteDataGridView.SelectedCells(1).Value.ToString
            nuevoTipSer()
        End If
        If Len(Me.TextBox2.Text) = 0 Then
            busca = 0
        Else
            busca = 1
        End If

    End Sub

    Private Sub TextBox2_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox2.KeyPress
        Dim CON As New SqlConnection(MiConexion)
        If Asc(e.KeyChar) = 13 Then
            CON.Open()
            Me.ConCambioServClienteTableAdapter.Connection = CON
            Me.ConCambioServClienteTableAdapter.Fill(Me.DataSetEric.ConCambioServCliente, 0, 0, Me.TextBox2.Text, 0, 3)
            CON.Close()
            If Me.ConCambioServClienteDataGridView.RowCount > 0 Then
                contrato = Me.ConCambioServClienteDataGridView.SelectedCells(1).Value.ToString
                nuevoTipSer()
            End If
        End If

    End Sub


    Private Sub BrwCamServCte_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        If busca <> 1 Then
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.ConCambioServClienteTableAdapter.Connection = CON
            Me.ConCambioServClienteTableAdapter.Fill(Me.DataSetEric.ConCambioServCliente, 0, 0, "", 0, 0)
            CON.Close()
            If Me.ConCambioServClienteDataGridView.RowCount > 0 Then
                contrato = Me.ConCambioServClienteDataGridView.SelectedCells(1).Value.ToString
                nuevoTipSer()
            End If
        End If
    End Sub

    Private Sub BrwCamServCte_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim CON As New SqlConnection(MiConexion)
        colorea(Me, Me.Name)
        CON.Open()
        Me.ConCambioServClienteTableAdapter.Connection = CON
        Me.ConCambioServClienteTableAdapter.Fill(Me.DataSetEric.ConCambioServCliente, 0, 0, "", 0, 0)
        CON.Close()
        If Me.ConCambioServClienteDataGridView.RowCount > 0 Then
            contrato = Me.ConCambioServClienteDataGridView.SelectedCells(1).Value.ToString
            nuevoTipSer()
        End If
    End Sub

    Private Sub RealizarTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RealizarTextBox.TextChanged

        If Me.RealizarTextBox.Text = 3 Then
            Me.Label5.Text = "Servicio Actual :"
            Me.Label6.Text = "Servicio Anterior :"
        End If
        If Me.RealizarTextBox.Text = 1 Then
            Me.Label5.Text = "Servicio Solicitado :"
            Me.Label6.Text = "Servicio Actual :"
        End If

    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Try
            If Me.ConCambioServClienteDataGridView.RowCount > 0 Then
                Dim CON As New SqlConnection(MiConexion)
                CON.Open()
                Me.BorCambioServClienteTableAdapter.Connection = CON
                Me.BorCambioServClienteTableAdapter.Fill(Me.DataSetEric.BorCambioServCliente, contrato, 0, eRes, eMsg)
                CON.Close()
                If eRes = 1 Then
                    MsgBox(eMsg)
                Else
                    MsgBox(mensaje6)
                    Refrescar()
                End If


            Else
                MsgBox("Selecciona un Cambio de Servicio.", , "Atenci�n")
            End If


        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If Me.ConCambioServClienteDataGridView.RowCount > 0 Then
            If Me.RealizarTextBox.Text = 1 Then
                eContrato = Me.ContratoLabel2.Text
                eTipSer = Me.Clv_TipSerTextBox.Text
                eOpcion = "M"
                FrmCamServCte.Show()
            Else
                MsgBox("S�lo se pueden Modificar Procesos Activos.")
            End If
        Else
            MsgBox("Selecciona un Cliente.", , "Atenci�n")
        End If
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Me.Close()
    End Sub

    Private Sub Refrescar()
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.ConCambioServClienteTableAdapter.Connection = CON
        Me.ConCambioServClienteTableAdapter.Fill(Me.DataSetEric.ConCambioServCliente, 0, 0, "", 0, 0)
        CON.Close()
        If Me.ConCambioServClienteDataGridView.RowCount > 0 Then
            contrato = Me.ConCambioServClienteDataGridView.SelectedCells(1).Value.ToString
            nuevoTipSer()
        End If
    End Sub

    Private Sub ConCambioServClienteDataGridView_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles ConCambioServClienteDataGridView.CellContentClick
        If Me.ConCambioServClienteDataGridView.RowCount > 0 Then
            contrato = Me.ConCambioServClienteDataGridView.SelectedCells(1).Value.ToString
            nuevoTipSer()
        End If
    End Sub

    Private Sub Button3_Click_1(sender As System.Object, e As System.EventArgs) Handles Button3.Click
        eOpcion = "N"
        eContrato = 0
        FrmCamServCte.Show()
    End Sub

    Private Sub Button7_Click(sender As System.Object, e As System.EventArgs) Handles Button7.Click
        Try
            If Me.ConCambioServClienteDataGridView.RowCount > 0 Then
                Dim CON As New SqlConnection(MiConexion)
                CON.Open()
                Me.BorCambioServClienteTableAdapter.Connection = CON
                Me.BorCambioServClienteTableAdapter.Fill(Me.DataSetEric.BorCambioServCliente, contrato, 0, eRes, eMsg)
                CON.Close()
                If eRes = 1 Then
                    MsgBox(eMsg)
                Else
                    MsgBox(mensaje6)
                    Refrescar()
                End If


            Else
                MsgBox("Selecciona un Cambio de Servicio.", , "Atenci�n")
            End If


        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
End Class
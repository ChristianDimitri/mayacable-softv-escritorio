Imports System.Data.SqlClient
Imports System.Text

Public Class BrwPromociones

    Private Sub MuestraTipServEric()
        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder("EXEC MuestraTipServEric 0,0")

        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString, conexion)
        Dim dataTable As New DataTable
        Dim bindingSource As New BindingSource

        Try

            dataAdapter.Fill(dataTable)
            bindingSource.DataSource = dataTable
            cTipSer.DataSource = bindingSource

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Sub

    Private Sub MuestraServiciosEric(ByVal Clv_TipSer As Integer, ByVal Clv_Servicio As Integer, ByVal Op As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder("EXEC MuestraServiciosEric " & CStr(Clv_TipSer) & " , " & CStr(Clv_Servicio) & " , " & CStr(Op))
        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString, conexion)
        Dim dataTable As New DataTable
        Dim bindingSource As New BindingSource

        Try
            dataAdapter.Fill(dataTable)
            bindingSource.DataSource = dataTable
            cServicio.DataSource = bindingSource
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub

    Private Sub ConPromocion(ByVal Op As Integer, ByVal Clv_TipSer As Integer, ByVal Clv_Servicio As Integer, ByVal Nombre As String, ByVal Id As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder("EXEC ConPromocion " & CStr(Op) & " , " & CStr(Clv_TipSer) & " , " & CStr(Clv_Servicio) & " , '" & Nombre & "' , " & CStr(Id))

        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString, conexion)
        Dim dataTable As New DataTable
        Dim bindingSource As New BindingSource

        Try
            dataAdapter.Fill(dataTable)
            bindingSource.DataSource = dataTable
            dgPromociones.DataSource = bindingSource
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Sub

    Private Sub bNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bNuevo.Click
        eOpcion = "N"
        eClv_TipSer = cTipSer.SelectedValue
        FrmPromociones.Show()
    End Sub

    Private Sub bConsultar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles bConsultar.Click
        If dgPromociones.Rows.Count = 0 Then
            MsgBox("Selecciona una promoción.", MsgBoxStyle.Information)
            Exit Sub
        End If
        eOpcion = "C"
        eIdPromocion = dgPromociones.SelectedCells(0).Value
        eClv_TipSer = cTipSer.SelectedValue
        FrmPromociones.Show()
    End Sub

    Private Sub bModificar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles bModificar.Click
        If dgPromociones.Rows.Count = 0 Then
            MsgBox("Selecciona una promoción.", MsgBoxStyle.Information)
            Exit Sub
        End If
        eOpcion = "M"
        eIdPromocion = dgPromociones.SelectedCells(0).Value
        eClv_TipSer = cTipSer.SelectedValue
        FrmPromociones.Show()
    End Sub

    Private Sub bSalir_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles bSalir.Click
        Me.Close()
    End Sub

    Private Sub BrwPromociones_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        If eBndPromocion = True Then
            eBndPromocion = False
            ConPromocion(0, cTipSer.SelectedValue, 0, String.Empty, 0)
        End If
    End Sub

    Private Sub BrwPromociones_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
        MuestraTipServEric()
        ConPromocion(0, cTipSer.SelectedValue, 0, String.Empty, 0)
    End Sub

    Private Sub cTipSer_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cTipSer.SelectedIndexChanged
        ConPromocion(0, cTipSer.SelectedValue, 0, String.Empty, 0)
        MuestraServiciosEric(cTipSer.SelectedValue, 0, 4)
    End Sub

    Private Sub bBusNom_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bBusNom.Click
        If tbNombre.Text.Length = 0 Then
            MsgBox("Captura un nombre.", MsgBoxStyle.Information)
            Exit Sub
        End If

        ConPromocion(2, cTipSer.SelectedValue, 0, tbNombre.Text, 0)

    End Sub

    Private Sub tbNombre_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles tbNombre.KeyDown
        If e.KeyValue <> 13 Then
            Exit Sub
        End If
        If tbNombre.Text.Length = 0 Then
            MsgBox("Captura un nombre.", MsgBoxStyle.Information)
            Exit Sub
        End If

        ConPromocion(2, cTipSer.SelectedValue, 0, tbNombre.Text, 0)

    End Sub

    
    Private Sub bBusSer_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bBusSer.Click
        If IsNumeric(cServicio.SelectedValue) = False Then
            Exit Sub
        End If
        If cServicio.SelectedValue = 0 Then
            Exit Sub
        End If

        ConPromocion(1, cTipSer.SelectedValue, cServicio.SelectedValue, String.Empty, 0)

    End Sub
End Class
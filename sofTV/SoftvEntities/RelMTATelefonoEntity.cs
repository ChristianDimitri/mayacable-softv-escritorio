using System;

namespace Softv.Entities
{
  /// <summary>
  /// Class                   : Softv.Entities.RelMTATelefonoEntity.cs
  /// Generated by            : Desarroll@, Class Generator (c) 2009
  /// Description             : RelMTATelefono entity
  /// File                    : RelMTATelefonoEntity.cs
  /// Creation date           : 17/11/2011
  /// Creation time           : 01:29:14 p.m.
  /// </summary>
  public class RelMTATelefonoEntity
  {
  
      #region attributes
  
      private long? _id;
      /// <summary>
      /// id
      /// </summary>
      public long? id
      {
          get { return _id; }
          set { _id = value; }
      }
  
      private long? _idAparato;
      /// <summary>
      /// idAparato
      /// </summary>
      public long? idAparato
      {
          get { return _idAparato; }
          set { _idAparato = value; }
      }
  
      private long? _idTelefono;
      /// <summary>
      /// idTelefono
      /// </summary>
      public long? idTelefono
      {
          get { return _idTelefono; }
          set { _idTelefono = value; }
      }
  
      private DateTime? _fechaAsignacion;
      /// <summary>
      /// fechaAsignacion
      /// </summary>
      public DateTime? fechaAsignacion
      {
          get { return _fechaAsignacion; }
          set { _fechaAsignacion = value; }
      }
  
      private String _usrAsignacion;
      /// <summary>
      /// usrAsignacion
      /// </summary>
      public String usrAsignacion
      {
          get { return _usrAsignacion; }
          set { _usrAsignacion = value; }
      }

      #endregion

      #region Constructors
      /// <summary>
      /// Default constructor
      /// </summary>

      public RelMTATelefonoEntity()
      {
      }

      /// <summary>
      ///  Parameterized constructor
      /// </summary>
      /// <param name="id"></param>
      /// <param name="idAparato"></param>
      /// <param name="idTelefono"></param>
      /// <param name="fechaAsignacion"></param>
      /// <param name="usrAsignacion"></param>
      public RelMTATelefonoEntity(long?  id,long?  idAparato,long?  idTelefono,DateTime?  fechaAsignacion,String usrAsignacion)
      {
          this._id = id;
          this._idAparato = idAparato;
          this._idTelefono = idTelefono;
          this._fechaAsignacion = fechaAsignacion;
          this._usrAsignacion = usrAsignacion;
      }
      #endregion

  }
}
